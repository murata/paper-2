aaa1def - Merge branch 'dev-visualize-lags-bayesian-sigmoid' into 'main' (Murat Ambarkutuk, 2024-04-02)

61ea03f - fix visualization error (Murat Ambarkutuk, 2024-04-02)

af3342a - max_nfev is now defaults to scipy values (Murat Ambarkutuk, 2024-04-02)

e08d6d7 - simplify the uq and visualization corresponding to it (Murat Ambarkutuk, 2024-04-02)

647e365 - disable non-ideal case (Murat Ambarkutuk, 2024-04-02)

1ae1013 - unparallelize (Murat Ambarkutuk, 2024-04-02)

01473ac - disable cost and jacobian surface visualizations for now (Murat Ambarkutuk, 2024-04-02)

a9f453d - fix broken pipeline (Murat Ambarkutuk, 2024-04-02)

d13099b - fix argument problem in spatial visualization code (Murat Ambarkutuk, 2024-04-02)

bcf149e - faster faster faster! (Murat Ambarkutuk, 2024-04-02)

c43c6a9 - faster faster faster (Murat Ambarkutuk, 2024-04-02)

1fab5f5 - housekeeping efforts (Murat Ambarkutuk, 2024-04-02)

bf9a5bd - disable multi-track processing for now (Murat Ambarkutuk, 2024-04-02)

bee9272 - fix sub pipeline files (Murat Ambarkutuk, 2024-04-02)

2a545fd - add a manual full-cycle stage (Murat Ambarkutuk, 2024-04-02)

55f74f9 - fast and furious. increase test speed and supress spatial visualization (WIP) errors (Murat Ambarkutuk, 2024-04-02)

467244e - test the code with smaller window for now (Murat Ambarkutuk, 2024-04-02)

1b0e4be - apply hotfix (Murat Ambarkutuk, 2024-04-02)

f98a11e - fix typo (Murat Ambarkutuk, 2024-04-02)

db94f11 - fix dependency issue due to pickled base tyoes (Murat Ambarkutuk, 2024-04-02)

deb0a55 - major update (Murat Ambarkutuk, 2024-04-01)

476e32d - spatial analysis (WIP) (Murat Ambarkutuk, 2024-04-01)

8b549ef - fix ci paths (Murat Ambarkutuk, 2024-04-01)

6758ee2 - temporal visualization is done (Murat Ambarkutuk, 2024-04-01)

551455b - embrace the multi-sensorness of the project (Murat Ambarkutuk, 2024-04-01)

94671b7 - improve spatial processing (WIP) (Murat Ambarkutuk, 2024-04-01)

5a03b02 - embrace layered design (Murat Ambarkutuk, 2024-04-01)

24f8269 - add debug strings (Murat Ambarkutuk, 2024-04-01)

b2f72d9 - fix plot (Murat Ambarkutuk, 2024-04-01)

9535a60 - add more plotting to the uq (Murat Ambarkutuk, 2024-04-01)

afd50ad - reduce layer widths significantly to speed the ci pipelines (Murat Ambarkutuk, 2024-04-01)

62e0943 - improve sigma-vs-error plotting with uq (Murat Ambarkutuk, 2024-04-01)

ff9a1ef - fix index mismatch (Murat Ambarkutuk, 2024-04-01)

3fce734 - disable debug prints (Murat Ambarkutuk, 2024-04-01)

3ea0390 - fix typo (Murat Ambarkutuk, 2024-04-01)

c710b2f - solves #9 (Murat Ambarkutuk, 2024-04-01)

e26f854 - ignore .DS_Store files (Murat Ambarkutuk, 2024-04-01)

0ef28e4 - add debug prints and reduce layer widths for testing purposes (Murat Ambarkutuk, 2024-04-01)

07cb24e - update uncertainty quantification plots (Murat Ambarkutuk, 2024-04-01)

e43671d - uq visualization code now uses the official image (Murat Ambarkutuk, 2024-04-01)

39d1b75 - rename the ci jobs to make their functionality (Murat Ambarkutuk, 2024-04-01)

5ff439b - add ci job for plotting for the uq results (Murat Ambarkutuk, 2024-04-01)

aba8b78 - single layer approach (Murat Ambarkutuk, 2024-04-01)

fff1aaa - add multi-layerness into the algorithm design (Murat Ambarkutuk, 2024-04-01)

f2bbe9e - automation improvements (Murat Ambarkutuk, 2024-04-01)

998cee5 - increase automation in trraining data processing (Murat Ambarkutuk, 2024-04-01)

7b31031 - clean repository (Murat Ambarkutuk, 2024-04-01)

a2f9ba9 - improve job dependency chain (Murat Ambarkutuk, 2024-03-31)

bed9022 - spatial test data import process (Murat Ambarkutuk, 2024-03-31)

99cf5f4 - housekeeping (Murat Ambarkutuk, 2024-03-31)

eab1ade - list of errors (Murat Ambarkutuk, 2024-03-31)

7f330fb - start debugging why lag estimations are strictly position (Murat Ambarkutuk, 2024-03-31)

3258546 - finalize visualizations. (Murat Ambarkutuk, 2024-03-31)

96a8826 - update plotting (Murat Ambarkutuk, 2024-03-31)

1f1bbe0 - add prior distribution plotting (Murat Ambarkutuk, 2024-03-31)

01039da - enable package registry provided by gitlab (Murat Ambarkutuk, 2024-03-31)

4dfcc7d - update visual artifacts (Murat Ambarkutuk, 2024-03-31)

06e9c8d - add missing dependency (Murat Ambarkutuk, 2024-03-31)

3f0d3b3 - final commit (hopefully) for the ci pipeline (Murat Ambarkutuk, 2024-03-31)

fd857f4 - fix typo (Murat Ambarkutuk, 2024-03-31)

3f1304b - merge tests into one stage for performance reasons (Murat Ambarkutuk, 2024-03-31)

16505c4 - debug (Murat Ambarkutuk, 2024-03-31)

9c3e927 - fix path (Murat Ambarkutuk, 2024-03-31)

5025923 - merge stages and groundtruth visualization job now `needs` temporal-estimations job to be completed (Murat Ambarkutuk, 2024-03-31)

c196f75 - fix ci job stage order (Murat Ambarkutuk, 2024-03-31)

6b1c60c - add analysis stage so the artifacts can acculumulate in the right order (Murat Ambarkutuk, 2024-03-31)

5acc381 - add  visual reporting (Murat Ambarkutuk, 2024-03-31)

1c1b27a - change to democratic voting scheme for now (Murat Ambarkutuk, 2024-03-31)

4b1750c - change likelihood calculation to ignore out-of-boundary estimations (Murat Ambarkutuk, 2024-03-31)

f700a7a - fix typo (Murat Ambarkutuk, 2024-03-30)

d23073c - fix missing init arg in sigmoid lag estimator (Murat Ambarkutuk, 2024-03-30)

8c5a8fd - revert parallelization in locata_sigmoid.py file (Murat Ambarkutuk, 2024-03-30)

31fe624 - Merge branch 'dev-visualize-lags-bayesian-sigmoid' of https://code.vt.edu/murata/paper-2 into dev-visualize-lags-bayesian-sigmoid (Murat Ambarkutuk, 2024-03-30)

7feb5b5 - update readme (license badge) and lag_space becomes a kwarg and property (Murat Ambarkutuk, 2024-03-30)

46f04ca - parallelize the locata_sigmoid.py script (Murat Ambarkutuk, 2024-03-30)

34125ed - expand lag space to range (-10, 11) (Murat Ambarkutuk, 2024-03-30)

eb69bea - integer optimization (Murat Ambarkutuk, 2024-03-30)

26180ef - add lag_space optimization as lag_differences can only be type of np.int64 (love discrete world!) (Murat Ambarkutuk, 2024-03-29)

708d960 - change the default values for lag_space variable (Murat Ambarkutuk, 2024-03-29)

a68d1b4 - disable normalization in the likelihood calculation. likelihood vectors now represent the total "vote" (Murat Ambarkutuk, 2024-03-29)

443ed8a - disable softmax in the layer probability calculation (the vote magnitude) (Murat Ambarkutuk, 2024-03-28)

9756340 - disable vector mapping before softmax (Murat Ambarkutuk, 2024-03-28)

a821aa8 - change soft voting to hard one (Murat Ambarkutuk, 2024-03-28)

e7d7999 - fix typo (Murat Ambarkutuk, 2024-03-28)

6f39a95 - remove name-tests-test hook from pre-commit (Murat Ambarkutuk, 2024-03-28)

b68bfd4 - add more pre-commit checks (Murat Ambarkutuk, 2024-03-28)

e0a7d56 - simplify likelihood generation code (Murat Ambarkutuk, 2024-03-28)

5c7be75 - add bayesian sigmoid result visualization script. add kde functionality to the sigmoid likelihood (Murat Ambarkutuk, 2024-03-27)

6d2505e - hot fix for deploy job (Murat Ambarkutuk, 2024-03-27)

1b6a653 - Merge branch 'dev-sigmoid-lag-estimator' into 'main' (Murat Ambarkutuk, 2024-03-27)

5cce452 - fix typo (Murat Ambarkutuk, 2024-03-27)

5740024 - add needsdata to the jobs need data (so they can be picked up by the right runner) (Murat Ambarkutuk, 2024-03-27)

fa62822 - allow failure on cleanup job (Murat Ambarkutuk, 2024-03-27)

1b29712 - revert visual outputting (Murat Ambarkutuk, 2024-03-27)

38c50cb - update pipeline configuration to create necessary artifact folders (Murat Ambarkutuk, 2024-03-26)

27994d4 - fix typo (Murat Ambarkutuk, 2024-03-26)

e5cf192 - add bayesian sigmoid lag estimator with visual reporting as ci artifacts (Murat Ambarkutuk, 2024-03-26)

e4be232 - add matplotlib needed dependencies so i can debug by plotting (Murat Ambarkutuk, 2024-03-26)

52c6ffd - test the bayesian idea (Murat Ambarkutuk, 2024-03-26)

f8c7d30 - add a method to calculate moments of a pdf given its domain (Murat Ambarkutuk, 2024-03-26)

fc83b3e - add bayesian estimation to the pipeline for automated processing (Murat Ambarkutuk, 2024-03-26)

ace3419 - add the bayesian idea (Murat Ambarkutuk, 2024-03-26)

b6b5094 - add tqdm to the uncertainty_quantification script (Murat Ambarkutuk, 2024-03-26)

3fc542c - hot-fix the typo in the unittest coverage reporting (Murat Ambarkutuk, 2024-03-26)

2be92e7 - fix typo in the deployment stage (Murat Ambarkutuk, 2024-03-26)

c118777 - add coverage reporting to the pipeline (Murat Ambarkutuk, 2024-03-26)

af5ad4b - fix deploy script in the ci pipelines (Murat Ambarkutuk, 2024-03-26)

d0c8dc9 - fix a typo (Murat Ambarkutuk, 2024-03-26)

72024cd - Merge branch 'dev-add-compose-makefile' into 'main' (Murat Ambarkutuk, 2024-03-26)

b70b1b0 - fix typo to improve changelog quality (Murat Ambarkutuk, 2024-03-26)

a68e749 - add changelog and docker-compose (Murat Ambarkutuk, 2024-03-26)

aaff9f1 - fix typo (Murat Ambarkutuk, 2024-03-26)

302d36b - Merge branch 'dev-data-processing-locata' into 'main' (Murat Ambarkutuk, 2024-03-26)

c9a5bae - add LICENSE and README files (Murat Ambarkutuk, 2024-03-26)

67fb0ba - add deploy stage (Murat Ambarkutuk, 2024-03-26)

11932d6 - change .gitlab-ci.yml file to conduct static tests on a smaller image (Murat Ambarkutuk, 2024-03-26)

b9340b4 - fix typo (Murat Ambarkutuk, 2024-03-26)

47282d1 - change the ci pipeline to reflect the commit sha not the branch name (Murat Ambarkutuk, 2024-03-26)

9e082dc - fix typo in dockerfile (Murat Ambarkutuk, 2024-03-26)

1108dde - Remove copying all the files into the docker image created (Murat Ambarkutuk, 2024-03-26)

00506c0 - add the missing static test tools (Murat Ambarkutuk, 2024-03-26)

1278638 - change image names in the rest of the test jobs (Murat Ambarkutuk, 2024-03-26)

8eec5d5 - maybe build the image with the right name (Murat Ambarkutuk, 2024-03-26)

1b7777e - fix type (Murat Ambarkutuk, 2024-03-26)

1829737 - fix ci pipeline by providing the right push address (Murat Ambarkutuk, 2024-03-26)

e97b5d7 - add debug prints (Murat Ambarkutuk, 2024-03-26)

2d51efc - improve dind structure by pushing into container registeries (Murat Ambarkutuk, 2024-03-26)

e5ee1ac - employ dind and add a new dockerfile to be used in dind structure (Murat Ambarkutuk, 2024-03-26)

dee3a65 - fix typo (Murat Ambarkutuk, 2024-03-26)

d2d4e83 - update file names and the data should be ready to runner images mounted at /data/ (Murat Ambarkutuk, 2024-03-26)

f79f741 - simplify gitlab ci configuration and separate tests into reasonable categories to allow concurrent testing (Murat Ambarkutuk, 2024-03-26)

49b69fd - change the lambda regularization weight to a test argument and set it to zero for the evaluation of ideal cases (Murat Ambarkutuk, 2024-03-26)

ea5c9e6 - change the sensor suite to eigenmike (n=32) to achieve the max accuracy (Murat Ambarkutuk, 2024-03-26)

3a03325 - Merge branch 'dev-remove-jupyter' into 'main' (Murat Ambarkutuk, 2024-03-25)

8b4b9ff - fix typo (Murat Ambarkutuk, 2024-03-25)

54cfc64 - add more assertions to the test cases (Murat Ambarkutuk, 2024-03-25)

c40e1e4 - fix the result size assertion by adding 1 for the ideal case (Murat Ambarkutuk, 2024-03-25)

16d6db0 - add verbose assertion messages (Murat Ambarkutuk, 2024-03-25)

b1d6232 - simplify test case to see the results for now (Murat Ambarkutuk, 2024-03-25)

883ce40 - fix typo in the test case and adjust test cases with reasonable numbers (Murat Ambarkutuk, 2024-03-25)

1e47857 - add automation to ci pipeline to automatically evaluate the error characteristics of localizers (Murat Ambarkutuk, 2024-03-25)

07ec4f3 - fix typo and add debug prints for artifact logs (Murat Ambarkutuk, 2024-03-25)

0fec1e0 - Change the vanilla and base localizers to resolve mro issues (Murat Ambarkutuk, 2024-03-25)

31c9499 - fix unhandled case in the vanilla tdoa localizer (Murat Ambarkutuk, 2024-03-25)

8975d65 - add dependencies into the new test step (Murat Ambarkutuk, 2024-03-25)

c7c546b - add functionality to characterize localization error in the ci pipeline (Murat Ambarkutuk, 2024-03-25)

aabe6c1 - fix typo (Murat Ambarkutuk, 2024-03-25)

3c7da97 - add functionality to characterize localization error in the ci pipeline (Murat Ambarkutuk, 2024-03-25)

9175635 - remove stale projects (Murat Ambarkutuk, 2024-03-25)

c66e78a - add caching to increase the ci/cd speed (Murat Ambarkutuk, 2024-03-25)

48d7e89 - Merge branch '5-job-failed' into 'main' (Murat Ambarkutuk, 2024-03-25)

a3e5604 - disable the unittest for the localizer module for now (Murat Ambarkutuk, 2024-03-25)

244beab - resolve unittest issue caused by network / location ambiguity (Murat Ambarkutuk, 2024-03-25)

294a787 - add missing dependency librosa (Murat Ambarkutuk, 2024-03-25)

19ecd23 - keep autofixing the code (Murat Ambarkutuk, 2024-03-25)

f98700c - Merge branch 'dev-enable-ci' into 'main' (Murat Ambarkutuk, 2024-03-25)

9629b75 - simplify even more (Murat Ambarkutuk, 2024-03-25)

c70fa50 - relax test terms for now (Murat Ambarkutuk, 2024-03-25)

eedbc83 - autoformat the repo (Murat Ambarkutuk, 2024-03-25)

0ec079f - add dependencies (Murat Ambarkutuk, 2024-03-25)

5fd901f - specify dev images in the ci pipeline (Murat Ambarkutuk, 2024-03-25)

f69e458 - add readme file with ci status badge (Murat Ambarkutuk, 2024-03-25)

31ad6d2 - move ci config file to root folder (Murat Ambarkutuk, 2024-03-25)

e86dc32 - enable simple ci steps (Murat Ambarkutuk, 2024-03-25)

3eb7176 - dump (Murat Ambarkutuk, 2024-02-17)

5cdaa74 - convert localizers into a subpackage (Murat Ambarkutuk, 2024-02-17)

0860897 - clean up (Murat Ambarkutuk, 2024-02-17)

55bbfad - update the code (Murat Ambarkutuk, 2024-02-08)

4a4ec9d - delete trunk for python linting (Murat Ambarkutuk, 2024-02-08)

70a7ea3 - add prelim content (Murat Ambarkutuk, 2024-01-08)

9535fb6 - Merge branch 'chaos' into main (Murat Ambarkutuk, 2024-01-02)

476b38f - remove all package (Murat Ambarkutuk, 2024-01-02)

74a4161 - rename the python pkg into sigmoid (Murat Ambarkutuk, 2024-01-02)

bbfd019 - remove old plots (Murat Ambarkutuk, 2024-01-02)

b80c507 - fix feature plots and nms code (Murat Ambarkutuk, 2023-12-20)

3af1b0e - lets dump again (Murat Ambarkutuk, 2023-12-20)

4a34c8c - update paper (Murat Ambarkutuk, 2023-12-07)

55245a7 - add new code (Murat Ambarkutuk, 2023-12-07)

53a9d62 - remove old code (Murat Ambarkutuk, 2023-12-07)

52d732a - add content to intro and separate lit rev (Murat Ambarkutuk, 2023-11-29)

cd744fe - chaos continues (Murat Ambarkutuk, 2023-11-29)

0232039 - dump (Murat Ambarkutuk, 2023-11-06)

3922372 - add new stuff (Murat Ambarkutuk, 2023-10-24)

c3ff260 - last-nights-writing (Murat Ambarkutuk, 2023-10-24)

33ab20b - remove and ignore ist files (Murat Ambarkutuk, 2023-10-20)

9cf06a5 - add the proposal manuscript before the paper (Murat Ambarkutuk, 2023-10-20)

f1001d1 - add cancel jobs (Murat Ambarkutuk, 2023-10-06)

4a88a59 - rpn is done with some missing points (Murat Ambarkutuk, 2023-10-06)

38f6a51 - full automation with argparsing etc (Murat Ambarkutuk, 2023-09-18)

0ea3105 - update the code repo (Murat Ambarkutuk, 2023-09-12)

89f6581 - update ignore file wrt the project structure (Murat Ambarkutuk, 2023-09-12)

a533abb - delete .gitmodules file to delete the submodule (Murat Ambarkutuk, 2023-09-12)

6f32bb3 - fix the weird issue in the vscode terminal (Murat Ambarkutuk, 2023-08-28)

52f702d - remove old figures -- spring clean up (Murat Ambarkutuk, 2023-08-28)

f14d97d - add the theorem environment (Murat Ambarkutuk, 2023-08-28)

66ca762 - delete mdpi template and install elsearticle (Murat Ambarkutuk, 2023-08-28)

a34553e - add the code (Murat Ambarkutuk, 2023-08-27)

5d0dd2d - keep removing the duplicated files in the repo (Murat Ambarkutuk, 2023-08-27)

3d84dc1 - remove duplicated folders (Murat Ambarkutuk, 2023-08-27)

b3e7c38 - update main move manuscript into a folder (Murat Ambarkutuk, 2023-08-27)

02d330c - add initial files (Murat Ambarkutuk, 2023-08-27)

3d3ad00 - Initial commit (Murat Ambarkutuk, 2023-08-24)

from sklearn.cluster import MeanShift
from sklearn.neighbors import KNeighborsClassifier


def fit_knn(X, y):
    knn = KNeighborsClassifier().fit(X, y)
    return knn


def fit_meanshift(X):
    mean_shift = MeanShift().fit(X.reshape(-1, 1))
    return mean_shift

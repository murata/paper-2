from dataclasses import dataclass, field
from statistics import mean
from typing import Iterator, List, Optional

import numpy as np
from utils import Security


@dataclass
class Sensor:
    id: str = field(default_factory=lambda: str(Security.new_uuid()), init=False)
    location: np.ndarray
    sampling_frequency: float = 1.0  # New property with default value of 1

    def __repr__(self):
        return f"Sensor(id='{self.id}', location={self.location}, sampling_frequency={self.sampling_frequency})"

    def __str__(self):
        return f"Sensor with ID {self.id} at ({self.location[0]}, {self.location[1]}) with sampling frequency {self.sampling_frequency} Hz"

    def __hash__(self):
        return hash(self.id)

    def __eq__(self, other):
        if not isinstance(other, Sensor):
            return NotImplemented
        return self.id == other.id


@dataclass
class SensorNetwork:
    id: str = field(default_factory=lambda: str(Security.new_uuid()), init=False)
    _sensors: List[Sensor] = field(default_factory=list, repr=False)

    def __repr__(self):
        return f"SensorNetwork(name='{self.id}', num_sensors={len(self._sensors)})"

    def __str__(self):
        sensor_info = "\n".join(str(sensor) for sensor in self._sensors)
        return f"SensorNetwork '{self.id}' with sensors:\n{sensor_info}"

    def __iter__(self):
        """Return an iterator over the sensors."""
        return iter(self._sensors)

    def __len__(self):
        return self.num_sensors

    def __getitem__(self, index):
        return self._sensors[index]

    def __eq__(self, other):
        if not isinstance(other, SensorNetwork):
            return NotImplemented
        return self.id == other.id

    @property
    def sensors(self) -> List[Sensor]:
        return self._sensors

    @property
    def num_sensors(self) -> int:
        return len(self._sensors)

    @property
    def locations(self) -> np.ndarray:
        return np.array([_sensor.location for _sensor in self._sensors])

    @property
    def gdop(self) -> float:
        return np.sqrt(self.pdop**2 + self.tdop**2)

    @property
    def pdop(self) -> float:
        return np.sqrt(self.Q[0, 0] + self.Q[1, 1] + self.Q[2, 2])

    @property
    def hdop(self) -> float:
        return np.sqrt(self.Q[0, 0] + self.Q[1, 1])

    @property
    def vdop(self) -> float:
        return np.sqrt(self.Q[2, 2])

    @property
    def tdop(self) -> float:
        return np.sqrt(self.Q[3, 3])

    @property
    def gdop_planar(self) -> float:
        return np.sqrt(self.pdop_planar**2 + self.tdop_planar**2)

    @property
    def pdop_planar(self) -> float:
        return np.sqrt(self.Q_planar[0, 0] + self.Q_planar[1, 1])

    @property
    def hdop_planar(self) -> float:
        return np.sqrt(self.Q_planar[0, 0] + self.Q_planar[1, 1])

    @property
    def vdop_planar(self) -> float:
        return 0

    @property
    def tdop_planar(self) -> float:
        return np.sqrt(self.Q_planar[2, 2])

    @property
    def Q_planar(self) -> np.ndarray:
        G = np.ones((len(self._sensors), 3))
        for i, sensor in enumerate(self._sensors):
            G[i, :2] = sensor.location[0:2]
        return np.linalg.inv(G.T @ G)

    @property
    def Q(self) -> np.ndarray:
        G = np.ones((len(self._sensors), 4))
        for i, sensor in enumerate(self._sensors):
            G[i, :3] = sensor.location[0:3]
        return np.linalg.inv(G.T @ G)

    def add_sensor(self, sensor: Sensor):
        if not isinstance(sensor, Sensor):
            raise ValueError("Only Sensor objects can be added.")
        self._sensors.append(sensor)

    def remove_sensor(self, sensor: Sensor):
        self._sensors.remove(sensor)

    def find_sensor_by_id(self, sensor_id: str) -> Optional[Sensor]:
        for sensor in self._sensors:
            if sensor.id == sensor_id:
                return sensor
        return None


@dataclass(frozen=True)
class Event:
    id: str = field(default_factory=lambda: str(Security.new_uuid()), init=False)
    time: float
    energy: float
    confidence: float
    detected_by: Sensor

    def is_temporally_close(self, other_event, threshold):
        """Check if another event is within the time threshold."""
        return abs(self.time - other_event.time) < threshold

    def is_more_confident_than(self, other_event):
        """Check if this event has a higher confidence than another event."""
        return self.confidence > other_event.confidence

    def time_difference(self, event):
        return self.time - event.time

    def __eq__(self, other):
        """Check equality based on id."""
        if not isinstance(other, Event):
            return NotImplemented
        return self.id == other.id

    def __hash__(self):
        """Compute the hash based on id."""
        return hash(self.id)


@dataclass
class MatchedEvent:
    id: str = field(default_factory=lambda: str(Security.new_uuid()), init=False)
    _events: List[Event] = field(default_factory=list)

    @property
    def events(self) -> List[Event]:
        return self._events

    @property
    def num_sensors(self) -> int:
        return len(set(event.detected_by for event in self.events))

    def __str__(self) -> str:
        return f"MatchedEvent with {len(self.events)} events from {self.num_sensors} sensors"

    def __repr__(self) -> str:
        return f"{self.__class__.__name__}(events={self.events})"

    def __iter__(self) -> Iterator[Event]:
        return iter(self.events)

    def __eq__(self, other) -> bool:
        if not isinstance(other, MatchedEvent):
            return NotImplemented
        return self.id == other.id

    def extract_times(self):
        times = [event.time for event in self._events]
        return np.array(times)

    def extract_confidences(self):
        confidences = [event.confidence for event in self._events]
        return np.array(confidences)

    def add_event(self, event: Event):
        self._events.append(event)

    def add_events(self, events: List[Event]):
        for event in events:
            self._events.append(event)

    def remove_event(self, event_id: float):
        self._events = [event for event in self._events if event.id != event_id]

    def remove_events(self, idx: int):
        if 0 <= idx < len(self._events):
            del self._events[idx]

    def average_time(self) -> float:
        return mean(event.time for event in self.events)

    def total_energy(self) -> float:
        return sum(event.energy for event in self.events)

    def average_confidence(self) -> float:
        return mean(event.confidence for event in self.events)

    def events_by_sensor(self, sensor):
        for event in self.events:
            if event.detected_by == sensor:
                return event
        raise ValueError(f"{sensor} did not detect these events.")

    def sort_events_by_time(self):
        self._events.sort(key=lambda event: event.time)

    def sort_events_by_confidence(self):
        self._events.sort(key=lambda event: event.confidence, reverse=True)

class SigmoidInvertalDetector(BaseEstimator):
    def __init__(self, sensor, layers, scaler, axis, distance_thr, confidence_thr):
        self.distance_thr = distance_thr
        self.confidence_thr = confidence_thr
        self.sigmoid = None
        self.layers = layers
        self.scaler = scaler
        self.sensor = sensor
        self.axis = axis
        self.sigmoid = SigmoidDecomposition(self.layers, self.scaler, self.axis)

    def fit(self, X, y=None):
        # Define the cost function

        y = np.array(y)

        def cost_function(params):
            # Unpack the parameters
            confidence_thr, distance_thr = params
            self.confidence_thr = confidence_thr
            self.distance_thr = distance_thr
            # Detect, merge events using the current parameters
            sensor_events, ptcd = self.detect(X)
            merged_events = SigmoidInvertalDetector.merge_events(
                sensor_events, ptcd, self.distance_thr
            )
            # Calculate predicted intervals
            predicted_intervals = np.zeros((len(merged_events), 2))
            for idx, event in enumerate(merged_events):
                detected_times = np.array([event.start_time, event.end_time])
                predicted_intervals[idx, :] = detected_times

            cost_matrix = SigmoidInvertalDetector.calculate_cost_matrix(
                predicted_intervals, y
            )
            normalized_costs = SigmoidInvertalDetector.soft_assignment(cost_matrix)
            loss = SigmoidInvertalDetector.soft_assignment_loss(
                normalized_costs, cost_matrix
            )
            print(
                params,
                len(merged_events),
                "matrix",
                cost_matrix.shape,
                "Cost ",
                normalized_costs,
                "loss",
                loss,
            )
            return loss

        # Optimization step
        initial_params = [self.confidence_thr, self.distance_thr]
        result = minimize(
            cost_function, initial_params, options={"eps": 1e-1}, method="BFGS"
        )

        # Update the parameters with optimized values
        self.confidence_thr, self.distance_thr = result.x

        return self

    def predict(self, X):
        sensor_events, ptcd = self.detect(X)
        # significant_events = SigmoidInvertalDetector.nms(sensor_events, ptcd, self.sensor, self.distance_thr)
        merged_events = SigmoidInvertalDetector.merge_events(
            sensor_events, ptcd, self.distance_thr
        )
        return merged_events

    def detect(self, X):
        sensor_events = []
        weight_pyramid, bias_pyramid = self.sigmoid.fit_transform(X)
        ptcd = self.sigmoid.ptcd.transform(X)

        for weight_layer, bias_layer in zip(weight_pyramid, bias_pyramid):
            for w, b in zip(weight_layer, bias_layer):
                conf = w / np.amax(weight_layer)
                if conf > self.confidence_thr:
                    start_time = max(0, self.scaler * self.sigmoid.logit(0.001) + b)
                    end_time = min(self.scaler * self.sigmoid.logit(0.999) + b, len(X))
                    energy = ptcd[int(end_time)] - ptcd[int(start_time)]
                    e = IntervalEvent(
                        start_time=start_time,
                        end_time=end_time,
                        energy=energy,
                        confidence=conf,
                        detected_by=self.sensor,
                    )
                    sensor_events.append(e)
        return sensor_events, ptcd

    @staticmethod
    def calculate_cost_matrix(predicted_intervals, true_intervals):
        num_predictions = len(predicted_intervals)
        num_ground_truths = len(true_intervals)
        cost_matrix = np.zeros((num_predictions, num_ground_truths))

        for i in range(num_predictions):
            for j in range(num_ground_truths):
                cost_matrix[i, j] = 1 - SigmoidInvertalDetector.vectorized_diou(
                    predicted_intervals[i].reshape(-1, 2),
                    true_intervals[j].reshape(-1, 2),
                )

        return cost_matrix

    @staticmethod
    def soft_assignment(cost_matrix, temperature=1.0):
        soft_costs = np.exp(
            -cost_matrix / temperature
        )  # Using temperature to control the sharpness of distribution
        normalized_costs = soft_costs / soft_costs.sum(
            axis=1, keepdims=True
        )  # Normalize across ground truths
        return normalized_costs

    @staticmethod
    def soft_assignment_loss(normalized_costs, cost_matrix):
        return np.sum(normalized_costs * cost_matrix)

    @staticmethod
    def merge_events(significant_events, ptcd, distance_thr):
        n = len(significant_events)
        overlap_matrix = np.zeros((n, n), dtype=bool)
        # Fill the overlap matrix
        for i in range(n):
            for j in range(i + 1, n):
                if significant_events[i].overlaps_with(
                    significant_events[j], distance_thr
                ):  # Adjust threshold as needed
                    overlap_matrix[i, j] = overlap_matrix[j, i] = True

        # Identify partitions
        partitions = []
        for i in range(n):
            found = False
            for p in partitions:
                if any(overlap_matrix[i, j] for j in p):
                    p.add(i)
                    found = True
                    break
            if not found:
                partitions.append({i})

        # Merge events in each partition
        merged_events = []
        for partition in partitions:
            merged_event = SigmoidInvertalDetector.merge_partition(
                partition, significant_events, ptcd
            )
            merged_events.append(merged_event)

        return merged_events

    @staticmethod
    def merge_partition(partition, events, ptcd):
        start_time = min(events[i].start_time for i in partition)
        end_time = max(events[i].end_time for i in partition)
        confidence = max(events[i].confidence for i in partition)
        energy = ptcd[int(end_time)] - ptcd[int(start_time)]
        sensor = events[next(iter(partition))].detected_by
        return IntervalEvent(start_time, end_time, energy, confidence, sensor)

    @staticmethod
    def nms(sensor_events, ptcd, sensor, distance_thr):
        # Sort events by confidence (assuming higher weight means higher confidence)
        sensor_events.sort(key=lambda event: event.confidence, reverse=True)
        significant_events = [sensor_events[0]]
        for current_event in sensor_events[1:]:
            add_event = True
            for i, significant_event in enumerate(significant_events):
                if current_event.overlaps_with(significant_event, distance_thr):
                    start_time = min(
                        significant_event.start_time, current_event.start_time
                    )
                    end_time = max(current_event.end_time, significant_event.end_time)
                    conf = current_event.confidence
                    energy = ptcd[int(end_time)] - ptcd[int(start_time)]
                    if current_event.confidence <= significant_event.confidence:
                        conf = significant_event.confidence
                    e = IntervalEvent(
                        start_time=start_time,
                        end_time=end_time,
                        energy=energy,
                        confidence=conf,
                        detected_by=sensor,
                    )
                    significant_events[i] = e  # Replace with more confident event
                    add_event = False
                    break
            if add_event:
                significant_events.append(current_event)
        return significant_events

    def score(self, X, y, sample_weight=None):
        y_hat = self.predict(X)
        return self.vectorized_diou(y, y_hat)

    def vectorized_diou(intervals1, intervals2):
        """
        Calculate the Distance Intersection over Union (DIoU) for arrays of 1-D intervals.
        :param intervals1: Array of intervals, each interval is a tuple (start, end).
        :param intervals2: Array of intervals, each interval is a tuple (start, end).
        :return: Array of DIoU scores for corresponding interval pairs.
        """
        # Convert tuples to NumPy arrays for vectorized operations
        starts1, ends1 = intervals1[:, 0], intervals1[:, 1]
        starts2, ends2 = intervals2[:, 0], intervals2[:, 1]

        # Calculate intersection and union
        intersections = np.maximum(
            0, np.minimum(ends1, ends2) - np.maximum(starts1, starts2)
        )
        unions = np.maximum(ends1, ends2) - np.minimum(starts1, starts2)
        iou = np.divide(
            intersections, unions, out=np.zeros_like(intersections), where=unions != 0
        )

        # Calculate center points and distances
        centers1 = (starts1 + ends1) / 2
        centers2 = (starts2 + ends2) / 2
        center_distances = np.abs(centers1 - centers2)

        # Calculate the diagonal length of the smallest enclosing interval
        diagonal_lengths = np.maximum(ends1, ends2) - np.minimum(starts1, starts2)

        # Calculate DIoU
        diou = iou - np.square(center_distances) / np.square(diagonal_lengths)

        return diou


class TemporalIntervalMatcher:
    @staticmethod
    def match(events, temporal_matching_criteria):
        grouped_events = defaultdict(list)
        # Group events by sensor
        for event in events:
            grouped_events[event.detected_by].append(event)

        matched_events = list()
        for sensor, sensor_events in grouped_events.items():
            sensor_events.sort(
                key=lambda e: (e.start_time + e.end_time) / 2
            )  # Sort events by time
            for event in sensor_events:
                matched = False
                for matched_event in matched_events:
                    # Check if event is temporally close to any event in matched_event
                    if any(
                        e.overlaps_with(event, temporal_matching_criteria)
                        for e in matched_event.events
                    ):
                        matched_event.add_event(event)
                        matched = True
                        break
                if not matched:
                    # Create a new MatchedEvent if no match found
                    matched_events.append(MatchedEvent([event]))

        return matched_events

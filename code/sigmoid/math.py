import numpy as np
import scipy.stats as ss
from scipy.special import logsumexp
from sklearn.decomposition import PCA


class Math:
    def __init__(self):
        self._eps = np.finfo(float).eps

    @property
    def eps(self):
        return self._eps

    @staticmethod
    def sigmoid(x):
        return 1 / (1 + np.exp(-x))

    @staticmethod
    def logit(p):
        if p <= 0 or p >= 1:
            raise ValueError("p must be between 0 and 1, exclusive.")
        return np.log(p / (1 - p))

    @staticmethod
    def map_vector(x, min_val=0, max_val=1):
        # Ensure the input vector x is a numpy array
        x = np.array(x)

        # Compute the original min and max of the vector
        orig_min = np.min(x)
        orig_max = np.max(x)

        # Scale the vector to the new range
        scaled_x = min_val + (x - orig_min) * (max_val - min_val) / (
            orig_max - orig_min
        )

        return scaled_x

    @staticmethod
    def softmax(x, safe=False):
        """Compute softmax values for each element in x."""
        if safe:
            lse = logsumexp(x)
            e_x = np.exp(x - lse)
        else:
            e_x = np.exp(x)
            e_x = e_x / e_x.sum(axis=0)
        return e_x

    @staticmethod
    def softplus(x):
        return np.log(1 + np.exp(-np.abs(x)))

    @staticmethod
    def mad(data):
        median = np.median(data)
        mad = np.median(np.abs(data - median))
        return mad

    @staticmethod
    def fit_line(x, y):
        A = np.vstack([x, np.ones(len(x))]).T
        m, c = np.linalg.lstsq(A, y, rcond=None)[0]
        return m, c

    @staticmethod
    def evaluate_line(x, m, c):
        return m * x + c

    @staticmethod
    def norm_pdf(x, mu, std):
        return ss.norm(mu, std).pdf(x)

    @staticmethod
    def music_spectrum(X, L, angles_scan, num_sources=1):
        """
        Compute the MUSIC spectrum for DOA estimation.

        Parameters:
        X : array_like
            The input signal matrix with shape (num_sensors, num_samples).
        L : array_like
            The sensor location matrix with shape (num_sensors, 2 or 3).
        angles_scan : array_like
            The range of angles to scan for DOA estimation, in degrees.

        Returns:
        spectrum : ndarray
            The MUSIC spectrum for the specified range of angles.
        """
        num_sensors, num_samples = X.shape

        # Covariance matrix estimation
        R = X @ X.conj().T / num_samples

        # Eigenvalue decomposition
        eigenvalues, eigenvectors = np.eigh(R)
        eigenvectors = eigenvectors[
            :, np.argsort(-eigenvalues)
        ]  # Sort eigenvectors by descending eigenvalues
        noise_subspace = eigenvectors[:, num_sensors - num_sources :]  # Noise subspace

        # MUSIC spectrum
        spectrum = np.zeros_like(angles_scan, dtype=float)

        for i, angle in enumerate(angles_scan):
            steering_vector = np.exp(
                -2j
                * np.pi
                * np.dot(L, [np.cos(np.radians(angle)), np.sin(np.radians(angle))])
            )
            spectrum[i] = 1 / (
                steering_vector.conj().T
                @ noise_subspace
                @ noise_subspace.conj().T
                @ steering_vector
            )

        return spectrum

    @staticmethod
    def sparse_cross_correlation(sparse1, sparse2):
        """Compute cross-correlation between two sparse arrays."""
        # Convert sparse arrays to COO format for easy iteration
        coo1 = sparse1.tocoo()
        coo2 = sparse2.tocoo()

        # Initialize the result array with the appropriate size
        max_shift = max(coo1.shape[0], coo2.shape[0])
        correlation = np.zeros(2 * max_shift - 1)

        # Iterate over non-zero elements in the first sparse array
        for i, j, v in zip(coo1.row, coo1.col, coo1.data):
            # For each non-zero element, iterate over non-zero elements in the second sparse array
            for k, l, w in zip(coo2.row, coo2.col, coo2.data):
                # Compute the shift and update the correlation value
                shift = k - i + max_shift - 1
                correlation[shift] += v * w

        return correlation

    @staticmethod
    def descriptives(vector):
        return ss.describe(vector)

    @staticmethod
    def winsorized_mean(vector, p):
        """
        Calculates the Winsorized mean of a vector at a specified percentage.

        Parameters:
        - vector: The input vector (list or NumPy array).
        - p: The percentage (0-50) of values to replace at each end of the vector.

        Returns:
        - The Winsorized mean of the vector.
        """
        if p < 0 or p > 0.50:
            raise ValueError("Percentage 'p' must be between 0 and 50.")

        n = len(vector)
        k = int(np.floor(n * p))

        sorted_vector = np.sort(vector)
        lower_bound = sorted_vector[k]
        upper_bound = sorted_vector[-k - 1]

        winsorized_vector = np.where(
            sorted_vector < lower_bound, lower_bound, sorted_vector
        )
        winsorized_vector = np.where(
            winsorized_vector > upper_bound, upper_bound, winsorized_vector
        )

        return np.mean(winsorized_vector)

    @staticmethod
    def whiten_data(x):
        pca = PCA(whiten=True)

        # Fit and transform the data
        x_whitened = pca.fit_transform(x)
        return x_whitened

    @staticmethod
    def moments_pdf(x, fx, n):
        moments = np.zeros(n)
        mean = np.trapz(x * fx, x)
        moments[0] = mean
        for i in range(1, len(moments)):
            moments[i] = np.trapz((x - mean) ** (i + 1) * fx, x)
        return moments

import numpy as np
import scipy.stats as ss
from sigmoid.math import Math
from sklearn.base import BaseEstimator, TransformerMixin


class PowerTemporalCumulativeDensity(BaseEstimator, TransformerMixin):
    """Calculate the power temporal cumulative density of input data."""

    def __init__(self, axis: int = 0):
        """
        Initialize the PowerTemporalCumulativeDensity transformer.

        Parameters:
        - axis: int, optional (default=0)
            The axis along which to calculate the cumulative density.
        """
        self._axis = axis

    @property
    def axis(self) -> int:
        """Get the axis value."""
        return self._axis

    def fit(self, X, y=None):
        """
        Fit the transformer to the data.

        Parameters:
        - X: array-like, shape (n_samples, n_features)
            The input data.
        - y: array-like, shape (n_samples,), optional (default=None)
            The target values.

        Returns:
        - self: object
            Returns the instance itself.
        """
        return self

    def transform(self, X):
        """
        Transform the input data.

        Parameters:
        - X: array-like, shape (n_samples, n_features)
            The input data.

        Returns:
        - cumulative_energy: array-like, shape (n_samples, n_features)
            The cumulative energy of the input data.
        - mask: array-like, shape (n_samples, n_features)
            The sign mask of the input data.
        """
        cumulative_energy = np.cumsum(np.square(X), axis=self._axis)
        mask = np.sign(X)
        return cumulative_energy, mask

    def inverse_transform(self, y):
        """
        Inverse transform the input data.

        Parameters:
        - y: tuple
            The transformed data, consisting of cumulative_energy and mask.

        Returns:
        - inverse_transformed_data: array-like, shape (n_samples, n_features)
            The inverse transformed data.
        """
        cumulative_energy, mask = y
        diff = np.diff(cumulative_energy, append=cumulative_energy[-1], axis=self._axis)
        return np.sqrt(diff) * mask


class SigmoidDecomposition(BaseEstimator, TransformerMixin):
    def __init__(self, axis=0):
        self._axis = axis

    @property
    def axis(self):
        return self._axis

    def fit(self, X, y=None):
        return self

    def transform(self, X, **kwargs):
        num_components = kwargs.get("num_components", 8)
        num_layers = kwargs.get("num_layers", 4)
        weight_pyramid = np.full((num_layers, num_components), np.nan)
        bias_pyramid = np.full((num_layers, num_components), 0, dtype=np.int64)

        power_density, mask = PowerTemporalCumulativeDensity().transform(X)

        for layer_idx in range(num_layers):
            step = np.power(2, layer_idx)
            signal_of_interest = power_density[::step]
            num_samples_layer = len(signal_of_interest)
            segment_length = num_samples_layer // num_components
            derivative_vals = np.diff(signal_of_interest, prepend=signal_of_interest[0])
            # print(f"layer idx: {layer_idx} num_components: {num_components} segment length {segment_length}, power density {power_density.shape} step {step} num_samples_layer {num_samples_layer}")
            for component_idx in range(num_components):
                start_idx = component_idx * segment_length
                end_idx = min((component_idx + 1) * segment_length, num_samples_layer)
                if end_idx > start_idx:  # Ensures that we have a valid range
                    weight_pyramid[layer_idx, component_idx] = (
                        signal_of_interest[end_idx - 1] - signal_of_interest[start_idx]
                    )
                    interval = np.arange(start_idx, end_idx)
                    if len(interval) == 1:
                        bias_pyramid[layer_idx, component_idx] = start_idx
                    else:
                        idx = np.argmax(derivative_vals[interval])
                        bias_pyramid[layer_idx, component_idx] = interval[idx]

        return weight_pyramid, bias_pyramid, mask

    def inverse_transform(self, y):
        weight_pyramid, bias_pyramid, mask, num_samples = y
        ptcd = PowerTemporalCumulativeDensity()

        if isinstance(self._layers, int):
            sigmoid_matrix = SigmoidDecomposition.create_sigmoid_matrix(
                num_samples, bias_pyramid[0], self.scaler
            )

            composed_ptcd = np.dot(weight_pyramid[0], sigmoid_matrix)
            return ptcd.inverse_transform(composed_ptcd)

        composed_signals = np.zeros((len(self._layers), num_samples))
        composed_ptcds = np.zeros((len(self._layers), num_samples))
        for layer_idx, _ in enumerate(self._layers):
            sigmoid_matrix = SigmoidDecomposition.create_sigmoid_matrix(
                num_samples, bias_pyramid[layer_idx], self._scaler
            )
            composed_ptcd = np.dot(weight_pyramid[layer_idx], sigmoid_matrix)
            composed_ptcds[layer_idx, :] = composed_ptcd
            composed_signals[layer_idx, :] = ptcd.inverse_transform(
                (composed_ptcd, mask)
            )
        return composed_signals, composed_ptcds

    @staticmethod
    def create_sigmoid_matrix(num_samples, biases, scaler) -> np.ndarray:
        x_matrix = np.arange(num_samples) - np.array(biases)[:, np.newaxis]
        sigmoid_matrix = Math.sigmoid(x_matrix / scaler)
        return sigmoid_matrix


class SigmoidLagEstimator(BaseEstimator):
    def __init__(
        self,
        lag_range,
        n_lag,
        axis=0,
    ):
        self._sigmoid = SigmoidDecomposition(axis)
        self._lag_space = np.linspace(lag_range[0], lag_range[1], n_lag)
        self._A = None

    @property
    def A(self):
        return self._A

    @property
    def lag_space(self):
        return self._lag_space

    @property
    def sigmoid(self):
        return self._sigmoid

    def fit(self, X, y=None):
        def objective(A, X, Y):
            return np.linalg.norm(Y - T(X, A), axis=0)

        def T(X, A):
            return np.dot(X, A.T)

        self._A = np.linalg.lstsq(X, y, rcond=None)[0].T
        self._is_fitted = True
        error = objective(self._A, X, y)
        print("Error:", error)
        return self

    def predict(self, signal1, signal2):
        """Estimate time lag between two signals using layer-wise cross-correlation."""
        posterior, lag_space = self.predict_proba(signal1, signal2)
        return Math.moments_pdf(x=lag_space, fx=posterior, n=2)

    def predict_proba(self, signal_A, signal_B, **kwargs):
        """Estimate the probability distribution of time lags between two signals using Bayesian aggregation."""
        if signal_A is None:
            weight_pyramid_A = kwargs.get("weight_pyramid_A")
            bias_pyramid_A = kwargs.get("bias_pyramid_A")
        else:
            weight_pyramid_A, bias_pyramid_A, _ = self._sigmoid.transform(signal_A)
        if signal_A is None:
            weight_pyramid_B = kwargs.get("weight_pyramid_B")
            bias_pyramid_B = kwargs.get("bias_pyramid_B")
        else:
            weight_pyramid_B, bias_pyramid_B, _ = self._sigmoid.transform(signal_B)
        likelihoods = np.zeros((weight_pyramid_A.shape[0], len(self.lag_space)))

        layer_idx = 0
        for prob_layer_A, bias_layer_A, prob_layer_B, bias_layer_B in zip(
            weight_pyramid_A,
            bias_pyramid_A,
            weight_pyramid_B,
            bias_pyramid_B,
        ):
            for bias_value_A, bias_value_B, prob_value_A, prob_value_B in zip(
                bias_layer_A,
                bias_layer_B,
                prob_layer_A,
                prob_layer_B,
            ):
                # nice job!
                if prob_value_A == 0 or prob_value_B == 0:
                    continue

                lag_value = bias_value_A - bias_value_B

                idx = np.searchsorted(self.lag_space, lag_value)
                if 0 < idx < len(self.lag_space) - 1:
                    likelihoods[layer_idx, :] += ss.norm.pdf(
                        x=self.lag_space, loc=lag_value, scale=5
                    )
                    likelihoods[layer_idx, :] /= likelihoods[layer_idx, :].sum()
            layer_idx += 1

        return likelihoods

import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns


def plot_errors(errors, fname):
    plt.plot(errors)
    plt.savefig(fname)
    plt.close()


def plot_signals(signals, fname="signals.png"):
    fig, ax = plt.subplots(len(signals), 5, figsize=(20, 20))

    for idx, signal in enumerate(signals):
        ax[idx, 0].plot(signal.k, signal.sequence)
        ax[idx, 0].set_ylabel(signal.label)
        ax[idx, 0].set_xlabel("Time Step $n$")

        ax[idx, 1].pcolormesh(signal.stft_t, signal.stft_f, np.abs(signal.stft))
        ax[idx, 1].set_xlabel("Time Step $n$")
        ax[idx, 1].set_ylabel(r"""$\frac{f}{f_s}$""")

        ax[idx, 2].plot(signal.k, signal.energy)
        ax[idx, 2].set_xlabel("Time Step $n$")

        ax[idx, 3].plot(np.abs(signal.dst) / np.abs(signal.dst[0]))

        ax[idx, 4].matshow(signal.dwt)

    ax[0, 0].set_title(f"Measurement $x[n]$")
    ax[0, 1].set_title(r"""DFT $X[k] = \mathcal{F}\{x[n]\}$""")
    ax[0, 2].set_title(r"""Energy $e[n] = \sum_{i=0}^{n} x[i]^2$""")
    ax[0, 3].set_title(r"""DST $E[k] = \mathcal{S}\{e[n]\}$""")
    ax[0, 4].set_title(r"""CWT $E[k] = \mathcal{W}\{x[n]\}$""")

    fig.tight_layout()
    fig.savefig(fname)


def show():
    plt.show()


def plot_time_series(ax, signal):
    ax.plot(signal.k, signal.sequence)
    ax.set_xlabel("Time Step $n$")
    ax.set_ylabel("Amplitude")


def plot_cumulative_energy(ax, signal):
    ax.plot(signal.k, signal.energy)
    ax.set_xlabel("Time Step $n$")
    ax.set_ylabel("Amplitude")


def plot_sigmoid(ax, sigmoid):
    ax.plot(sigmoid.composed_signal.T)


def make_figure(nrows, ncols, width, height):
    fig, ax = plt.subplots(nrows, ncols, figsize=(width, height))
    return fig, ax


def plot_state(signal, fname="state.png"):
    fig, ax = plt.subplots(1, 5, figsize=(20, 3))

    ax[0].plot(signal.k, signal.sequence, label="input")
    ax[0].set_ylabel(signal.label)
    ax[0].set_xlabel("Time Step $n$")

    ax[1].pcolormesh(signal.stft_t, signal.stft_f, np.abs(signal.stft))
    ax[1].set_xlabel("Time Step $n$")
    ax[1].set_ylabel(r"""$\frac{f}{f_s}$""")

    ax[2].plot(signal.k, signal.energy, "k-", lw=3)
    ax[2].plot(signal.k, signal.approx_energy, "c-.", lw=3)
    ax[2].set_xlabel("Time Step $n$")
    qq = np.abs(signal.dst) / np.abs(signal.dst[0])
    ax[3].plot(signal.k, np.diff(qq, append=0))
    ax[3].set_xlabel("Time Step $n$")
    ax[3].set_ylabel("Scales")

    ax[4].matshow(signal.dwt)
    ax[4].set_xlabel("Time Step $n$")
    ax[4].set_ylabel("Scales")

    ax[0].set_title(f"Measurement $x[n]$")
    ax[1].set_title(r"""DFT $X[k] = \mathcal{F}\{x[n]\}$""")
    ax[2].set_title(r"""Energy $e[n] = \sum_{i=0}^{n} x[i]^2$""")
    ax[3].set_title(r"""DST $E[k] = \mathcal{S}\{e[n]\}$""")
    ax[4].set_title(r"""CWT $E[k] = \mathcal{W}\{x[n]\}$""")

    fig.tight_layout()
    fig.savefig(fname)


def heatmap(matrix, fname, title="Heatmap", annot=False, figsize=(10, 10)):
    """
    Plots a heatmap for a given 2D square matrix using Seaborn.

    Parameters:
    matrix (list[list] or np.ndarray): 2D square matrix to be visualized.
    title (str): Title of the heatmap.
    figsize (tuple): Size of the figure (width, height) in inches.

    Returns:
    None: The function plots the heatmap.
    """

    plt.figure(figsize=figsize)
    # matrix[matrix>5] = np.nan
    sns.heatmap(matrix, annot=annot, cmap="viridis", square=True)

    plt.title(title)
    plt.xlabel("X-axis")
    plt.ylabel("Y-axis")
    plt.savefig(fname)


def plot_bisiler(signal_est, energy_est, signal, weights, biases, rms, fname):
    fig, ax = plt.subplots(3, sharex=True, figsize=(6, 6))
    ax[0].plot(signal.k, signal.sequence, label="Signal")
    ax[0].plot(signal.k, signal_est, label="Est")
    ax[0].plot(np.nan, np.nan, label=f"RMS: {rms[0]}")

    ax[0].legend()

    ax[1].plot(signal.k, signal.energy, label="Signal")
    ax[1].plot(signal.k, energy_est, label="Est")
    ax[1].plot(np.nan, np.nan, label=f"RMS: {rms[1]}")
    ax[1].legend()

    ax[2].plot(biases, weights / np.amax(weights), "r.")
    ax[2].set_ylim([0, 1.1])

    # ax[0].set_title(f"Transform Order: {order}")
    ax[0].set_ylabel("Sensor Measurement")
    ax[1].set_ylabel("Energy Accumulated")
    ax[2].set_ylabel("Coefficients")
    ax[2].set_xlabel("Time Step $k$")
    fig.tight_layout()
    plt.savefig(fname)
    plt.close()


def plot_matched_events(data, matched_events, lims, fname="matched_events.png"):
    plt.figure(figsize=(30, 3))
    # plt.plot(np.max(data**2, axis=1))
    # plt.plot(-np.max(data**2, axis=1))
    plt.plot(data)
    colors = np.random.rand(len(matched_events), 3)
    for idx, matched_event in enumerate(matched_events):
        min_time = min(event.time for event in matched_event)
        max_time = max(event.time for event in matched_event)

        # Calculate median confidence
        median_confidence = np.median([event.confidence for event in matched_event])

        # Shade the area between min_time and max_time
        plt.fill_betweenx(
            lims, min_time, max_time, color=colors[idx], alpha=median_confidence
        )

    plt.ylim(lims)
    plt.savefig(fname)
    plt.close()

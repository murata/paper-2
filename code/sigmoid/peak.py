from collections import defaultdict

import sigmoid.base as sb
from sigmoid.math import Math
from sigmoid.types import *
from sklearn.base import BaseEstimator


class SigmoidPeakDetector(BaseEstimator):
    def __init__(self, layers, scaler=1, axis=0):
        self._sigmoid = sb.SigmoidDecomposition(layers, scaler, axis)

    @property
    def sigmoid(self):
        return self._sigmoid

    def fit(self, X, y=None):
        return self

    def predict(self, X, sensor=None):
        sensor_events = []

        weight_pyramid, bias_pyramid, _ = self._sigmoid.fit_transform(X)

        instant_power = np.square(X)
        for weight_layer, bias_layer in zip(weight_pyramid, bias_pyramid):
            normalized_weights = Math.map_vector(weight_layer)
            confidence_layer = Math.softmax(normalized_weights)
            # TODO: check this out
            confidence_layer = Math.map_vector(confidence_layer)

            for conf, time_sample in zip(confidence_layer, bias_layer):
                if conf >= (1 / len(weight_layer)):
                    # Calculate adjusted time and energy for the event
                    energy = instant_power[
                        time_sample
                    ]  # Assuming energy is indexed by time

                    # Create an event for this data point
                    e = Event(
                        time=time_sample / sensor.sampling_frequency,
                        energy=energy,
                        confidence=conf,
                        detected_by=sensor,
                    )
                    sensor_events.append(e)
        return sensor_events


class TemporalPeakMatcher:
    @staticmethod
    def match(events, temporal_matching_criteria):
        grouped_events = defaultdict(list)
        # Group events by sensor
        for event in events:
            grouped_events[event.detected_by].append(event)

        matched_events = list()
        for sensor, sensor_events in grouped_events.items():
            sensor_events.sort(key=lambda e: e.time)  # Sort events by time
            for event in sensor_events:
                matched = False
                for matched_event in matched_events:
                    # Check if event is temporally close to any event in matched_event
                    if any(
                        e.is_temporally_close(event, temporal_matching_criteria)
                        for e in matched_event.events
                    ):
                        matched_event.add_event(event)
                        matched = True
                        break
                if not matched:
                    # Create a new MatchedEvent if no match found
                    matched_events.append(MatchedEvent([event]))

        return matched_events

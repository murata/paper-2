import numpy as np


def rms(x):
    return np.round(np.sqrt(np.mean(x**2)), 2)


def noise(length, mean=0, var=1):
    return np.random.randn(length) * np.sqrt(var) + mean


def stair(length, peak1, peak2):
    return step(length, peak1) + step(length, peak2)


def step(length, peak):
    signal = np.zeros(length, dtype=np.float32)
    signal[peak:] = 1
    return signal


def impulse(length, peak):
    signal = np.zeros(length)
    signal[peak] = 1
    return signal


def triangle(length, peak, height):
    signal = np.zeros(length)
    ascent_slope = height / peak
    descent_slope = height / (length - peak - 1)

    # Generate the ascent and descent of the triangle
    for i in range(length):
        if i < peak:
            signal[i] = ascent_slope * i
        else:
            signal[i] = height - descent_slope * (i - peak)

    return signal


def impulse_response(length, peak, natural_freq, damping_ratio):
    # Time array
    t = np.arange(length)

    # Damped natural frequency
    omega_d = natural_freq * np.sqrt(1 - damping_ratio**2)

    # Impulse response function (for t >= peak)
    signal = np.zeros(length)
    for i in range(length):
        if t[i] >= peak:
            signal[i] = np.exp(-damping_ratio * natural_freq * (t[i] - peak)) * np.sin(
                omega_d * (t[i] - peak)
            )

    return signal

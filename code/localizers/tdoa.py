import numpy as np
import sigmoid.types as st
from localizers.base import BaseLocalizer
from scipy.optimize import least_squares
from sigmoid.types import Sensor, SensorNetwork


class VanillaTDOAMultilateration(BaseLocalizer):
    def __init__(self, network, wave_velocity=343):
        if isinstance(network, (np.ndarray, list)):
            self._network = SensorNetwork()
            for location in network:
                self._network.add_sensor(Sensor(location))
        else:
            self._network = network
        super().__init__(self._network.locations, wave_velocity)

    @property
    def network(self):
        return self._network

    def jacobian(self, x, events):
        x_source = x[:-1]
        jac = []

        for event in events:
            distance = np.linalg.norm(
                x_source - event.detected_by.location
            )  # Distance to the sensor

            dxyz = -(x_source - event.detected_by.location) / (
                self.wave_velocity * distance
            )
            dt_0 = -1

            jac.append(np.hstack([dxyz, dt_0]))
        return np.array(jac)

    def residuals(self, x, events):
        x_source = x[:-1]
        t_0 = x[-1]
        equations = []

        for event in events:
            distance = np.linalg.norm(x_source - event.detected_by.location)
            propagation_duration = distance / self.wave_velocity
            equations.append(event.time - t_0 - propagation_duration)

        res = np.array(equations).ravel()
        return res

    def predict(
        self,
        matched_event,
        initial_guess=None,
        lower_bounds=(0, 0, 0, -1000),
        upper_bounds=(10, 10, 4, 1000),
    ):
        if len(lower_bounds) == 3:
            lower_bounds = np.hstack([lower_bounds, -1000])

        if len(upper_bounds) == 3:
            upper_bounds = np.hstack([upper_bounds, 1000])

        initial_guess = (
            initial_guess if initial_guess is not None else np.array([0, 0, 0, 0])
        )
        result = least_squares(
            self.residuals,
            initial_guess,
            args=([matched_event.events]),
            method="trf",
            xtol=1e-12,
            ftol=1e-12,
            max_nfev=1e5,
            bounds=(lower_bounds, upper_bounds),
            jac=self.jacobian,
        )

        if result.success:
            result = result.x[:-1]
        else:
            print(result)
            result = initial_guess[:-1]

        return np.array(result)

    def inverse(
        self,
        impact_location,
        ref_event=None,
        network=None,
        wave_velocity=None,
        energy=1.0,
        confidence=1.0,
    ):
        matched_event = st.MatchedEvent()
        if wave_velocity is None:
            wave_velocity = self.wave_velocity

        if network is None:
            network = self.network

        distances = np.array(
            [
                np.linalg.norm(impact_location - sensor.location)
                for sensor in self.network
            ]
        )
        time_of_flight = distances / wave_velocity

        if ref_event is not None:
            t0 = ref_event.time - time_of_flight[0]
        else:
            t0 = 0
        time_of_arrival = time_of_flight + t0
        events = [
            st.Event(time=toa, energy=energy, confidence=confidence, detected_by=sensor)
            for toa, sensor in zip(time_of_arrival, network)
        ]
        matched_event.add_events(events)
        return matched_event


class TDOAHyperbolaeSolver(VanillaTDOAMultilateration):
    def __init__(self, network, wave_velocity=343):
        super().__init__(network, wave_velocity)

    def jacobian_hyper(self, x, events, ref_events=None):
        jac = []
        if ref_events is None:
            ref_events = [events[0]]

        for ref_event in ref_events:
            dist_to_ref_sensor = np.linalg.norm(x - ref_event.detected_by.location)

            for event in events:
                if event == ref_event:
                    continue
                dist_to_current_sensor = np.linalg.norm(x - event.detected_by.location)

                # Compute the gradient of the time difference with respect to x
                grad_current_sensor = (
                    x - event.detected_by.location
                ) / dist_to_current_sensor
                grad_ref_sensor = (
                    x - ref_event.detected_by.location
                ) / dist_to_ref_sensor

                # Combine the gradients and scale by the inverse of the wave velocity
                dxdydz = (
                    (grad_current_sensor - grad_ref_sensor)
                    / self.wave_velocity
                    * event.detected_by.sampling_frequency
                )
                jac.append(dxdydz)

        return np.array(jac)

    def residuals_hyper(self, x, events, ref_events=None):
        equations = []
        if ref_events is None:
            ref_events = [events[0]]

        for ref_event in ref_events:
            for event in events:
                if event == ref_event:
                    continue
                observed_sample_difference = (
                    event.time * event.detected_by.sampling_frequency
                    - ref_event.time * ref_event.detected_by.sampling_frequency
                )
                distance_difference = np.linalg.norm(
                    x - event.detected_by.location
                ) - np.linalg.norm(x - ref_event.detected_by.location)
                theoretical_time_difference = distance_difference / self.wave_velocity
                theoretical_sample_difference = (
                    theoretical_time_difference * event.detected_by.sampling_frequency
                )
                equations.append(
                    theoretical_sample_difference - observed_sample_difference
                )

        return np.array(equations).ravel()

    def cost(self, x, features, ref_events=None, lamb=99):
        res = self.residuals_hyper(x, features, ref_events=ref_events)
        reg = np.linalg.norm(x)
        return res + lamb * reg

    def jacobian_cost(self, x, features, ref_events=None, lamb=0):
        jac_res = self.jacobian_hyper(x, features, ref_events=ref_events)
        jac_reg = x / np.linalg.norm(x)
        return jac_res + lamb * jac_reg

    def predict(
        self,
        matched_event,
        initial_guess=None,
        ref_events=None,
        lamb=0,
        lower_bounds=np.array([-3, -3, -1e-4]),
        upper_bounds=np.array([3, 3, 1e4]),
    ):

        if initial_guess is None:
            initial_guess = super().predict(
                matched_event,
                initial_guess=initial_guess,
                lower_bounds=lower_bounds,
                upper_bounds=upper_bounds,
            )

        result = least_squares(
            self.cost,
            initial_guess,
            args=([matched_event.events, ref_events, lamb]),
            method="trf",
            xtol=1e-12,
            ftol=1e-12,
            max_nfev=1e3,
            bounds=(lower_bounds, upper_bounds),
            jac=self.jacobian_cost,
        )
        if result.success:
            result = result.x
        else:
            print(result)
            result = initial_guess
        return np.array(result)

import numpy as np
from localizers.base import BaseLocalizer
from scipy.linalg import null_space, pinv


class VanillaTrilateration(BaseLocalizer):
    def __init__(self, sensor_locations, wave_velocity=340):
        super().__init__(sensor_locations, wave_velocity)

    def predict(self, distances, weights=None):
        if weights is None:
            weights = np.eye(len(distances))

        P = self.sensor_locations.T
        S = np.array(distances)
        W = np.array(weights)

        A = []
        b = []
        for i in range(len(P[0])):
            x, y, z = P[:, i]
            s = S[i]
            A.append([1, -2 * x, -2 * y, -2 * z])
            b.append([s**2 - x**2 - y**2 - z**2])

        A = np.array(A)
        b = np.array(b)

        if len(P[0]) == 3:
            Xp = np.linalg.lstsq(A, b, rcond=None)[0]
            xp = Xp[1:]
            Z = null_space(A)
            z = Z[1:]

            a2 = z[0] ** 2 + z[1] ** 2 + z[2] ** 2
            a1 = 2 * (z[0] * xp[0] + z[1] * xp[1] + z[2] * xp[2]) - Z[0]
            a0 = xp[0] ** 2 + xp[1] ** 2 + xp[2] ** 2 - Xp[0]
            p = [a2, a1, a0]
            t = np.roots(p)

            N1 = Xp + t[0] * Z
            N2 = Xp + t[1] * Z
            return N1[1:], N2[1:]

        C = W.T @ W
        if not np.array_equal(W, np.diag(np.ones(len(W)))):
            Xpdw = np.linalg.inv(A.T @ C @ A) @ A.T @ C @ b
        else:
            Xpdw = pinv(A) @ b
        N1 = Xpdw
        N2 = N1
        return N1[1:], N2[1:]

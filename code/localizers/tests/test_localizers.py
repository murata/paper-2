import unittest

import numpy as np
from feelings import *
from localizers.tdoa import VanillaTDOAMultilateration
from localizers.tof import VanillaTrilateration


class TestLocalizerPerformance(unittest.TestCase):
    def setUp(self):
        self.sensor_radius = 4
        self.wave_velocity = 343
        self.lower_bounds = np.array([-5, -5, 0])
        self.upper_bounds = np.array([5, 5, 2])
        self.num_repeat = 20

    def run_localizer_test(self, num_sensors, sigma, lamb):
        network = get_random_network(
            num_sensors, self.sensor_radius, center=(0, 0), sampling_rate=48_000
        )
        target_location = get_random_target_location(
            self.lower_bounds * 0.8, self.upper_bounds * 0.8
        )
        cov = sigma**2 * np.eye(len(network)) / np.square(network[0].sampling_frequency)
        result = evaluate_network(
            network,
            self.wave_velocity,
            target_location,
            lamb=lamb,
            lower_bounds=self.lower_bounds,
            upper_bounds=self.upper_bounds,
            num_repeat=self.num_repeat,
            mean=np.zeros(len(network)),
            cov=cov,
        )
        return result

    def test_localizer_case_1(self):
        result = self.run_localizer_test(num_sensors=10, sigma=1, lamb=0)
        self.assertEqual(
            len(result),
            self.num_repeat + 1,
            f"Output shape is not equal to num_sensors+1",
        )

        e = np.linalg.norm(
            np.vstack(
                (result.ideal_location[0:2] - result.target_location[0:2]).to_numpy()
            ),
            axis=1,
        )
        self.assertLess(
            e.mean(), 0.5, "Mean (ideal) error is greater than expected for case 1"
        )

        e = np.linalg.norm(
            np.vstack(
                (
                    result.estimated_location[0:2] - result.target_location[0:2]
                ).to_numpy()
            ),
            axis=1,
        )
        self.assertLess(e.mean(), 1, "Mean error is greater than expected for case 1")


class TestLocalizers(unittest.TestCase):
    def setUp(self):
        self.sensor_locations = np.array(
            [[0, 0, 0], [10, 0, 0], [0, 10, 0], [0, 0, 10]]
        )
        self.target_location = np.array([5, 5, 5])
        self.wave_velocity = 340  # in m/s
        self.event_time = 2  # Arbitrary event time (t_0)
        self.distances = np.linalg.norm(
            self.sensor_locations - self.target_location, axis=1
        )
        self.flight_times = self.distances / self.wave_velocity
        self.time_of_arrival = self.flight_times + self.event_time

    def test_trilateration(self):
        trilateration_localizer = VanillaTrilateration(
            self.sensor_locations, wave_velocity=self.wave_velocity
        )
        trilateration_result = trilateration_localizer.predict(self.distances)
        np.testing.assert_array_almost_equal(
            trilateration_result[0].ravel(),
            self.target_location.ravel(),
            decimal=1,
            err_msg="Trilateration localization failed",
        )

    # def test_tdoa_multilateration(self):
    #     tdoa_localizer = VanillaTDOAMultilateration(
    #         self.sensor_locations, wave_velocity=self.wave_velocity
    #     )
    #     tdoa_result = tdoa_localizer.predict(self.time_of_arrival)
    #     np.testing.assert_array_almost_equal(
    #         tdoa_result,
    #         self.target_location,
    #         decimal=1,
    #         err_msg="TDOA Multilateration localization failed",
    #     )


if __name__ == "__main__":
    unittest.main()

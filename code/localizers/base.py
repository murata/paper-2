import numpy as np
from sklearn.base import BaseEstimator
from utils import Security


class BaseLocalizer(BaseEstimator):
    def __init__(self, sensor_locations, wave_velocity=340):
        if sensor_locations.shape[0] > sensor_locations.shape[1]:
            self._sensor_locations = sensor_locations
        else:
            ValueError("Too few sensors")
        self._wave_velocity = wave_velocity
        self._id = Security.new_uuid()

    @property
    def id(self):
        return self._id

    @property
    def sensor_locations(self):
        return self._sensor_locations

    @property
    def wave_velocity(self):
        return self._wave_velocity

    @wave_velocity.setter
    def wave_velocity(self, value):
        self._wave_velocity = value

    def fit(self, X, y=None):
        # This method can be implemented if fitting is required for the localization algorithm
        return self

    def predict(self, features, lamb=0):
        raise NotImplementedError("This method should be implemented by subclasses.")

    def inverse(self, target_location):
        raise NotImplementedError("This method should be implemented by subclasses")

    def residuals(self, features):
        raise NotImplementedError("This method should be implemented by subclasses.")

    def jacobian(self, features):
        raise NotImplementedError("This method should be implemented by subclasses")

    @staticmethod
    def evaluate(x, y, axis=0):
        return np.linalg.norm(np.squeeze(x) - np.squeeze(y), axis=axis)

    def __hash__(self):
        return hash(self.id)

    def __eq__(self, other):
        if not isinstance(other, BaseLocalizer):
            return NotImplemented
        return self.id == other.id

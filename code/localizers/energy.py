import numpy as np
from scipy.optimize import least_squares


class HeuristicLocalization:
    def __init__(self, sensors, event):
        self.sensors = sensors
        self.event = event

    def localize(self):
        # Extract locations from sensors
        L = np.array([sensor.location for sensor in self.sensors]).T  # 2xN matrix

        # Use the energies from the event
        e = self.event.energies  # Nx1 vector

        # Calculate the weighted sum of locations
        estimated_location = L @ e / np.sum(e)

        return estimated_location


class EnergyBasedMultilateration:
    def __init__(self, sensors, event, alpha, beta, initial_guess=None):
        self.sensors = sensors
        self.event = event
        self.alpha = alpha
        self.beta = beta
        self.initial_guess = (
            initial_guess
            if initial_guess is not None
            else np.mean([sensor.location for sensor in self.sensors], axis=0)
        )

    def energy_to_distance(self, energy):
        """
        Convert energy measurement to an estimated distance.
        This function uses a logarithmic model.

        :param energy: The energy measurement from a sensor.
        :return: The estimated distance to the event.
        """
        return self.alpha * np.log(energy * self.beta)

    def localize(self):
        """
        Localize the event based on estimated distances from sensor energy readings.

        :return: The estimated location of the event.
        """

        # Define the system of nonlinear equations for energy-based multilateration
        def multilateration_equations(x, sensors, energies):
            equations = []
            for sensor, energy in zip(sensors, energies):
                estimated_distance = self.energy_to_distance(energy)
                actual_distance = np.linalg.norm(x - sensor.location)
                equations.append(actual_distance - estimated_distance)
            return equations

        # Solve the nonlinear system of equations
        result = least_squares(
            multilateration_equations,
            self.initial_guess,
            args=(self.sensors, self.event.energies),
        )

        return result.x  # Estimated position

import unittest

from trackers.types import BaseTarget, Location, PlaybackTarget, Pose, Track


class TestBaseTarget(unittest.TestCase):
    def setUp(self):
        self.location = Location(0, 0, 0)
        self.velocity = Location(
            0, 0, 0
        )  # Assuming velocity uses the Location class for simplicity
        self.base_target = BaseTarget(location=self.location, velocity=self.velocity)

    def test_base_target_initial_state(self):
        """Test the initial state of BaseTarget."""
        self.assertEqual(self.base_target.pose.location, self.location)
        self.assertEqual(self.base_target.pose.velocity, self.velocity)

    def test_track_initialization(self):
        """Test track initialization in BaseTarget."""
        self.assertIsInstance(self.base_target.track, Track)
        self.assertEqual(
            len(self.base_target.track), 1
        )  # Initially, track should have one pose


class TestPlaybackTarget(unittest.TestCase):
    def setUp(self):
        self.poses = [
            Pose(Location(1, 0, 0), Location(0, 1, 0)),
            Pose(Location(2, 0, 0), Location(0, 2, 0)),
            Pose(Location(3, 0, 0), Location(0, 3, 0)),
        ]
        self.playback_target = PlaybackTarget(poses=self.poses)

    def test_initial_current_index(self):
        """Test the initial index of PlaybackTarget."""
        self.assertEqual(self.playback_target.current_idx, 0)

    def test_update_method(self):
        """Test the update method in PlaybackTarget."""
        self.playback_target.update()
        self.assertEqual(self.playback_target.current_idx, 1)
        self.assertEqual(self.playback_target.pose, self.poses[1])

    def test_update_until_end(self):
        """Test updating until the end of the pose list without going out of bounds."""
        for _ in range(len(self.poses) - 1):
            self.playback_target.update()
        # Trying to update beyond the last index should not change the index
        self.playback_target.update()  # This should not increment the index anymore
        self.assertEqual(self.playback_target.current_idx, 3)
        self.assertEqual(self.playback_target.pose, self.poses[3])

    def test_edge_case_empty_poses(self):
        """Test initializing PlaybackTarget with an empty list of poses."""
        with self.assertRaises(ValueError):
            PlaybackTarget(poses=[])


class TestTrack(unittest.TestCase):
    def setUp(self):
        self.poses = [
            Pose(Location(1, 2, 3), Location(0.01, 0.02, 0.03)),
            Pose(Location(4, 5, 6), Location(0.04, 0.05, 0.06)),
            Pose(Location(7, 8, 9), Location(0.07, 0.08, 0.09)),
        ]
        self.track = Track(poses=[])

    def test_add_pose(self):
        """Test adding poses to the track."""
        for pose in self.poses:
            self.track.add_pose(pose)
        self.assertEqual(len(self.track), 3)

    def test_remove_pose(self):
        """Test removing a pose from the track."""
        for pose in self.poses:
            self.track.add_pose(pose)
        self.track.remove_pose(self.poses[0])
        self.assertEqual(len(self.track), 2)
        self.assertNotEqual(self.track.poses[0], self.poses[0])

    def test_remove_nonexistent_pose(self):
        """Test removing a pose that is not in the track."""
        self.track.add_pose(self.poses[0])
        with self.assertRaises(ValueError):
            self.track.remove_pose(self.poses[1])  # Attempt to remove a pose not added

    def test_empty_track_removal(self):
        """Test removing a pose from an empty track."""
        with self.assertRaises(ValueError):
            self.track.remove_pose(
                self.poses[0]
            )  # Ensure this raises an error or handles gracefully

    def test_iterate_poses(self):
        """Test iteration over track poses."""
        for pose in self.poses:
            self.track.add_pose(pose)
        for idx, track_pose in enumerate(self.track):
            self.assertEqual(track_pose, self.poses[idx])

    def test_num_poses(self):
        """Test num_poses property."""
        for pose in self.poses:
            self.track.add_pose(pose)
        self.assertEqual(len(self.track), 3)

    def test_get_item(self):
        """Test __getitem__ functionality."""
        for pose in self.poses:
            self.track.add_pose(pose)
        self.assertEqual(self.track[1], self.poses[1])

    def test_out_of_bounds_access(self):
        """Test accessing an index that is out of bounds."""
        self.track.add_pose(self.poses[0])
        with self.assertRaises(IndexError):
            _ = self.track[1]  # Accessing out of bounds index


if __name__ == "__main__":
    unittest.main()

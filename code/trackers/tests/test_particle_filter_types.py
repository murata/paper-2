import unittest
from uuid import uuid4

import numpy as np
from trackers.particle_filter_types import Particle, Particles


class Particle:
    def __init__(self, position, velocity, weight=1.0):
        self.position = position
        self.velocity = velocity
        self.weight = weight
        self.id = str(uuid4())

    @property
    def state(self):
        return np.hstack([self.position, self.velocity])


class Particles:
    def __init__(self):
        self.particles = []
        self.id = str(uuid4())

    def __len__(self):
        return len(self.particles)

    def add_particle(self, particle):
        self.particles.append(particle)

    def remove_particle(self, particle):
        if particle in self.particles:
            self.particles.remove(particle)
        else:
            raise ValueError("Particle not found in the list")

    def normalize_weights(self, recover_strategy=None):
        total_weight = sum(p.weight for p in self.particles)
        if total_weight == 0:
            if recover_strategy == "uniform":
                uniform_weight = 1 / len(self.particles)
                for p in self.particles:
                    p.weight = uniform_weight
            else:
                raise ValueError("Total weight of particles cannot be zero")
        else:
            for p in self.particles:
                p.weight /= total_weight

    def find_particle_by_id(self, particle_id):
        for p in self.particles:
            if p.id == particle_id:
                return p
        return None


class TestParticle(unittest.TestCase):
    def test_initialization(self):
        """Particles should initialize with the correct state, default weight, and a unique ID."""
        position = np.array([1.0, 2.0])
        velocity = np.array([0.1, 0.2])
        particle = Particle(position=position, velocity=velocity)
        np.testing.assert_array_equal(particle.position, position)
        np.testing.assert_array_equal(particle.velocity, velocity)
        self.assertEqual(particle.weight, 1.0)
        self.assertIsInstance(particle.id, str)

    def test_state_property(self):
        """The state property should return a concatenated array of position and velocity."""
        position = np.array([1.0, 2.0])
        velocity = np.array([0.1, 0.2])
        particle = Particle(position=position, velocity=velocity)
        expected_state = np.hstack([position, velocity])
        np.testing.assert_array_equal(particle.state, expected_state)

    def test_id_uniqueness(self):
        """Particle IDs should be unique for each instance."""
        particles = [
            Particle(np.array([1.0, 2.0]), np.array([0.1, 0.2])) for _ in range(100)
        ]
        ids = {p.id for p in particles}
        self.assertEqual(len(ids), 100, "Particle IDs are not unique.")


class TestParticles(unittest.TestCase):
    def setUp(self):
        self.particles = Particles()
        self.particle1 = Particle(
            position=np.array([1, 2]), velocity=np.array([0.1, 0.2])
        )
        self.particle2 = Particle(
            position=np.array([3, 4]), velocity=np.array([0.3, 0.4])
        )

    def test_initialization(self):
        """Particles container should initialize empty with a unique ID."""
        self.assertIsInstance(self.particles, Particles)
        self.assertIsInstance(self.particles.id, str)
        self.assertEqual(len(self.particles), 0)

    def test_add_and_remove_particle(self):
        """Adding and removing particles should correctly update the container."""
        self.particles.add_particle(self.particle1)
        self.particles.add_particle(self.particle2)
        self.assertEqual(len(self.particles), 2)

        self.particles.remove_particle(self.particle1)
        self.assertEqual(len(self.particles), 1)
        self.assertNotIn(self.particle1, self.particles.particles)

        with self.assertRaises(ValueError):
            self.particles.remove_particle(
                self.particle1
            )  # Test removing the same particle twice

        self.particles.remove_particle(self.particle2)
        self.assertEqual(len(self.particles), 0)

    def test_normalize_weights(self):
        """Weights should normalize correctly, and total normalized weights should sum to 1."""
        self.particle1.weight = 2.0
        self.particle2.weight = 8.0
        self.particles.add_particle(self.particle1)
        self.particles.add_particle(self.particle2)

        self.particles.normalize_weights()
        expected_weights = [0.2, 0.8]
        np.testing.assert_allclose(
            [p.weight for p in self.particles.particles], expected_weights
        )

        self.assertAlmostEqual(sum(p.weight for p in self.particles.particles), 1.0)

    def test_normalize_weights_no_particles(self):
        """Normalizing an empty particle list."""
        particles = Particles()
        with self.assertRaises(ValueError):
            particles.normalize_weights()

    def test_find_particle_by_id(self):
        """Finding a particle by ID should return the correct particle or None if not found."""
        self.particles.add_particle(self.particle1)
        self.particles.add_particle(self.particle2)
        found_particle = self.particles.find_particle_by_id(self.particle1.id)
        self.assertEqual(found_particle, self.particle1)
        self.assertIsNone(self.particles.find_particle_by_id("non_existent_id"))


if __name__ == "__main__":
    unittest.main()

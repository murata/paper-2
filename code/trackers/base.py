from typing import Any, Callable, List, Optional

import numpy as np
from sklearn.base import BaseEstimator
from trackers.particle_filter_types import Particle, Particles
from trackers.types import Pose
from utils import Security


class BaseTracker(BaseEstimator):
    """Base class for all tracker objects, providing basic functionalities such as ID management and common interfaces."""

    def __init__(self):
        """Initialize the tracker with a unique ID."""
        self._id: str = Security.new_uuid()

    @property
    def id(self) -> str:
        """Returns the unique ID of the tracker."""
        return self._id

    def fit(self, X: Any, y: Optional[Any] = None) -> "BaseTracker":
        """Fit the tracker on the data. Placeholder for compatibility with scikit-learn's estimator interface."""
        return self

    def predict(self, features: Any, lamb: float = 0) -> Any:
        """Predict the next state based on the given features. Must be implemented by subclasses."""
        raise NotImplementedError("This method should be implemented by subclasses.")

    @staticmethod
    def evaluate(x: np.ndarray, y: np.ndarray, axis: int = 0) -> np.ndarray:
        """Calculate the Euclidean distance between two sets of points."""
        return np.linalg.norm(np.squeeze(x) - np.squeeze(y), axis=axis)

    def __hash__(self) -> int:
        """Return the hash based on the unique ID."""
        return hash(self.id)

    def __eq__(self, other: object) -> bool:
        """Check equality with another BaseTracker instance."""
        if not isinstance(other, BaseTracker):
            return NotImplemented
        return self.id == other.id


class ParticleFilter(BaseTracker):
    """Implementation of a particle filter for state estimation in tracking systems."""

    def __init__(
        self,
        initial_states: List[np.ndarray],
        num_particles: int = 1000,
        state_transition_func: Optional[Callable[[Pose, Any], Pose]] = None,
        measurement_func: Optional[Callable[[Pose], np.ndarray]] = None,
    ):
        """Initialize the particle filter with initial states and particle count."""
        super().__init__()
        self.particles = Particles(
            _particles=[
                Particle(pose=Pose.from_vector(state, [0] * len(state)))
                for state in initial_states
            ]
            * num_particles
        )
        self.num_particles = num_particles
        self.state_transition = state_transition_func
        self.measurement_func = measurement_func

    def predict(self, control_input: Optional[Any] = None, **kwargs) -> None:
        """Predict the next state of each particle using the state transition function."""
        for i, particle in enumerate(self.particles):
            updated_pose = self.state_transition(particle.pose, control_input, **kwargs)
            self.particles._particles[i].pose = updated_pose

    def update(self, measurement: np.ndarray, **kwargs) -> None:
        """Update the weights of particles based on the measurement using the measurement function."""
        for particle in self.particles:
            predicted_measurement = self.measurement_func(particle.pose, **kwargs)
            particle.weight *= self.likelihood(measurement, predicted_measurement)

    def resample(self) -> None:
        """Resample the particles based on their weights to focus on high-probability regions."""
        weights = self.particles.weights
        indices = np.random.choice(
            self.num_particles, size=self.num_particles, p=weights, replace=True
        )
        self.particles._particles = [
            self.particles._particles[index] for index in indices
        ]
        for particle in self.particles:
            particle.weight = 1.0 / self.num_particles

    def estimate(self) -> np.ndarray:
        """Estimate the current state as the weighted average of the particles' states."""
        weighted_poses = np.array(
            [particle.pose.location.to_numpy() for particle in self.particles]
        )
        weights = np.array([particle.weight for particle in self.particles])
        return np.average(weighted_poses, weights=weights, axis=0)

    def likelihood(
        self, measurement: np.ndarray, predicted_measurement: np.ndarray
    ) -> float:
        """Calculate the likelihood of a measurement given the predicted measurement."""
        return np.exp(-np.square(measurement - predicted_measurement))

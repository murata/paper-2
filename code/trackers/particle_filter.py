from typing import Callable, Dict

import numpy as np
import scipy.stats as ss
from trackers.base import BaseTracker
from trackers.particle_filter_types import *
from trackers.types import Location


class ParticleFilter(BaseTracker):
    """
    A class representing a particle filter tracker.

    The particle filter is a probabilistic filtering algorithm used for estimating the state of a dynamic system
    given a sequence of measurements. It maintains a set of particles, each representing a possible state of the system,
    and updates their weights based on the likelihood of the measurements. The particles are then resampled based on
    their weights to generate a new set of particles for the next iteration.

    Args:
        num_particles (int): The number of particles to use in the filter. Default is 1000.
        state_transition_func (Callable): The state transition function that predicts the next state of each particle.
        measurement_func (Callable): The measurement function that calculates the predicted measurement for each particle.

    Attributes:
        particles (Particles): The set of particles representing the possible states of the system.
        state_transition (Callable): The state transition function.
        measurement_func (Callable): The measurement function.

    """

    def __init__(
        self,
        num_particles: int = 1000,
        state_transition_func: Callable = None,
        measurement_func: Callable = None,
        **kwargs: Dict[str, any],
    ):
        super().__init__()
        self.particles = None
        self.init_particles(num_particles)
        self.state_transition = state_transition_func
        self.measurement_func = measurement_func

    def init_particles(self, num_particles: int) -> Particles:
        self.particles = Particles()
        for _ in range(num_particles):
            x = ss.norm.rvs(loc=0, scale=0.35)
            y = ss.norm.rvs(loc=0, scale=0.35)
            z = ss.norm.rvs(loc=0, scale=0.35)
            initial_pose = Pose(location=Location(x, y, z), velocity=Location())
            particle = Particle(pose=initial_pose)
            self.particles.add_particle(particle)

        self.particles.normalize_weights()

    def predict(self, control_input=None, **kwargs):
        """
        Predicts the next state of each particle based on the control input.

        Args:
            control_input: The control input for the prediction step.
            **kwargs: Additional keyword arguments.

        Returns:
            None
        """
        for i in range(len(self.particles)):
            self.particles[i] = self.state_transition(
                self.particles[i], control_input, **kwargs
            )

    def update(self, measurement, **kwargs):
        """
        Updates the particle filter with a new measurement.

        Args:
            measurement: The new measurement to update the filter.
            **kwargs: Additional keyword arguments for the measurement function.

        Returns:
            None
        """
        for i in range(len(self.particles)):
            predicted_measurement = self.measurement_func(self.particles[i], **kwargs)
            likelihood = self.likelihood(predicted_measurement, measurement)
            # print(self.particles[i].weight, likelihood, self.particles[i].weight * likelihood)
            self.particles[i].weight *= likelihood

    def resample(self, method: str = "multinomial"):
        """
        Resamples the particles based on their weights using specified resampling scheme.

        Supported methods are:
        - 'multinomial': Default. Resamples with replacement based on the weights.
        - 'systematic': Performs systematic resampling.
        - 'stratified': Performs stratified resampling.

        Args:
            method (str): The resampling scheme to use. Defaults to 'multinomial'.

        Returns:
            None
        """
        weights = self.particles.weights
        num_particles = len(self.particles)

        if sum(weights) == 0:
            raise ValueError("All weights are zero. Cannot resample.")

        weights = self.particles.weights
        num_particles = len(self.particles)

        if method == "multinomial":
            indices = np.random.choice(
                num_particles, size=num_particles, p=weights, replace=True
            )
        elif method == "systematic":
            indices = self.systematic_resampling(weights)
        elif method == "stratified":
            indices = self.stratified_resampling(weights)
        else:
            raise ValueError(f"Unknown resampling method: {method}")

        resampled_particles = [self.particles[i] for i in indices]
        self.particles._particles = (
            resampled_particles  # Directly setting the private variable to maintain IDs
        )

    def systematic_resampling(self, weights: np.ndarray) -> np.ndarray:
        """
        Systematic resampling implementation.

        Args:
            weights (np.ndarray): The weights of the particles.

        Returns:
            np.ndarray: The resampled indices.
        """
        num_particles = len(weights)
        positions = (np.arange(num_particles) + np.random.random()) / num_particles
        indexes = np.zeros(num_particles, "i")
        cumulative_sum = np.cumsum(weights)
        i, j = 0, 0
        while i < num_particles:
            if positions[i] < cumulative_sum[j]:
                indexes[i] = j
                i += 1
            else:
                j += 1
        return indexes

    def stratified_resampling(self, weights: np.ndarray) -> np.ndarray:
        """
        Stratified resampling implementation.

        Args:
            weights (np.ndarray): The weights of the particles.

        Returns:
            np.ndarray: The resampled indices.
        """
        num_particles = len(weights)
        positions = (
            np.linspace(0, 1, num_particles, endpoint=False)
            + np.random.rand(num_particles) / num_particles
        )
        indexes = np.zeros(num_particles, "i")
        cumulative_sum = np.cumsum(weights)
        i, j = 0, 0
        while i < num_particles:
            if positions[i] < cumulative_sum[j]:
                indexes[i] = j
                i += 1
            else:
                j += 1
        return indexes

    def estimate(self) -> np.ndarray:
        """
        Estimates the weighted pose based on the particles' poses and weights.

        Returns:
            np.ndarray: The estimated weighted pose.
        """
        weighted_pose = Pose(location=Location(), velocity=Location())
        self.particles.normalize_weights()
        for particle in self.particles:
            weighted_pose.location += particle.pose.location.scale(particle.weight)
            weighted_pose.velocity += particle.pose.velocity.scale(particle.weight)

        return weighted_pose

    def likelihood(self, particle: Particle, measurement) -> float:
        """
        Calculates the likelihood of a particle given a predicted measurement.

        Parameters:
            particle (Particle): The particle for which to calculate the likelihood.
            measurement (Any): The predicted measurement.

        Returns:
            float: The likelihood of the particle given the predicted measurement.
        """
        distance_error = particle.pose.location.distance(measurement)
        sigma = 1
        likelihood = ss.norm.pdf(distance_error, loc=0, scale=sigma)
        return likelihood

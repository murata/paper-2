from dataclasses import dataclass, field
from typing import Iterator, List, Optional

import numpy as np
from trackers.types import Pose
from utils import Security


@dataclass
class Particle:
    """
    Represents a particle in a particle filter simulation.

    Attributes:
        pose (Pose): The state of the particle, typically including position and possibly orientation.
        weight (float): The weight of the particle, representing its likelihood or importance in the filter.
        id (str): A unique identifier for the particle, automatically generated.
    """

    pose: Pose
    weight: float = 1.0
    id: str = field(default_factory=lambda: str(Security.new_uuid()), init=False)

    def __eq__(self, other: object) -> bool:
        """
        Checks equality based on the unique ID of the particles, allowing for comparison of Particle instances.

        Args:
            other (object): Another particle or object to compare against.

        Returns:
            bool: True if both are Particle instances with the same ID, False otherwise.
        """
        if not isinstance(other, Particle):
            return NotImplemented
        return self.id == other.id

    def __str__(self) -> str:
        """
        Returns a human-readable string representation of the Particle, showing its ID, weight, and pose.
        """
        return f"Particle(ID={self.id}, Weight={self.weight:.4f}, Pose={self.pose})"

    def __repr__(self) -> str:
        """
        Returns an unambiguous string representation of the Particle, suitable for debugging.
        """
        return f"Particle(pose={self.pose!r}, weight={self.weight}, id='{self.id}')"

    @property
    def position(self) -> np.ndarray:
        """
        Retrieves the positional component of the particle's pose.

        Returns:
            np.ndarray: The position of the particle as a NumPy array.
        """
        return self.pose.location

    @property
    def velocity(self) -> np.ndarray:
        """
        Retrieves the velocity component of the particle's pose.

        Returns:
            np.ndarray: The velocity of the particle as a NumPy array.
        """
        return self.pose.velocity


@dataclass
class Particles:
    """
    A collection of particle instances, typically used in particle filter implementations.

    Attributes:
        id (str): Unique identifier for the collection, automatically generated.
        _particles (List[Particle]): A private list of Particle instances.
    """

    id: str = field(default_factory=lambda: str(Security.new_uuid()), init=False)
    _particles: List[Particle] = field(default_factory=list, repr=False)

    def __repr__(self) -> str:
        """Returns a concise string representation for debugging purposes."""
        return f"Particles(id='{self.id}', num_particles={len(self._particles)})"

    def __str__(self) -> str:
        """Returns a readable string representation of the Particles instance."""
        info = "\n".join(str(particle) for particle in self._particles)
        return f"Particles '{self.id}' with particles:\n{info}"

    def __iter__(self) -> Iterator[Particle]:
        """Allows iteration over the collection of particles."""
        return iter(self._particles)

    def __len__(self) -> int:
        """Returns the number of particles in the collection."""
        return len(self._particles)

    def __getitem__(self, index: int) -> Particle:
        """Allows indexed access to the particles."""
        return self._particles[index]

    def __setitem__(self, index: int, value: Particle) -> None:
        """Allows indexed assignment of particles."""
        self._particles[index] = value

    def __eq__(self, other: object) -> bool:
        """
        Checks equality with another Particles instance based on unique IDs.

        Args:
            other (object): Another Particles instance to compare against.

        Returns:
            bool: True if both instances have the same ID, False otherwise.
        """
        if not isinstance(other, Particles):
            return NotImplemented
        return self.id == other.id

    def add_particle(self, particle: Particle) -> None:
        """
        Adds a particle to the collection.

        Args:
            particle (Particle): The Particle instance to be added.

        Raises:
            ValueError: If the object added is not an instance of Particle.
        """
        if not isinstance(particle, Particle):
            raise ValueError("Only Particle objects can be added.")
        self._particles.append(particle)

    def remove_particle(self, particle: Particle) -> None:
        """
        Removes a particle from the collection.

        Args:
            particle (Particle): The Particle instance to be removed.
        """
        self._particles.remove(particle)

    def find_particle_by_id(self, particle_id: str) -> Optional[Particle]:
        """
        Finds a particle by its unique ID.

        Args:
            particle_id (str): The unique ID of the particle.

        Returns:
            Optional[Particle]: The Particle instance if found, None otherwise.
        """
        for particle in self._particles:
            if particle.id == particle_id:
                return particle
        return None

    def normalize_weights(self) -> None:
        """
        Normalize the weights of the particles.

        Raises:
            AttributeError: If there are no particles.
            ValueError: If the weights are zero or invalid.

        Returns:
            None
        """
        if len(self._particles) == 0:
            raise AttributeError(
                "We need at least one particle to complete this operation."
            )

        # Check if all weights are zero, nan, or inf
        if np.allclose(np.sum(self.weights), 0, atol=1e-5):
            raise ValueError(f"Zero weights detected {np.sum(self.weights):.2f}")
        if np.any(np.isnan(self.weights)):
            raise ValueError(
                f"NaN weights detected {np.count_nonzero(np.isnan(self.weights))}"
            )
        if np.any(np.isinf(self.weights)):
            raise ValueError(f"Inf weights detected {np.isinf(self.weights)}")

        total_weight = np.sum(self.weights)
        normalized_weights = self.weights / total_weight

        # Assign the normalized weights back to the particles
        for index in range(len(self._particles)):
            self._particles[index].weight = normalized_weights[index]

    def reset_weights(self) -> None:
        """Resets all particle weights to 1.0."""
        for idx in range(len(self._particles)):
            self._particles[idx].weight = 1.0 / len(self._particles)

    @property
    def particles(self) -> List[Particle]:
        """Returns the list of Particle instances."""
        return self._particles

    @property
    def weights(self) -> List[float]:
        """
        Returns the list of weights of all particles.

        Returns:
            List[float]: A list of weights corresponding to each particle.
        """
        return [particle.weight for particle in self._particles]

    @property
    def num_particles(self) -> int:
        """Returns the number of particles."""
        return len(self)

    @property
    def locations(self) -> np.ndarray:
        """
        Returns the locations of all particles as a NumPy array.

        Returns:
            np.ndarray: An array of all particle locations.
        """
        return np.vstack(
            [particle.pose.location.to_numpy() for particle in self._particles]
        )

    @property
    def effective_sample_size(self) -> float:
        """
        Calculate the effective sample size of the particle filter.

        Returns:
            float: The effective sample size.
        """
        w_sq = [particle.weight**2 for particle in self._particles]
        return 1.0 / np.sum(w_sq)


if __name__ == "__main__":
    pass

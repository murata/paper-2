from typing import List

import numpy as np


class Location:
    """Represents a three-dimensional point in space."""

    __slots__ = ["x", "y", "z"]

    def __init__(self, x: float = 0.0, y: float = 0.0, z: float = 0.0) -> None:
        self.x = x
        self.y = y
        self.z = z

    def __add__(self, other) -> "Location":
        """Adds two Location instances or a Location instance with a number, list, or np.array."""
        if isinstance(other, Location):
            return Location(self.x + other.x, self.y + other.y, self.z + other.z)
        elif isinstance(other, (int, float)):
            return Location(self.x + other, self.y + other, self.z + other)
        elif isinstance(other, (list, np.ndarray)):
            if len(other) != 3:
                raise ValueError("List or np.array must have three elements")
            return Location(self.x + other[0], self.y + other[1], self.z + other[2])
        else:
            raise TypeError(
                "Unsupported operand type for +: Location and {}".format(type(other))
            )

    def __sub__(self, other) -> "Location":
        """Subtracts one Location instance or a number, list, or np.array from a Location instance."""
        if isinstance(other, Location):
            return Location(self.x - other.x, self.y - other.y, self.z - other.z)
        elif isinstance(other, (int, float)):
            return Location(self.x - other, self.y - other, self.z - other)
        elif isinstance(other, (list, np.ndarray)):
            if len(other) != 3:
                raise ValueError("List or np.array must have three elements")
            return Location(self.x - other[0], self.y - other[1], self.z - other[2])
        else:
            raise TypeError(
                "Unsupported operand type for -: Location and {}".format(type(other))
            )

    def __radd__(self, other) -> "Location":
        """Handles right addition for commutative addition."""
        return self.__add__(other)

    def __rsub__(self, other) -> "Location":
        """Handles right subtraction."""
        return other.__sub__(self)

    def __eq__(self, other) -> bool:
        """Checks equality with another Location instance based on x, y, and z coordinates."""
        if not isinstance(other, Location):
            return NotImplemented
        return self.x == other.x and self.y == other.y and self.z == other.z

    def __str__(self) -> str:
        """Provides a human-readable string representation of the Location."""
        return f"Location(x={self.x}, y={self.y}, z={self.z})"

    def __repr__(self) -> str:
        """Provides a detailed string representation of the Location, suitable for debugging."""
        return f"Location(x={self.x}, y={self.y}, z={self.z})"

    @classmethod
    def from_vector(cls, vector: List[float]) -> "Location":
        """Creates a Location instance from a given list or array of three elements."""
        return cls(x=vector[0], y=vector[1], z=vector[2])

    def to_numpy(self) -> np.ndarray:
        """Converts the Location instance to a numpy array."""
        return np.array([self.x, self.y, self.z])

    def distance(self, other: "Location") -> float:
        """Calculates the Euclidean distance between two locations."""
        delta = self - other
        return delta.magnitude()

    def magnitude(self) -> float:
        """Returns the magnitude of the vector representing the location."""
        return np.linalg.norm(self.to_numpy())

    def normalize(self) -> "Location":
        """Normalizes the vector representing the location."""
        mag = self.magnitude()
        if mag == 0:
            return self
        return Location(self.x / mag, self.y / mag, self.z / mag)

    def scale(self, scalar: float) -> "Location":
        """Scales the vector by a scalar value."""
        return Location(self.x * scalar, self.y * scalar, self.z * scalar)


class Pose:
    __slots__ = ["location", "velocity", "frame", "time"]

    def __init__(
        self,
        location: Location,
        velocity: Location,
        frame: str = "self",
        time: float = 0.0,
    ) -> None:
        self.location = location
        self.velocity = velocity
        self.frame = frame
        self.time = time

    def __eq__(self, other) -> bool:
        """Checks equality with another Pose instance."""
        if not isinstance(other, Pose):
            return NotImplemented
        return (
            self.location == other.location
            and self.velocity == other.velocity
            and self.frame == other.frame
            and self.time == other.time
        )

    def __str__(self) -> str:
        """Provides a human-readable string representation of the Pose."""
        return f"Location: {self.location}, Velocity: {self.velocity} defined wrt '{self.frame}' at {self.time})"

    def __repr__(self) -> str:
        """Provides a detailed string representation of the Pose, suitable for debugging."""
        return (
            f"Pose(location={self.location!r}, velocity={self.velocity!r}, "
            f"frame='{self.frame}', time={self.time})"
        )

    @classmethod
    def from_vector(
        cls,
        location_vector: List[float],
        velocity_vector: List[float],
        frame: str = "self",
        time: float = 0.0,
    ) -> "Pose":
        """Creates a Pose instance from location and velocity vectors."""
        return cls(
            location=Location.from_vector(location_vector),
            velocity=Location.from_vector(velocity_vector),
            frame=frame,
            time=time,
        )


class Track:
    """Tracks multiple poses of an object over time."""

    __slots__ = ["_poses"]

    def __init__(self, poses: List[Pose] = None) -> None:
        self._poses = poses if poses else []

    def __iter__(self):
        return iter(self._poses)

    def __len__(self) -> int:
        return len(self._poses)

    def __getitem__(self, index: int) -> Pose:
        return self._poses[index]

    def __setitem__(self, index: int, value: Pose) -> None:
        self._poses[index] = value

    def __delitem__(self, index: int) -> None:
        del self._poses[index]

    def __str__(self) -> str:
        """Provides a human-readable string representation of the Track."""
        pose_descriptions = ", ".join(str(pose) for pose in self._poses)
        return f"Track with {len(self._poses)} poses: [{pose_descriptions}]"

    def __repr__(self) -> str:
        """Provides a detailed string representation of the Track, suitable for debugging."""
        pose_reprs = ", ".join(repr(pose) for pose in self._poses)
        return f"Track(poses=[{pose_reprs}])"

    @property
    def poses(self) -> List[Pose]:
        """Returns the list of poses."""
        return self._poses

    def add_pose(self, pose: Pose) -> None:
        """Adds a pose to the track."""
        self._poses.append(pose)

    def remove_pose(self, pose: Pose) -> None:
        """Removes a pose from the track."""
        self._poses.remove(pose)


class BaseTarget:
    """Base class representing a target with a position and velocity, tracked over time."""

    def __init__(
        self, location: Location, velocity: Location, name: str = "self"
    ) -> None:
        """Initializes the target with a location, velocity, and an optional frame name."""
        self._pose = Pose(location=location, velocity=velocity, frame=name, time=0)
        self._track = Track(poses=[self._pose])

    def __str__(self) -> str:
        """Provides a human-readable string representation of the BaseTarget."""
        return f"BaseTarget: {self.pose}"

    def __repr__(self) -> str:
        """Provides a detailed string representation of the BaseTarget, suitable for debugging."""
        return f"BaseTarget(location={self.pose.location}, velocity={self.pose.velocity}, name={self.pose.frame})"

    @property
    def pose(self) -> Pose:
        """Returns the current pose of the target."""
        return self._pose

    @property
    def track(self) -> Track:
        """Returns the track of poses for the target."""
        return self._track


class PlaybackTarget(BaseTarget):
    """A target that can playback through a series of poses based on an index."""

    def __init__(self, poses: List[Pose], current_idx: int = 0) -> None:
        """Initializes a PlaybackTarget with a list of poses and an optional starting index."""
        if not poses:
            raise ValueError("Pose list cannot be empty")

        initial_pose = poses[current_idx]
        super().__init__(
            location=initial_pose.location,
            velocity=initial_pose.velocity,
            name=initial_pose.frame,
        )
        self._poses = poses
        self._current_idx = current_idx
        self._track = Track(poses=poses)

    @property
    def current_idx(self) -> int:
        """Returns the current index of the pose being played back."""
        return self._current_idx

    def update(self) -> None:
        """Updates the pose to the next in the sequence, if available."""
        # Add the current pose to the track
        self._track.add_pose(self.pose)
        # Update to the next pose if available
        if self._current_idx + 1 < len(self._poses):
            self._current_idx += 1
            self._pose = self._poses[self._current_idx]


if __name__ == "__main__":
    pass

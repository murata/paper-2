import argparse
import os

import matplotlib

matplotlib.use("Agg")  # noqa
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from tqdm import tqdm


def main_tracking(fname_tracking, fname_out):
    if not os.path.exists(fname_out):
        os.makedirs(fname_out)

    tracking_df = pd.read_hdf(fname_tracking, key="results")
    print(tracking_df.columns.to_list())
    print(len(tracking_df))

    for exp_id, exp_group in tracking_df.groupby("experiment_idx"):
        for index, tracking_row in tqdm(
            exp_group.iterrows(),
            desc=f"Visualizing Tracking Results for Experiment {exp_id}",
            total=len(exp_group),
        ):
            prior = np.array(tracking_row.prior_particles)
            prior_weights = np.array(tracking_row.prior_weights)
            prior_weights = prior_weights / np.amax(prior_weights)
            posterior = np.array(tracking_row.posterior_particles)
            posterior_weights = np.array(tracking_row.posterior_weights)
            posterior_weights = posterior_weights / np.amax(posterior_weights)

            # Histogram for weights
            fig, ax = plt.subplots(1, figsize=(4, 4))
            ax.hist(
                prior_weights,
                bins=np.linspace(0, 1, 100),
                alpha=0.5,
                label="Prior",
                density=True,
            )
            ax.hist(
                posterior_weights,
                bins=np.linspace(0, 1, 100),
                alpha=0.75,
                label="Posterior",
                density=True,
            )
            ax.legend()
            ax.set_title(f"Time step: {index:03d} [sample]")
            fig.tight_layout()
            fig.savefig(f"{fname_out}/hist-exp{exp_id:03d}-{index:03d}.png")
            plt.close()

            # Plot for sensor and particle locations
            fig, ax = plt.subplots(1, figsize=(4, 4))
            sensor_locations = np.array(tracking_row.sensor_locations)
            ax.plot(
                sensor_locations[:, 0],
                sensor_locations[:, 1],
                "rx",
                label="Sensor",
            )
            ax.plot(
                tracking_row.measured_location[0],
                tracking_row.measured_location[1],
                "bo",
                label="Measurement",
            )
            ax.plot(
                tracking_row.target_location[0],
                tracking_row.target_location[1],
                "gx",
                label="True",
            )

            ax.scatter(
                prior[:, 0],
                prior[:, 1],
                marker=".",
                color="cyan",
                label="Prior",
                alpha=prior_weights,
            )
            ax.scatter(
                posterior[:, 0],
                posterior[:, 1],
                marker="+",
                color="k",
                label="Posterior",
                alpha=posterior_weights,
            )

            ax.set_xlabel("x [m]")
            ax.set_ylabel("y [m]")
            ax.set_xlim([-1, 1])
            ax.set_ylim([-1, 1])
            ax.set_title(f"Localization Result (Case: {index})")
            ax.legend()
            ax.set_aspect("equal")
            fig.tight_layout()
            fig.savefig(f"{fname_out}/tracking-exp{exp_id:03d}-{index:03d}.png")
            plt.close()


def main_uq(fname_in, fname_out):
    if not os.path.exists(fname_out):
        os.makedirs(fname_out)

    results = pd.read_hdf(fname_in, key="results")
    num_sensors = np.array([len(row["network"]) for _, row in results.iterrows()])
    errors = np.vstack(
        (results.estimated_location - results.target_location).to_numpy()
    )
    errors_normed = np.linalg.norm(errors, axis=1)
    random_errors = np.vstack(
        (results.random_location - results.target_location).to_numpy()
    )
    errors_normed = np.linalg.norm(errors, axis=1)
    random_errors_normed = np.linalg.norm(random_errors, axis=1)

    plt.figure(figsize=(8, 6))
    sns.set_theme(style="whitegrid", palette="viridis")
    sns.lineplot(
        x="sigma",
        y=errors_normed,
        hue=num_sensors,
        data=results,
        errorbar=("ci", 90),
    )
    sns.lineplot(
        x="sigma",
        y=random_errors_normed,
        data=results,
        errorbar=("ci", 90),
        label="random",
    )
    plt.ylim(min(errors_normed), max(errors_normed))
    plt.ylabel("Localization Error [m]")
    plt.xlabel("Variance")
    plt.title("Localization Error by Number of Sensors")
    plt.savefig(f"{fname_out}/num_sensors-boxplot.png")
    plt.close()
    print(
        f"Random Errors Desc. Mean: {np.mean(random_errors_normed):2.2f} [m] Var: {np.var(random_errors_normed):2.2f} [m]"
    )


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="")
    parser.add_argument("--uq", type=str, required=True, help="UQ filename")
    parser.add_argument("--tracking", type=str, required=True, help="Tracking filename")
    parser.add_argument("--output", type=str, required=True, help="Output folder name")
    args = parser.parse_args()
    main_uq(args.uq, args.output)
    main_tracking(args.tracking, args.output)

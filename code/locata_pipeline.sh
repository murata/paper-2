#!/bin/bash
set -euxo pipefail
# Check for the correct number of arguments
if [ "$#" -ne 1 ]; then
    echo "Usage: $0 <dataset_name>"
    exit 1
fi

dataset_name=$1
ref=15
dataset_filename="../data/${dataset_name}.tar.gz"

mkdir -p "$dataset_name"
tar -xzvf "$dataset_filename" > /dev/null

# Create output directory
mkdir -p "output-${dataset_name}"
output_spatial="spatial-${dataset_name}.h5"
output_temporal="temporal-${dataset_name}.h5"
output_tracking="tracking-${dataset_name}.h5"

# Run Python scripts with provided arguments
python locata_sigmoid_temporal.py \
    --input "$dataset_name.h5" \
    --output "$output_temporal" \
    --ref $ref

python locata_sigmoid_spatial.py \
    --input "$dataset_name.h5" \
    --output "$output_spatial" \
    --temporal "$output_temporal" \
    --ref $ref

python locata_sigmoid_tracking.py \
    --input "$dataset_name.h5" \
    --output "$output_tracking" \
    --temporal "$output_temporal" \
    --spatial "$output_spatial" \
    --ref $ref

python locata_visualize.py \
    --input "$dataset_name.h5" \
    --output "output-${dataset_name}" \
    --temporal "$output_temporal" \
    --spatial "$output_spatial" \
    --tracking "$output_tracking" \

bash make_video.sh -f "output-${dataset_name}" -p "tracking-" -s ".png" -r 3 -o "output-${dataset_name}/tracking.mp4"
bash make_video.sh -f "output-${dataset_name}" -p "lag-likelihoods-" -s ".png" -r 3 -o "output-${dataset_name}/delay-estimations.mp4"
bash make_video.sh -f "output-${dataset_name}" -p "corr-est-vs-gt-sensor-" -s ".png" -r 3 -o "output-${dataset_name}/correlation.mp4"



echo "All processes completed successfully."

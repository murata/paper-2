#!/bin/bash
set -euxo pipefail
# Check for the correct number of arguments
if [ "$#" -ne 3 ]; then
    echo "Usage: $0 <num_experiments> <num_steps> <output>"
    exit 1
fi

num_experiments=$1
num_steps=$2
output=$3

# Clean up the output directory or create it if it doesn't exist
if [ -d "$output" ]; then
    find "$output" -type f -exec rm {} +
else
    mkdir -p "$output"
fi

# Run the Python scripts to generate data and visualize
python random_network_uncertainty_quantification.py --output "random-network-uncertanity-quantification.h5" --steps "$num_steps"
python random_network_tracking.py --output "random-network-tracking.h5" --experiments "$num_experiments" --steps "$num_steps"
python visualize_uq.py --uq "random-network-uncertanity-quantification.h5" --tracking "random-network-tracking.h5" --output "$output"
echo "All processes completed successfully."

# Generate a video for each experiment
for (( i=0; i<num_experiments; i++ ))
do
    bash make_video.sh -f "$output" -p "hist-exp$(printf "%03d" $i)-" -s ".png" -r 3 -o "$output/hist-exp$(printf "%03d" $i).mp4"
    bash make_video.sh -f "$output" -p "tracking-exp$(printf "%03d" $i)-" -s ".png" -r 3 -o "$output/tracking-exp$(printf "%03d" $i).mp4"
done

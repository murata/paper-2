import time
from datetime import datetime, timedelta

import numpy as np


class TimeBase:
    @classmethod
    def counter(cls):
        return time.perf_counter()

    @classmethod
    def sleep(cls, duration):
        return time.sleep(duration)


class Time(TimeBase):
    @classmethod
    def from_list(cls, **kwargs):
        # year=0, month=0, day=0, hour=0, minute=0, second=0, microsecond=0
        return datetime(**kwargs)

    @classmethod
    def from_string(cls, string, format="%Y-%m-%dT%H:%M:%S"):
        return datetime.strptime(string, format)

    @classmethod
    def from_timestamp(cls, ts):
        return datetime.fromtimestamp(ts)

    @classmethod
    def now(cls):
        return datetime.now()

    @classmethod
    def timestamp(cls, milliseconds=True):
        now = datetime.timestamp(Time.now())
        if milliseconds:
            return int(now * 1000)
        return int(now)

    @classmethod
    def duration(cls, **kwargs):
        return timedelta(**kwargs)


class TimeNative(TimeBase):
    @classmethod
    def from_list(
        cls, year=0, month=0, day=0, hour=0, minute=0, second=0, microsecond=0
    ):
        string = f"{year:04d}-{month:02d}-{day:02d}T{hour:02d}:{minute:02d}:{second:02d}.{microsecond:09d}"
        return np.datetime64(string)

    @classmethod
    def from_string(cls, string, format="%Y-%m-%dT%H:%M:%S"):
        return np.datetime64(datetime.strptime(string, format))

    @classmethod
    def from_timestamp(cls, ts):
        return np.datetime64(int(ts), "s")

    @classmethod
    def now(cls):
        return np.datetime64("now")

    @classmethod
    def timestamp(cls, milliseconds=True):
        now = np.datetime64("now")
        if milliseconds:
            return now.astype("datetime64[ms]").astype("int64")
        return now.astype("datetime64[s]").astype("int64")

    @classmethod
    def duration(
        cls,
        days=0,
        hours=0,
        minutes=0,
        seconds=0,
        milliseconds=0,
        microseconds=0,
        nanoseconds=0,
    ):
        total_milliseconds = (
            days * 24 * 60 * 60 + hours * 60 * 60 + minutes * 60 + seconds
        ) * 1000 + milliseconds
        total_microseconds = total_milliseconds * 1000 + microseconds
        total_nanoseconds = total_microseconds * 1000 + nanoseconds
        return np.timedelta64(total_nanoseconds, "ns")

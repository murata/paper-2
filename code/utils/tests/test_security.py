import re
import unittest

from utils import Security


class TestSecurity(unittest.TestCase):
    def test_calculate_md5(self):
        # Test that the output is a valid MD5 hash
        input_str = "hello"
        output, _ = Security.calculate_md5(input_str)
        self.assertTrue(re.match(r"^[a-f0-9]{32}$", output))

    def test_calculate_sha256(self):
        # Test that the output is a valid SHA256 hash
        input_str = "hello"
        output, _ = Security.calculate_sha256(input_str)
        self.assertTrue(re.match(r"^[a-f0-9]{64}$", output))

    def test_new_uuid(self):
        # Test UUID generation
        uuid1 = Security.new_uuid()
        uuid2 = Security.new_uuid()
        self.assertNotEqual(uuid1, uuid2)  # Ensure two generated UUIDs are not the same

    def test_generate_salt(self):
        # Test salt generation
        salt1 = Security.generate_salt()
        salt2 = Security.generate_salt()
        self.assertNotEqual(salt1, salt2)  # Ensure two generated salts are not the same
        self.assertEqual(
            len(salt1), 32
        )  # Check the length of the salt (16 bytes * 2 characters per byte)

    def test_hmac_sha256(self):
        # Test HMAC SHA256
        key = b"secret_key"
        message = "hello"
        hmac1 = Security.hmac_sha256(key, message)
        hmac2 = Security.hmac_sha256(key, message)
        self.assertEqual(
            hmac1, hmac2
        )  # Same key and message should produce the same HMAC


if __name__ == "__main__":
    unittest.main()

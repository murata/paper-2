"""
This module contains a class which is used for miscellaneous purposes.

The misc class contains all the functions and utilities needed for the other
packages and classes, such as providing low-level OS interactions, creating
timestamps etc.
"""

import datetime
import json
import os
import subprocess
import tarfile
import threading

import numpy as np
import scipy.io as sio
import yaml


class _CustomEncoder(json.JSONEncoder):
    """Special json encoder for difficult to serialize type of types"""

    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        elif isinstance(obj, np.floating):
            return float(obj)
        elif isinstance(obj, np.ndarray):
            return obj.tolist()
        elif isinstance(
            obj,
            (
                datetime.date,
                datetime.time,
                datetime.datetime,
                datetime.timedelta,
                datetime.tzinfo,
                datetime.timezone,
            ),
        ):
            return str(obj)
        return json.JSONEncoder.default(self, obj)


class _FileOperations:
    @staticmethod
    def get_tarmembers(file):
        members = None
        with tarfile.open(file, "r:") as tar:
            members = tar.getmembers()
        return members

    @staticmethod
    def make_tarball(source, destination, **kwargs):
        if _FileOperations.check_file_exists(destination):
            tarinfo = _FileOperations.get_tarmembers(destination)
            files_tarball = [file.name for file in tarinfo if file.isfile()]
            files_source = list()
            for root, _, files in os.walk(source, topdown=False):
                if len(files) != 0:
                    if root[0] == "/":
                        root = root[1:]
                    paths = _FileOperations.make_path(root, files)
                    if isinstance(paths, list):
                        files_source.extend(paths)
                    else:
                        files_source.append(paths)

            missing_files = set(files_source) - set(files_tarball)

            for file in missing_files:
                file = f"/{file}"
                print(f"{file} is missing")
                with tarfile.open(
                    destination,
                    mode="a:",
                    **kwargs,
                ) as tar:
                    tar.add(file)
                print(f"{file} is added")
        else:
            with tarfile.open(destination, mode="w:", **kwargs) as tar:
                tar.add(source)
        return True

    @staticmethod
    def readlink(path):
        return os.readlink(path)

    @staticmethod
    def make_path(folder, fname):
        """Make path from folder and file."""
        fnames = list()
        if isinstance(fname, list):
            for f in fname:
                fnames.append(os.path.join(folder, f))
            return fnames
        else:
            return os.path.join(folder, fname)

    @staticmethod
    def create_folder(fname):
        """Create an empty folder."""
        os.makedirs(fname)

    @staticmethod
    def find_files(folder, ext=None):
        """Find images in a folder with a given extension."""
        file_list = list()
        for root, _, files in os.walk(folder):
            for file in files:
                if ext is not None and file.endswith(ext):
                    file_list.append(os.path.join(root, file))
                else:
                    file_list.append(os.path.join(root, file))

        return file_list

    @staticmethod
    def list_subfolders(folder):
        """List subfolders of a given folder."""
        subs = list()
        for root, dirs, _ in os.walk(folder):
            for dir_name in dirs:
                subs.append(os.path.join(root, dir_name))
        return subs

    @staticmethod
    def delete_folder(folder):
        """Delete a folder provided in the argument."""
        if _FileOperations.check_folder_exists(folder):
            for root, dirs, files in os.walk(folder, topdown=False):
                for name in files:
                    _FileOperations.delete_file(os.path.join(root, name))
                for name in dirs:
                    os.rmdir(os.path.join(root, name))
            os.rmdir(folder)

    @staticmethod
    def delete_file(file):
        """Delete the file provided in the argument."""
        if os.path.exists(file):
            os.remove(file)
            return True
        else:
            return False

    @staticmethod
    def check_folder_exists(folder):
        """Check if folder given in the argument exists or not."""
        return os.path.isdir(folder)

    @staticmethod
    def check_file_exists(file):
        """Check if file given in the argument exists or not."""
        return os.path.exists(file)

    @staticmethod
    def get_dirname(path):
        """Get directory name: (/etc/folder/file.ext) -> (/etc/folder)"""
        return os.path.dirname(path)

    @staticmethod
    def get_basename(path):
        """Get directory name: (/etc/folder/file.ext) -> (/etc/folder)"""
        return os.path.basename(path)

    @staticmethod
    def load_data(fname, fn):
        """Read data from file with given function fn."""
        data = None
        try:
            with open(fname) as file_handler:
                data = fn(file_handler)
        except yaml.YAMLError as exc:
            raise exc
        except json.JSONDecodeError as exc:
            raise exc
        except Exception as exc:
            print(exc)
            raise exc
        finally:
            return data

    @staticmethod
    def dump_data(fname, fn, data, pretty=True):
        """Save data to file with given function fn."""
        with open(fname, "w") as outfile:
            if pretty:
                fn(data, outfile, indent=4, sort_keys=True)
            else:
                fn(data, outfile)


class io(_FileOperations):

    def __init__(self):
        super(io, self).__init__()

    @staticmethod
    def dump_yaml(fname, data):
        """Save data to a yaml file."""
        io.dump_data(fname, yaml.dump, data, pretty=False)

    @staticmethod
    def dump_json(fname, data, pretty=True):
        """Save data to a json file."""
        io.dump_data(fname, json.dump, data, pretty=pretty)

    @staticmethod
    def print_json(data, pretty=True, **kwargs):
        """Save data to a json file."""
        if pretty:
            return json.dumps(data, indent=4, cls=_CustomEncoder)
        else:
            return json.dumps(data)

    @staticmethod
    def load_yaml(fname):
        """Read a yaml file and return the data as an dictionary object."""
        data = io.load_data(fname, yaml.safe_load)
        return data

    @staticmethod
    def load_yaml_multidoc(fname):
        try:
            Loader = yaml.CSafeLoader
        except AttributeError:  # System does not have libyaml
            Loader = yaml.SafeLoader
        with open(fname) as f:
            data = list(yaml.load_all(f, Loader=Loader))
        return data

    @staticmethod
    def load_json(fname):
        """Read a json file and return the data as a dict object."""
        data = io.load_data(fname, json.load)
        return data

    @staticmethod
    def run_command(command):
        with subprocess.Popen(
            command, stdout=subprocess.PIPE, stderr=subprocess.PIPE
        ) as proc:
            (out, err) = proc.communicate()
        return proc.returncode, out, err

    @staticmethod
    def run_func_threaded(func, args=()):
        th = threading.Thread(target=func, args=args)
        th.start()
        th.join()
        return 1

    @staticmethod
    def load_matfile(fname):
        mat_data = sio.loadmat(fname)

        # Create a dictionary to store variables
        data_dict = {}

        # Iterate over the items in the loaded data
        for key, value in mat_data.items():
            # Typically, .mat files have metadata entries starting with '__'
            # We want to ignore these and only add actual variables
            if not key.startswith("__"):
                data_dict[key] = value
        return data_dict

    @staticmethod
    def load_wavefile(fname):
        return sio.wavfile.read(fname)


if __name__ == "__main__":
    pass

import hashlib
import os
import uuid
from hmac import HMAC


class Security:
    @staticmethod
    def generate_salt(length=16):
        """Generate a random salt."""
        return os.urandom(length).hex()

    @staticmethod
    def calculate_md5(foo, salt=None):
        """Create an MD5 hash of a string with optional salt."""
        if salt is None:
            salt = Security.generate_salt()
        foo += salt
        return hashlib.md5(foo.encode()).hexdigest().lower(), salt

    @staticmethod
    def calculate_sha256(foo, salt=None):
        """Create an SHA256 hash of a string with optional salt."""
        if salt is None:
            salt = Security.generate_salt()
        foo += salt
        return hashlib.sha256(foo.encode()).hexdigest().lower(), salt

    @staticmethod
    def new_uuid(version=4):
        """Generate a new UUID."""
        if version == 1:
            return str(uuid.uuid1())
        return str(uuid.uuid4())

    @staticmethod
    def hmac_sha256(key, message):
        """Generate an HMAC SHA256 signature."""
        if isinstance(key, str):
            key = key.encode()
        if isinstance(message, str):
            message = message.encode()
        return HMAC(key, message, hashlib.sha256).hexdigest().lower()


if __name__ == "__main__":
    pass

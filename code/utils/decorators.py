import functools
import time


def timeit(func):
    """
    Decorator to time a function's execution.
    """

    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        start_time = time.time()
        result = func(*args, **kwargs)
        end_time = time.time()
        print(
            f"{func.__module__}.{func.__name__} took {(end_time - start_time):.6f} seconds to run."
        )
        return result

    return wrapper

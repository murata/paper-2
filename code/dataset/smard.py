import random
import re

import librosa
import numpy as np
from dataset.base import BaseDataset
from utils import Security


class SMARD:
    def __init__(self, folder, training_ratio=0.7):
        assert training_ratio <= 1
        self._training = BaseDataset()
        self._testing = BaseDataset()
        # self._cases = ["0000", "1000", "2000", "0001", "1001", "2001", "0002", "1002", "2002", "0003", "1003", "2003", "0010", "1010", "2010", "0011", "1011", "2011", "0020", "1020", "2020", "0021", "1021", "2021", "0100", "1100", "2100", "0101", "1101", "2101", "0102", "1102", "2102", "0103", "1103", "2103", "0110", "1110", "2110", "0111", "1111", "2111", "0120", "1120", "2120", "0121", "1121", "2121"]
        self._cases = ["0000", "1000"]
        self._folder = folder
        self.__training_cases, self.__testing_cases = SMARD.split_cases(
            self._cases, training_ratio
        )
        self.load_cases()

    def load_cases(self):
        for __case in self.__training_cases + self.__testing_cases:
            setup_file = f"{self._folder}/{__case}/setup.txt"
            setup_contents = SMARD.read_text_from_file(setup_file)
            speaker_info = SMARD.parse_loudspeaker_position(setup_contents)
            mic_info = SMARD.parse_mic_positions(setup_contents)
            audio_names = SMARD.parse_audio_names(setup_contents)

            for audio_name in audio_names:
                case_idx = Security.new_uuid()
                for info in mic_info:
                    __file_path = f"{self._folder}/{__case}/{audio_name}_ch{info['channel_number']}_{info['mic_name']}.flac"
                    __data, _ = librosa.load(__file_path, mono=False, sr=None)

                    if __case in self.__training_cases:
                        self._training.add_labeled_data(
                            __data, speaker_info["location"], info["location"], case_idx
                        )
                    else:
                        self._testing.add_labeled_data(
                            __data, speaker_info["location"], info["location"], case_idx
                        )

    @property
    def folder(self):
        return self._folder

    @property
    def cases(self):
        return self._cases

    @property
    def training(self):
        return self._training

    @property
    def testing(self):
        return self._testing

    @staticmethod
    def parse_loudspeaker_position(text):
        pattern = r"Loudspeaker position \(width, length, height\):\s*(.+?)\s*\((.+?) m, (.+?) m, (.+?) m\)"
        match = re.search(pattern, text)
        if match:
            loudspeaker = match.group(1)
            x = float(match.group(2))
            y = float(match.group(3))
            z = float(match.group(4))
            return {"loudspeaker": loudspeaker, "location": np.array([x, y, z])}
        else:
            return None

    @staticmethod
    def parse_mic_positions(text):
        pattern = r"MIC positions \(width, length, height\):\s*(.+)"
        match = re.search(pattern, text, re.DOTALL)
        if match:
            mic_positions = []
            mic_lines = match.group(1).strip().split("\n")
            for line in mic_lines:
                mic_info = re.match(
                    r"(.+?) Ch\. (\d+) \((.+?) m, (.+?) m, (.+?) m\)", line
                )
                if mic_info:
                    mic_name = mic_info.group(1)
                    channel_number = int(mic_info.group(2))
                    x = float(mic_info.group(3))
                    y = float(mic_info.group(4))
                    z = float(mic_info.group(5))
                    mic_positions.append(
                        {
                            "mic_name": mic_name,
                            "channel_number": channel_number,
                            "location": np.array([x, y, z]),
                        }
                    )
            return mic_positions
        else:
            return None

    @staticmethod
    def parse_audio_names(text):
        pattern = r"Input audio:\s*(.+?)\s*\nLatencies:"
        match = re.search(pattern, text, re.DOTALL)
        audio_names = list()
        if match:
            audio_lines = match.group(1).strip().split("\n")
            for line in audio_lines:
                chunks = line.split("/")
                if chunks[0] == "artificial":
                    audio_names.append(chunks[-1])
            return audio_names
        else:
            return None

    @staticmethod
    def read_text_from_file(file_path):
        with open(file_path, "r") as file:
            text = file.read()
        return text

    @staticmethod
    def split_cases(cases, training_ratio):
        random.shuffle(cases)
        total_cases = len(cases)
        training_count = int(total_cases * training_ratio)
        training_cases = cases[:training_count]
        testing_cases = cases[training_count:]
        return training_cases, testing_cases

import glob

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from utils import Time, TimeNative, io


class LocataSensorLoader:
    def load_dev(self, folder, task_id, recording_id, sensor, meta_only=False):
        __folder_task = f"{folder}/dev/task{task_id}"
        if not io.check_folder_exists(__folder_task):
            raise FileNotFoundError(f"{__folder_task} was not found")

        if sensor.lower() not in ["dicit", "eigenmike", "dummy", "benchmark2"]:
            raise ValueError(f"{sensor} type is not known")

        __folder_sensor = f"{__folder_task}/recording{recording_id}/{sensor}"
        if not io.check_folder_exists(__folder_task):
            raise FileNotFoundError(f"{__folder_task} was not found")

        __sensor_metadata = LocataSensorLoader.load_text_data(
            f"{__folder_sensor}/position_array_{sensor}.txt"
        )

        if sensor == "dicit":
            sensor_locations = __sensor_metadata[:, 21:].reshape(-1, 15, 3)
        elif sensor == "eigenmike":
            sensor_locations = __sensor_metadata[:, 21:].reshape(-1, 32, 3)
        elif sensor == "dummy":
            sensor_locations = __sensor_metadata[:, 21:].reshape(-1, 4, 3)
        elif sensor == "benchmark2":
            sensor_locations = __sensor_metadata[:, 21:].reshape(-1, 12, 3)

        __sensor_times = __sensor_metadata[:, :6]

        sensor_timestamps = [
            LocataSensorLoader.convert_time_list(*__time) for __time in __sensor_times
        ]

        __times = LocataSensorLoader.load_text_data(
            f"{__folder_sensor}/audio_array_timestamps_{sensor}.txt"
        )

        timestamps = [
            LocataSensorLoader.convert_time_list(*__time) for __time in __times
        ]

        pattern = f"{__folder_sensor}/position_source_*.txt"
        source_files = glob.glob(pattern)
        tracks = []

        for source_file in source_files:
            # Load the data from the file
            data = LocataSensorLoader.load_text_data(source_file)

            # Extract the relevant columns (assuming the position data is in columns 6 to 8)
            track = data[:, 6:9].reshape(-1, 3)

            # Append the track to the list of tracks
            tracks.append(track)

        # Convert the list of tracks to a numpy array with shape (nobjects, ntimes, 3)
        tracks = np.atleast_3d(np.array(tracks)).transpose((1, 0, 2))

        df = {
            "timestamps": sensor_timestamps,
            "target_tracks": tracks,
            "sensor_tracks": sensor_locations,
        }

        if meta_only:
            return df

        sampling_frequency, training_data = io.load_wavefile(
            f"{__folder_sensor}/audio_array_{sensor}.wav"
        )

        data = {
            "timestamps": timestamps,
            "data": training_data,
            "sampling_frequency": sampling_frequency,
        }
        return data, df

    @staticmethod
    def load_text_data(filename):
        # Load the data from the file, skipping the first row (header)
        data = np.loadtxt(filename, delimiter="\t", skiprows=1)

        return data

    @staticmethod
    def convert_time_list(year, month, day, hour, minute, second):
        second_int = int(second)
        microseconds = int((second - second_int) * 1e6)

        # time = {"year": int(year),
        #         "month": int(month),
        #         "day": int(day),
        #         "hour": int(hour),
        #         "minute": int(minute),
        #         "second": second_int,
        #         "microsecond": microseconds
        #         }

        # time = Time.from_list(**time)
        return np.datetime64(
            f"{int(year):04d}-{int(month):02d}-{int(day):02d}T{int(hour):02d}:{int(minute):02d}:{second_int:02d}.{microseconds:06d}"
        )

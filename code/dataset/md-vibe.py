import numpy as np
from sigmoid.dataset.base import BaseDataset
from sigmoid.utils import io


class MDVibe:
    def __init__(
        self, path, training_ratio=0.15, testing_ratio=0.85, validation_ratio=0
    ):
        assert (training_ratio + testing_ratio + validation_ratio) == 1
        data = io.load_matfile(path)
        z = data["z_normalized"]
        shuffled_indices = np.random.permutation(z.shape[0])

        num_training = int(z.shape[0] * training_ratio)
        num_testing = int(z.shape[0] * testing_ratio)
        num_validation = z.shape[0] - num_training - num_testing

        # Split the shuffled indices into three mutually exclusive sequences
        training_indices = shuffled_indices[:num_training]
        testing_indices = shuffled_indices[num_training : num_training + num_testing]
        validation_indices = shuffled_indices[
            num_training + num_testing : num_training + num_testing + num_validation
        ]
        self._sensor_locations = data["sensor_locations"]

        self._training = BaseDataset(
            z[training_indices, :, :], data["gt"][training_indices, :]
        )
        self._testing = BaseDataset(
            z[testing_indices, :, :], data["gt"][testing_indices, :]
        )
        self._validation = BaseDataset(
            z[validation_indices, :, :], data["gt"][validation_indices, :]
        )

    @property
    def training(self):
        return self._training

    @property
    def testing(self):
        return self._testing

    @property
    def validation(self):
        return self._validation

    @property
    def sensor_locations(self):
        return self._sensor_locations

    @property
    def sampling_frequency(self):
        return 100_000

from dataclasses import dataclass, field

import numpy as np
import pandas as pd
from utils import Security


@dataclass
class BaseDataset:
    id: str = field(default_factory=lambda: str(Security.new_uuid()), init=False)
    _dataframe: pd.DataFrame = field(
        default_factory=lambda: pd.DataFrame(
            columns=[
                "data",
                "track",
                "sensor_locations",
                "num_sensors",
                "num_variables",
                "case_id",
            ]
        ),
        init=False,
    )

    def add_labeled_data(self, data, label, sensor_locations, case_id=None):
        num_sensors = len(sensor_locations)
        num_variables = np.zeros(len(data))
        new_row = pd.DataFrame(
            {
                "data": [data],
                "labels": [label],
                "sensor_locations": [sensor_locations],
                "num_sensors": [num_sensors],
                "num_variables": [num_variables],
                "case_id": [case_id],
            }
        )
        self._dataframe = pd.concat([self._dataframe, new_row], ignore_index=True)

    @property
    def dataframe(self):
        return self._dataframe

    @property
    def data(self):
        return self._dataframe["data"]

    @property
    def training_data(self):
        return self._dataframe["data"]

    @property
    def testing_data(self):
        return self._testing

    @property
    def validation_data(self):
        return self._validation

    @property
    def labels(self):
        return self._dataframe["labels"].tolist()

    @property
    def training(self):
        return self._training

    @property
    def testing(self):
        return self._testing

    @property
    def validation(self):
        return self._validation

    @property
    def sampling_frequency(self):
        return self._sampling_frequency

    def __iter__(self):
        return zip(self.data, self.labels)

    def __len__(self):
        len(self._dataframe)

    def __hash__(self):
        return hash(self.id)

    def __str__(self):
        return f"BaseDataset '{self.id}' with {len(self._dataframe)} samples."

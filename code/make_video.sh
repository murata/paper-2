#!/bin/bash

# Default values
folder="output"
prefix="hist-"
suffix=".png"
fps=3
output="hist.mp4"

# Parse command line arguments
while getopts "f:p:s:r:o:" opt; do
  case $opt in
    f) folder=$OPTARG;;
    p) prefix=$OPTARG;;
    s) suffix=$OPTARG;;
    r) fps=$OPTARG;;
    o) output=$OPTARG;;
    \?) echo "Invalid option -$OPTARG" >&2
        exit 1
    ;;
  esac
done

# Running the Python script with the given arguments
python make_video.py --folder "$folder" --prefix "$prefix" --suffix "$suffix" --fps "$fps" --output "$output"

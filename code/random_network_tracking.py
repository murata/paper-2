import argparse

import numpy as np
import pandas as pd
import sigmoid.types as st
from feelings import *
from tqdm import tqdm
from trackers.particle_filter import ParticleFilter
from trackers.types import Location


def make_target_trajectory(num_steps=100, radius=1, radial_velocity=np.pi / 20):
    """Generate a list of locations to form a circular target trajectory."""
    final_angle = num_steps * radial_velocity
    angles = np.linspace(0, final_angle, num_steps)
    x = radius * np.cos(angles) + 0.3
    y = radius * np.sin(angles) + 0.3
    z = np.zeros_like(x)
    trajectory = np.column_stack((x, y, z))
    return trajectory


def state_transition_func(particle, control_input, dt=1):
    """Apply state transition with random noise to simulate movement."""
    noise = np.random.normal(0, 0.001, size=3)
    particle.pose.velocity = control_input
    particle.pose.location += particle.pose.velocity.scale(dt) + noise
    return particle


def measurement_func(particle, **kwargs):
    """Simulate measurement process with added noise."""
    measurement_noise = np.random.normal(0, 0.01, size=3)
    particle.pose.location += measurement_noise
    measurement_noise = np.random.normal(0, 0.001, size=3)
    particle.pose.velocity += measurement_noise
    return particle


def main(output, num_experiments, num_steps, num_sensors=32):
    wave_velocity = 343
    lower_bounds = np.array([-0.5, -0.5, 0])
    upper_bounds = np.array([0.5, 0.5, 2])
    sensor_radius = 0.8
    sigmas = np.arange(0, 50, 2)
    lamb = 0
    results = []
    num_particles = 5000
    num_experiments = 1
    for experiment_idx in range(num_experiments):
        for sigma in sigmas:
            pf = ParticleFilter(
                num_particles=num_particles,
                state_transition_func=state_transition_func,
                measurement_func=measurement_func,
            )
            network = get_random_network(num_sensors, sensor_radius)
            localizer = TDOAHyperbolaeSolver(network, wave_velocity)
            target_trajectory = make_target_trajectory(
                num_steps=num_steps, radius=0.1, radial_velocity=0.8
            )

            for _, target_location in tqdm(
                enumerate(target_trajectory),
                total=len(target_trajectory),
                desc="Processing Experiments",
            ):
                ideal_events = localizer.inverse(target_location)
                ideal_times = ideal_events.extract_times()
                noise = (
                    np.random.randn(*ideal_times.shape)
                    * sigma
                    / network[0].sampling_frequency
                )
                noisy_times = ideal_times + noise
                noisy_events = st.MatchedEvent()

                for idx, ideal_event in enumerate(ideal_events):
                    noisy_event = st.Event(
                        energy=ideal_event.energy,
                        time=noisy_times[idx],
                        confidence=ideal_event.confidence,
                        detected_by=ideal_event.detected_by,
                    )
                    noisy_events.add_event(noisy_event)

                measurement = localizer.predict(
                    noisy_events,
                    initial_guess=target_location,
                    lamb=lamb,
                    lower_bounds=lower_bounds,
                    upper_bounds=upper_bounds,
                )

                prior_particles = np.copy(pf.particles.locations)
                prior_weights = np.copy(pf.particles.weights)
                control_input = (
                    Location.from_vector(measurement) - pf.estimate().location
                )
                pf.predict(control_input.scale(0.1))
                predicted_particles = np.copy(pf.particles.locations)
                predicted_weights = np.copy(pf.particles.weights)
                pf.update(measurement)

                try:
                    pf.particles.normalize_weights()
                except ValueError as e:
                    print(f"Degenerate weights detected: {e} Weights are being reset.")
                    pf.init_particles(num_particles=num_particles)

                if (
                    pf.particles.effective_sample_size
                    < 0.25 * pf.particles.num_particles
                ):
                    print("it is time to resample")
                    try:
                        pf.resample()
                    except ValueError:
                        pf.init_particles(num_particles=num_particles)
                        pf.resample()
                    finally:
                        pf.particles.normalize_weights()

                posterior_particles = np.copy(pf.particles.locations)
                posterior_weights = np.copy(pf.particles.weights)
                estimated_pose = pf.estimate()

                result = {
                    "experiment_idx": experiment_idx,
                    "sigma": sigma,
                    "target_location": target_location.tolist(),
                    "measured_location": measurement.tolist(),
                    "estimated_location": estimated_pose.location.to_numpy().tolist(),
                    "prior_particles": prior_particles.tolist(),
                    "prior_weights": prior_weights.tolist(),
                    "predicted_particles": predicted_particles.tolist(),
                    "predicted_weights": predicted_weights.tolist(),
                    "posterior_particles": posterior_particles.tolist(),
                    "posterior_weights": posterior_weights.tolist(),
                    "sensor_locations": network.locations.tolist(),
                }
                results.append(result)

    results_df = pd.DataFrame(results)
    results_df.to_hdf(output, key="results", mode="w")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="")
    parser.add_argument("--output", type=str, required=True, help="File name")
    parser.add_argument(
        "--experiments", type=int, required=True, help="Number of experiments"
    )
    parser.add_argument(
        "--steps", type=int, required=True, help="Number of steps in the trajectory"
    )
    args = parser.parse_args()
    print(f"args received: {args}")
    main(args.output, args.experiments, args.steps)

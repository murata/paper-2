import argparse

import matplotlib

matplotlib.use("Agg")  # noqa
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import scipy.stats as ss
import seaborn as sns
import sklearn.metrics as sm
from tqdm import tqdm


def visualize_decompositon(input_df, output, temporal_df, spatial_df, tracking_df):
    for index, input_row in tqdm(
        input_df.iterrows(),
        total=len(input_df),
        desc="Visualizing Decomposition Results",
    ):
        num_sensors, num_layers, num_components = input_row.weight_pyramids.shape

        # Prepare data
        data = []
        for i in range(num_sensors):
            for j in range(num_layers):
                for k in range(num_components):
                    x = input_row.bias_pyramids[i, j, k]
                    x_scaled = x / np.max(np.abs(input_row.bias_pyramids))
                    y = input_row.weight_pyramids[i, j, k]
                    y_scaled = y / np.max(np.abs(input_row.weight_pyramids))
                    data.append([x_scaled, y_scaled, i, j, k])
        data = pd.DataFrame(
            data, columns=["Bias", "Weight", "Sensor", "Layer", "Component"]
        )

        # Create scatterplot
        plt.figure(figsize=(8, 8))
        sns.set_context("talk")
        sns.lineplot(
            x="Bias",
            y="Weight",
            hue="Layer",
            data=data,
            markers=True,
            dashes=False,
            palette="tab20",
        )
        plt.suptitle(f"Decomposition (Case: {index})")
        # Save figure
        plt.tight_layout()
        plt.savefig(f"{output}/decomposition-{index}.png")
        plt.close()


def visualize_temporal(input_df, output, temporal_df, spatial_df, tracking_df):
    prob_layers = temporal_df["lag_likelihoods"].to_numpy()
    prob_layers = np.stack(prob_layers)
    lag_space = temporal_df["lag_space"].to_numpy()
    lag_space = np.vstack(lag_space)[0, :]
    print(f"Probability pyramid is loaded with shape {prob_layers.shape}")
    true = np.vstack(temporal_df.ideal_lags.to_numpy())
    est_layers_mle = lag_space[np.argmax(prob_layers, axis=2)]
    cc_delay = np.vstack(input_df.cc_phase_delay.to_numpy())
    pp_delay = np.vstack(input_df.pp_phase_delay.to_numpy())
    print(
        f"True Values shape: {true.shape} -- \
          Est Values (MLE) shape: {est_layers_mle.shape} -- \
          Prob {prob_layers.shape}"
    )
    num_sensors = est_layers_mle.shape[1]
    # Calculate the residuals
    residuals = true.ravel() - est_layers_mle.ravel()

    # Calculate the first and third quartiles
    first_quartile, third_quartile = np.quantile(residuals, [0.25, 0.75])

    # Get the values between the first and third quartiles
    values_between_quartiles = residuals[
        (residuals >= first_quartile) & (residuals <= third_quartile)
    ]
    fig, ax = plt.subplots(1, 1, figsize=(6, 3))
    ax.hist(residuals, lag_space, density=True)
    ax.plot(
        lag_space,
        ss.norm(
            loc=np.mean(values_between_quartiles),
            scale=np.std(values_between_quartiles),
        ).pdf(lag_space),
    )
    ax.set_xlabel("Delay [samp]")
    ax.set_ylabel("Freq [%]")
    ax.set_title("Residual Distribution by Delay")
    fig.tight_layout()
    fig.savefig(f"{output}/residuals-all.png")
    plt.close()

    for idx in range(num_sensors):
        fig, ax = plt.subplots(1, 1, figsize=(8, 8))
        ax.plot(true[:, idx], est_layers_mle[:, idx], "kx", label="MLE")
        ax.plot(
            true[:, idx],
            pp_delay[:, idx],
            "g+",
            label="Plassmann",
        )
        ax.plot(
            true[:, idx],
            cc_delay[:, idx],
            "bo",
            label="CC",
        )
        ax.plot([-8, 8], [-8, 8], "r-.")
        ax.set_xlabel("True Delay [samp]")
        ax.set_ylabel("Est. Delay [samp]")
        ax.set_title(f"Sensor {idx}")
        ax.set_xlim(-10, 10)
        ax.set_ylim(-10, 10)
        ax.set_aspect("equal")
        ax.legend()
        fig.tight_layout()
        fig.savefig(f"{output}/corr-est-vs-gt-sensor-{idx:02d}.png")
        plt.close()

    for idx, prob_layer in enumerate(prob_layers):
        fig, ax = plt.subplots(1, figsize=(8, 8))
        contour = ax.contourf(lag_space, range(num_sensors), prob_layer, levels=100)
        for sensor_idx in range(num_sensors):
            if sensor_idx == 0:
                __label_true = "True"
                __label_mle = "MLE"
                __label_plassmann = "Plassmann"
                __label_cc = "CC"
            else:
                __label_true = "_True"
                __label_mle = "_MLE"
                __label_plassmann = "_"
                __label_cc = "_"

            ax.plot(true[idx, sensor_idx], sensor_idx, "g+", label=__label_true)
            ax.plot(
                est_layers_mle[idx, sensor_idx], sensor_idx, "rx", label=__label_mle
            )

            ax.plot(
                pp_delay[idx, sensor_idx],
                sensor_idx,
                "k+",
                label=__label_plassmann,
            )

            ax.plot(
                cc_delay[idx, sensor_idx],
                sensor_idx,
                "wo",
                label=__label_cc,
            )
        ax.set_xlim(-10, 10)
        ax.set_ylabel("Sensor ID")
        ax.set_xlabel("Delay [samples]")
        ax.set_title(f"Delay Likelihood by Sensor (Case: {idx})")
        ax.legend()
        fig.colorbar(contour, ax=ax, label="Likelihood")  # Add colorbar to the plot
        fig.tight_layout()
        fig.savefig(f"{output}/lag-likelihoods-{idx:02d}.png")
        plt.close()

    for idx in range(num_sensors):
        print(
            f"Sensor {idx:03d} -- MSE (MLE): {sm.mean_squared_error(true[:, idx], est_layers_mle[:, idx]):07.3f} [samples], r2: {sm.r2_score(true[:, idx], est_layers_mle[:, idx]):07.3f}"
        )
    print(
        f"All        -- MSE: {sm.mean_squared_error(true.ravel(), est_layers_mle.ravel()):07.3f} [samples], r2: {sm.r2_score(true.ravel(), est_layers_mle.ravel()):07.3f}"
    )


def visualize_tracking(input_df, output, temporal_df, spatial_df, tracking_df):
    for index, input_row in tqdm(
        input_df.iterrows(), "Visualizing Tracking Results", total=len(input_df)
    ):
        fig, ax = plt.subplots(1, figsize=(8, 8))
        ax.plot(
            input_row.network.locations[:, 0],
            input_row.network.locations[:, 1],
            "rx",
            label="Sensor",
        )
        ax.plot(
            spatial_df.loc[index].estimated_location[0],
            spatial_df.loc[index].estimated_location[1],
            "bo",
            label="Measurement",
        )
        ax.plot(
            input_row.target_location[0],
            input_row.target_location[1],
            "go",
            label="True",
        )

        prior = np.array(tracking_df.loc[index].prior_particles)
        prior_weights = np.array(tracking_df.loc[index].prior_weights)
        prior_weights /= np.max(prior_weights)
        posterior = np.array(tracking_df.loc[index].posterior_particles)
        posterior_weights = np.array(tracking_df.loc[index].posterior_weights)
        posterior_weights /= np.max(posterior_weights)

        ax.scatter(
            prior[:, 0],
            prior[:, 1],
            marker="o",
            color="cyan",
            label="Prior",
            alpha=prior_weights,
        )
        ax.scatter(
            posterior[:, 0],
            posterior[:, 1],
            marker="+",
            color="k",
            label="Posterior",
            alpha=posterior_weights,
        )

        ax.set_xlabel("x [m]")
        ax.set_ylabel("y [m]")
        ax.set_xlim([-3, 3])
        ax.set_ylim([-3, 3])
        ax.set_title(f"Localization Result (Case: {index})")
        ax.legend()
        ax.set_aspect("equal")
        fig.tight_layout()
        fig.savefig(f"{output}/tracking-{index:02d}.png")
        plt.close()


def main(input, output, temporal, spatial, tracking):
    input_df = pd.read_hdf(input, key="df")
    print(f"Input data loaded with shape: {len(input_df)}")
    print(f"Columns: {input_df.columns.to_list()}")
    temporal_df = pd.read_hdf(temporal, key="df")
    print(f"Temporal data loaded with shape: {len(temporal_df)}")
    print(f"Columns: {temporal_df.columns.to_list()}")
    spatial_df = pd.read_hdf(spatial, key="df")
    print(f"Spatial data loaded with shape: {len(spatial_df)}")
    print(f"Columns: {spatial_df.columns.to_list()}")
    tracking_df = pd.read_hdf(tracking, key="df")
    print(f"Tracking data loaded with shape: {len(tracking_df)}")
    print(f"Columns: {tracking_df.columns.to_list()}")

    visualize_decompositon(input_df, output, temporal_df, spatial_df, tracking_df)
    visualize_temporal(input_df, output, temporal_df, spatial_df, tracking_df)
    visualize_tracking(input_df, output, temporal_df, spatial_df, tracking_df)
    return 0


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="")
    parser.add_argument("--input", type=str, required=True, help="Input File name")
    parser.add_argument("--output", type=str, required=True, help="Output folder name")
    parser.add_argument(
        "--temporal", type=str, required=True, help="Temporal File name"
    )
    parser.add_argument("--spatial", type=str, required=True, help="Spatial File name")
    parser.add_argument(
        "--tracking", type=str, required=True, help="Tracking File name"
    )

    args = parser.parse_args()
    print("Args received:", args)
    main(args.input, args.output, args.temporal, args.spatial, args.tracking)

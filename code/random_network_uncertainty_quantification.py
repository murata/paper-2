import argparse

import numpy as np
import pandas as pd
import sigmoid.types as st
from feelings import *
from tqdm import tqdm


def main(output, steps):
    wave_velocity = 343
    lower_bounds = np.array([-0.5, -0.5, 0])
    upper_bounds = np.array([0.5, 0.5, 2])
    num_experiments = steps
    sensor_radius = 0.8
    sigmas = np.arange(0, 50, 2)
    nums_sensors = np.array([4, 8, 16, 32])
    lamb = 0

    results = list()

    for _ in tqdm(np.arange(num_experiments), desc="Processing Labels"):
        for num_sensors in nums_sensors:
            for sigma in sigmas:
                network = get_random_network(
                    num_sensors=num_sensors, sensor_radius=sensor_radius
                )
                localizer = TDOAHyperbolaeSolver(network, wave_velocity)
                target_location = get_random_target_location(
                    lower_bounds * 0.8, upper_bounds * 0.8
                )
                random_location = get_random_target_location(
                    lower_bounds * 0.8, upper_bounds * 0.8
                )

                ideal_events = localizer.inverse(target_location)

                x_ideal = localizer.predict(
                    ideal_events,
                    # initial_guess=target_location,
                    lamb=lamb,
                    lower_bounds=lower_bounds,
                    upper_bounds=upper_bounds,
                )
                ideal_times = ideal_events.extract_times()
                noise = (
                    np.random.randn(*ideal_times.shape)
                    * sigma
                    / network[0].sampling_frequency
                )
                noisy_times = ideal_times + noise
                noisy_events = st.MatchedEvent()
                for idx, ideal_event in enumerate(ideal_events):
                    noisy_event = st.Event(
                        energy=ideal_event.energy,
                        time=noisy_times[idx],
                        confidence=ideal_event.confidence,
                        detected_by=ideal_event.detected_by,
                    )
                    noisy_events.add_event(noisy_event)

                estimated_location = localizer.predict(
                    noisy_events,
                    # initial_guess=target_location,
                    lamb=lamb,
                    lower_bounds=lower_bounds,
                    upper_bounds=upper_bounds,
                )

                result = pd.DataFrame(
                    {
                        "type": ["ideal"],
                        "network": [network],
                        "target_location": [target_location],
                        "estimated_location": [estimated_location],
                        "random_location": [random_location],
                        "ideal_location": [x_ideal],
                        "ideal_times": [ideal_times],
                        "noisy_times": [noisy_times],
                        "lamb": [lamb],
                        "sigma": [sigma],
                    }
                )

                results.append(result)

    results = pd.concat(results)
    results.to_hdf(output, key="results", mode="w")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="")
    parser.add_argument("--output", type=str, required=True, help="File name")
    parser.add_argument("--steps", type=int, required=True, help="File name")
    args = parser.parse_args()
    print(f"args received: {args}")
    main(args.output, args.steps)

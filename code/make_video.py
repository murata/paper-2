import argparse
import os

import imageio
from PIL import Image


def create_video_from_images(folder, prefix, suffix, fps, output_video_name):
    # List all files in the folder that start with the prefix and end with the suffix
    files = [
        f for f in os.listdir(folder) if f.startswith(prefix) and f.endswith(suffix)
    ]
    # Sort files lexicographically
    files = sorted(files)

    images = []
    for file in files:
        image_path = os.path.join(folder, file)
        # Open and append each image to the list of images
        try:
            img = Image.open(image_path)
            images.append(img)
        except IOError:
            print(f"Cannot open {image_path}")

    # Write the images to a video file
    if images:
        imageio.mimsave(output_video_name, images, fps=fps)
        print(f"Video saved as {output_video_name}")
    else:
        print("No images to process.")


def main():
    parser = argparse.ArgumentParser(
        description="Create a video from images in a folder."
    )
    parser.add_argument(
        "--folder", type=str, default="path_to_folder", help="Folder containing images"
    )
    parser.add_argument(
        "--prefix", type=str, default="img_", help="Prefix of the image files"
    )
    parser.add_argument(
        "--suffix",
        type=str,
        default=".jpg",
        help="Suffix of the image files (file extension)",
    )
    parser.add_argument(
        "--fps", type=int, default=24, help="Frames per second of the output video"
    )
    parser.add_argument(
        "--output", type=str, default="output_video.mp4", help="Output video file name"
    )

    args = parser.parse_args()

    create_video_from_images(
        args.folder, args.prefix, args.suffix, args.fps, args.output
    )


if __name__ == "__main__":
    main()

#!/bin/bash
set -euxo pipefail
# Check for the correct number of arguments
if [ "$#" -ne 1 ]; then
    echo "Usage: $0 <num_samples>"
    exit 1
fi


num_samples=$1

python make_dataset.py --input "/data/downloads/" --output "../data" --task 1 --recording 1 --num_components 8 --num_samples "$num_samples"
mv "../data/task-01-recording-01.h5" "single-static.h5"
tar -czvf "single-static.tar.gz" "single-static.h5" > /dev/null
mv "single-static.tar.gz" "../data/"

# python make_dataset.py --input "/data/downloads/" --output "../data" --task 2 --recording 1 --num_components 8 --num_samples "$num_samples"
# mv "../data/task-02-recording-01.h5" "multiple-static.h5"
# tar -czvf "multiple-static.tar.gz" "multiple-static.h5" > /dev/null
# mv "multiple-static.tar.gz" "../data/"

python make_dataset.py --input "/data/downloads/" --output "../data" --task 3 --recording 1 --num_components 8 --num_samples "$num_samples"
mv "../data/task-03-recording-01.h5" "single-dynamic.h5"
tar -czvf "single-dynamic.tar.gz" "single-dynamic.h5" > /dev/null
mv "single-dynamic.tar.gz" "../data/"

# python make_dataset.py --input "/data/downloads/" --output "../data" --task 4 --recording 1 --num_components 8 --num_samples "$num_samples"
# mv "../data/task-04-recording-01.h5" "multiple-dynamic.h5"
# tar -czvf "multiple-dynamic.tar.gz" "multiple-dynamic.h5" > /dev/null
# mv "multiple-dynamic.tar.gz" "../data/"

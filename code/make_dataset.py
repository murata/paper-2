import argparse
import concurrent.futures
import os
from typing import Any, Dict, List, Tuple

import numpy as np
import pandas as pd
from dataset.locata import LocataSensorLoader
from localizers.tdoa import TDOAHyperbolaeSolver
from sigmoid import Sensor, SensorNetwork
from sigmoid.base import SigmoidDecomposition
from tqdm import tqdm
from utils import TimeNative


def process_network_data(
    network_data: np.ndarray,
    num_components: int,
    sigmoid: SigmoidDecomposition,
    parallel: bool,
) -> Tuple[np.ndarray, np.ndarray, List[np.ndarray]]:
    weight_pyramids = []
    bias_pyramids = []
    masks = []

    if parallel:
        with concurrent.futures.ProcessPoolExecutor() as executor:
            futures = {
                executor.submit(
                    sigmoid.fit_transform, network_data[:, sensor_idx]
                ): sensor_idx
                for sensor_idx, _ in enumerate(sigmoid.network)
            }

            for future in concurrent.futures.as_completed(futures):
                sensor_idx = futures[future]
                (
                    weight_pyramid,
                    bias_pyramid,
                    mask,
                ) = future.result()
                weight_pyramids.append(weight_pyramid)
                bias_pyramids.append(bias_pyramid)
                masks.append(mask)
    else:
        for sensor_idx, _ in enumerate(sigmoid.network):
            sensor_data = network_data[:, sensor_idx]
            weight_pyramid, bias_pyramid, mask = sigmoid.transform(
                sensor_data, num_components=num_components
            )
            weight_pyramids.append(weight_pyramid)
            bias_pyramids.append(bias_pyramid)
            masks.append(mask)

    reference_signal = network_data[:, 15]

    # Array to store the estimated delays for each sensor
    delays = np.zeros(len(sigmoid.network), dtype=int)
    delays_plassmann = np.zeros(len(sigmoid.network), dtype=int)

    for sensor_idx, _ in enumerate(sigmoid.network):
        # Compute cross-correlation between the sensor signal and the reference
        signal = network_data[:, sensor_idx]
        energy = np.cumsum(signal**2)
        energy = np.convolve(energy, np.ones(13) / 13, mode="valid")
        denergy = np.diff(energy, append=energy[-1])
        delays_plassmann[sensor_idx] = np.argmax(denergy)

        cross_corr = np.correlate(signal, reference_signal, mode="full")

        # Find the index of the maximum correlation
        max_corr_idx = np.argmax(cross_corr)

        # Compute delay, adjusting for the length of the signals
        delay = max_corr_idx - (len(signal) - 1)
        delays[sensor_idx] = delay

    delays_plassmann = delays_plassmann - delays_plassmann[15]

    return (
        np.array(weight_pyramids),
        np.array(bias_pyramids),
        np.array(masks),
        np.array(delays),
        np.array(delays_plassmann),
    )


def process_label(
    label_idx: int,
    data: Dict[str, Any],
    labels: Dict[str, Any],
    sigmoid: SigmoidDecomposition,
    task_id: int,
    recording_id: int,
    localizer: TDOAHyperbolaeSolver,
    num_components: int,
    parallel: bool,
) -> List[Dict[str, Any]]:
    target_track = labels["target_tracks"][label_idx]
    results = []
    for target_id, target_location in enumerate(target_track):
        result = {
            "target_id": [target_id],
            "task_id": [task_id],
            "recording_id": [recording_id],
            "label_idx": [label_idx],
            "target_location": [target_location],
            "ideal_location": [np.nan],
            "residual": [np.nan],
            "ideal_times": [np.nan],
            "network": [localizer.network],
            "weight_pyramids": [np.nan],
            "bias_pyramids": [np.nan],
            "masks": [np.nan],
            "cc_phase_delay": [np.nan],
        }
        start_idx = np.searchsorted(data["timestamps"], labels["timestamps"][label_idx])
        end_idx = np.searchsorted(
            data["timestamps"] - TimeNative.duration(milliseconds=7),
            labels["timestamps"][label_idx],
        )

        ideal_events = localizer.inverse(target_location)
        ideal_times = ideal_events.extract_times()
        result["ideal_times"] = [ideal_times]
        x_ideal = localizer.predict(ideal_events, initial_guess=target_location)
        result["ideal_location"] = [x_ideal]
        residual = localizer.evaluate(x_ideal, target_location)
        result["residual"] = [residual]
        network_data = data["data"][start_idx:end_idx, :]
        weight_pyramids, bias_pyramids, masks, cc_phase_delay, delays_plassmann = (
            process_network_data(network_data, num_components, sigmoid, parallel)
        )

        result["weight_pyramids"] = [weight_pyramids]
        result["bias_pyramids"] = [bias_pyramids]
        result["masks"] = [masks]
        result["cc_phase_delay"] = [cc_phase_delay]
        result["pp_phase_delay"] = [delays_plassmann]
        df = pd.DataFrame(result)
        results.append(df)
    results_df = pd.concat(results, ignore_index=True)
    return results_df


def main(
    input_folder: str,
    output_folder: str,
    task_id: int,
    recording_id: int,
    num_components: int,
    parallel: bool = False,
    num_samples=100,
) -> None:
    print(
        f"data folder: {input_folder} is used to process task: {task_id} recording: {recording_id} with {num_components} components."
    )
    print(f"outputs will be saved to: {output_folder}")
    print(f"parallel processing is set: {parallel}")
    locata = LocataSensorLoader()
    data, labels = locata.load_dev(input_folder, task_id, recording_id, "eigenmike")
    sigmoid = SigmoidDecomposition()
    indices = np.linspace(0, len(labels["timestamps"]) - 10, num_samples, dtype=int)
    print(indices)
    results = []
    for label_idx in tqdm(indices, desc="Processing Labels"):
        network = SensorNetwork()
        for x in labels["sensor_tracks"][label_idx, :, :]:
            network.add_sensor(Sensor(x, data["sampling_frequency"]))
        localizer = TDOAHyperbolaeSolver(network, 343)
        sigmoid.network = localizer.network
        result = process_label(
            label_idx,
            data,
            labels,
            sigmoid,
            task_id=task_id,
            recording_id=recording_id,
            localizer=localizer,
            num_components=num_components,
            parallel=parallel,
        )
        results.append(result)

    recording_fname = (
        f"{output_folder}/task-{task_id:02}-recording-{recording_id:02d}.h5"
    )
    df = pd.concat(results, ignore_index=True)
    df.to_hdf(recording_fname, key="df")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Process sensor data.")
    parser.add_argument("--input", type=str, required=True, help="Input folder path")
    parser.add_argument("--output", type=str, required=True, help="Output folder path")
    parser.add_argument("--task", type=int, required=True, help="Task ID")
    parser.add_argument("--recording", type=int, required=True, help="Recording ID")
    parser.add_argument(
        "--num_samples",
        type=int,
        required=True,
        help="Number of samples to generate from the full dataset",
    )
    parser.add_argument(
        "--num_components", type=int, required=True, help="Number of components to use"
    )
    parser.add_argument(
        "--parallel", action="store_true", help="Use parallel processing"
    )
    args = parser.parse_args()

    main(
        args.input,
        args.output,
        args.task,
        args.recording,
        args.num_components,
        parallel=args.parallel,
        num_samples=args.num_samples,
    )

import argparse

import numpy as np
import pandas as pd
from tqdm import tqdm
from trackers.particle_filter import ParticleFilter
from trackers.particle_filter_types import Particle, Particles
from trackers.types import Location, Pose, Track
from utils.io import io


# State transition function for a mass-damper system under a given force input
def state_transition(particle, control_input, dt=1):
    """Apply state transition with random noise to simulate movement."""
    noise = np.random.normal(0, 0.001, size=3)
    particle.pose.velocity = control_input
    particle.pose.location += particle.pose.velocity.scale(dt) + noise
    return particle


# Measurement function simulating position sensor data with noise
def measurement_func(particle, **kwargs):
    """Simulate measurement process with added noise."""
    measurement_noise = np.random.normal(0, 0.01, size=3)
    particle.pose.location += measurement_noise
    measurement_noise = np.random.normal(0, 0.001, size=3)
    particle.pose.velocity += measurement_noise
    return particle


def main(input, output, ref_sensor_idx, temporal_file, spatial_file):
    print(f"Input received: {input}, the output will be saved in {output}")
    df = pd.read_hdf(input, key="df")
    num_particles = 5000
    print("Column Names in input_df:", df.columns.tolist(), "len:", len(df))
    temporal_df = pd.read_hdf(temporal_file, key="df")
    print(
        "Column Names in temporal_df:",
        temporal_df.columns.tolist(),
        "len:",
        len(temporal_df),
    )
    spatial_df = pd.read_hdf(spatial_file, key="df")
    print(
        "Column Names in spatial_df:",
        spatial_df.columns.tolist(),
        "len:",
        len(spatial_df),
    )

    print(f"Reference sensor: {ref_sensor_idx}")

    pf = ParticleFilter(
        num_particles=num_particles,
        state_transition_func=state_transition,
        measurement_func=measurement_func,
    )
    results = []
    for idx, input_row in tqdm(df.iterrows(), total=len(df), desc="Processing Rows"):
        temporal_row = temporal_df.loc[idx]
        spatial_row = spatial_df.loc[idx]
        prior_particles = np.copy(pf.particles.locations)
        prior_weights = np.copy(pf.particles.weights)
        measurement = spatial_row.estimated_location
        control_input = Location.from_vector(measurement) - pf.estimate().location
        pf.predict(control_input=control_input)
        predicted_particles = np.copy(pf.particles.locations)
        predicted_weights = np.copy(pf.particles.weights)
        pf.update(measurement)
        try:
            pf.particles.normalize_weights()
        except ValueError as e:
            print(f"Degenerate weights detected: {e} Weights are being reset.")
            pf.init_particles(num_particles=num_particles)

        if pf.particles.effective_sample_size < 0.25 * pf.particles.num_particles:
            print("it is time to resample")
            try:
                pf.resample()
            except ValueError:
                pf.init_particles(num_particles=num_particles)
                pf.resample()
            finally:
                pf.particles.normalize_weights()

        posterior_particles = np.copy(pf.particles.locations)
        posterior_weights = np.copy(pf.particles.weights)
        estimated_pose = pf.estimate()
        pf.particles.normalize_weights()
        pf.resample()
        posterior_particles = np.copy(pf.particles.locations)
        result = {
            "task_id": input_row["task_id"],
            "recording_id": input_row["recording_id"],
            "label_idx": input_row["label_idx"],
            "estimated_location": estimated_pose.location.to_numpy().tolist(),
            "prior_particles": prior_particles.tolist(),
            "prior_weights": prior_weights.tolist(),
            "predicted_particles": predicted_particles.tolist(),
            "predicted_weights": predicted_weights.tolist(),
            "posterior_particles": posterior_particles.tolist(),
            "posterior_weights": posterior_weights.tolist(),
        }

        results.append(pd.DataFrame([result]))
    # Create a new DataFrame to store the results
    results_df = pd.concat(results, ignore_index=True)
    results_df.to_hdf(output, key="df", mode="w")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="")
    parser.add_argument("--input", type=str, required=True, help="Input file name")
    parser.add_argument(
        "--temporal", type=str, required=True, help="Temporal results file name"
    )
    parser.add_argument(
        "--spatial", type=str, required=True, help="Spatial results file name"
    )
    parser.add_argument("--output", type=str, required=True, help="Output folder name")
    parser.add_argument(
        "--ref", type=int, required=False, default=15, help="Reference sensor index"
    )

    args = parser.parse_args()
    print("Args received:", args)
    main(args.input, args.output, args.ref, args.temporal, args.spatial)

import argparse

import numpy as np
import pandas as pd
from sigmoid.base import SigmoidLagEstimator
from tqdm import tqdm


def main(input, output, ref_sensor_idx, max_lag=10):
    print(f"Input received: {input}, the output will be saved in {output}")
    df = pd.read_hdf(input, key="df")
    print(len(df))
    print(df.columns.tolist())
    network = df["network"][0]
    print(f"Reference sensor: {ref_sensor_idx}")
    ref_sensor = network[ref_sensor_idx]

    lag_estimator = SigmoidLagEstimator((-max_lag, max_lag), 235)
    print("Lag ranges between:", -max_lag, max_lag)

    results = []
    for _, row in tqdm(df.iterrows(), total=len(df), desc="Processing data"):
        weight_pyramids = row.weight_pyramids
        bias_pyramids = row.bias_pyramids
        lag_likelihoods = np.zeros((len(network), 235))

        try:
            ideal_lags = row.ideal_times * 48e3 - row.ideal_times[ref_sensor_idx] * 48e3
        except TypeError:
            ideal_lags = np.zeros(len(network))

        for sensor_idx, sensor in enumerate(network):
            if sensor == ref_sensor:
                continue
            try:
                likelihoods = lag_estimator.predict_proba(
                    signal_A=None,
                    signal_B=None,
                    weight_pyramid_A=weight_pyramids[sensor_idx],
                    weight_pyramid_B=weight_pyramids[ref_sensor_idx],
                    bias_pyramid_A=bias_pyramids[sensor_idx],
                    bias_pyramid_B=bias_pyramids[ref_sensor_idx],
                )
                aggregated_likelihoods = np.prod(likelihoods, axis=0)
            except TypeError as e:
                print(f"Exception caught: {e}, recovering with uniform likelihood")
                aggregated_likelihoods = np.ones(235) / 235
            # aggregate the result of each layer by summing across layers
            lag_likelihoods[sensor_idx, :] = aggregated_likelihoods

        result = {
            "target_id": [row["target_id"]],
            "task_id": [row["task_id"]],
            "recording_id": [row["recording_id"]],
            "label_idx": [row["label_idx"]],
            "lag_likelihoods": [lag_likelihoods],
            "lag_space": [lag_estimator.lag_space],
            "ideal_lags": [ideal_lags],
        }
        results.append(pd.DataFrame(result))
    # Create a new DataFrame to store the results
    results_df = pd.concat(results, ignore_index=True)
    results_df.to_hdf(output, key="df", mode="w")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="make temporal estimations")
    parser.add_argument("--input", type=str, required=True, help="Folder path")
    parser.add_argument("--output", type=str, required=True, help="Output folder name")
    parser.add_argument(
        "--ref", type=int, required=False, default=15, help="Reference sensor index"
    )

    args = parser.parse_args()
    print("Args received:", args)
    main(args.input, args.output, args.ref)

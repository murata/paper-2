import argparse

import numpy as np
import pandas as pd
from localizers.tdoa import TDOAHyperbolaeSolver
from sigmoid import Event, MatchedEvent
from tqdm import tqdm


def main(input, output, ref_sensor_idx, temporal_file):
    print(f"Input received: {input}, the output will be saved in {output}")
    df = pd.read_hdf(input, key="df")
    network = df["network"][0]
    print(f"Reference sensor: {ref_sensor_idx}")
    temporal_df = pd.read_hdf(temporal_file, key="df")
    print("Column Names in input_df:", df.columns.tolist(), "len:", len(df))
    print(
        "Column Names in temporal_df:",
        temporal_df.columns.tolist(),
        "len:",
        len(temporal_df),
    )
    ref_sensor = network[ref_sensor_idx]
    localizer = TDOAHyperbolaeSolver(network=network)
    results = []
    for idx, temporal_row in tqdm(
        temporal_df.iterrows(), total=len(temporal_df), desc="Processing Rows"
    ):
        lag_likelihoods = temporal_row.lag_likelihoods
        lag_space = temporal_row.lag_space
        lag_mle = lag_space[np.argmax(lag_likelihoods, axis=1)]
        # create event
        matched = MatchedEvent()
        for sensor_idx, sensor in enumerate(network):
            if sensor == ref_sensor:
                continue
            event = Event(
                time=lag_mle[sensor_idx], energy=1, confidence=1, detected_by=sensor
            )
            matched.add_event(event)
        estimated_location = localizer.predict(matched_event=matched)
        # Collect data for each row in a dictionary
        result = {
            "task_id": [temporal_row["task_id"]],
            "recording_id": [temporal_row["recording_id"]],
            "label_idx": [temporal_row["label_idx"]],
            "estimated_location": [estimated_location],
        }
        results.append(pd.DataFrame(result))

    # Convert list of dictionaries to DataFrame
    results_df = pd.concat(results, ignore_index=True)
    results_df.to_hdf(output, key="df", mode="w")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="")
    parser.add_argument("--input", type=str, required=True, help="Input file name")
    parser.add_argument(
        "--temporal", type=str, required=True, help="Temporal results file name"
    )
    parser.add_argument("--output", type=str, required=True, help="Output folder name")
    parser.add_argument(
        "--ref", type=int, required=False, default=15, help="Reference sensor index"
    )

    args = parser.parse_args()
    print("Args received:", args)
    main(args.input, args.output, args.ref, args.temporal)

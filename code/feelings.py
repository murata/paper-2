from concurrent.futures import ProcessPoolExecutor, as_completed

import matplotlib.patches as patches
import numpy as np
import pandas as pd
import scipy.stats as ss
from localizers.tdoa import TDOAHyperbolaeSolver
from sigmoid import Event, MatchedEvent, Sensor, SensorNetwork


def get_random_network(num_sensors, sensor_radius, center=(0, 0), sampling_rate=48_000):
    network = SensorNetwork()

    # Define the mean and covariance for the 2D normal distribution
    mean = np.array(center)
    cov = np.eye(2) * sensor_radius**2 / 4

    # Generate random locations for the sensors
    locs = np.random.multivariate_normal(mean, cov, num_sensors)
    locs = np.hstack([locs, np.zeros((int(num_sensors), 1))])
    # Add the sensors to the network
    for loc in locs:
        network.add_sensor(Sensor(loc, sampling_rate))

    return network


def get_circular_network(
    num_sensors, sensor_radius, center=(0, 0), sampling_rate=48_000
):
    network = SensorNetwork()

    angle_step = 2 * np.pi / num_sensors
    locs = [
        np.array(
            [
                np.cos(i * angle_step) * sensor_radius + center[0],
                np.sin(i * angle_step) * sensor_radius + center[1],
                0,
            ]
        )
        for i in range(num_sensors)
    ]
    _ = [network.add_sensor(Sensor(x, sampling_rate)) for x in locs]
    return network


def get_linear_network(num_sensors, start_pt, end_pt, sampling_rate=48_000):
    network = SensorNetwork()
    xs = np.linspace(start_pt[0], end_pt[0], num_sensors)
    ys = np.linspace(start_pt[1], end_pt[1], num_sensors)
    z = 0

    for _x, _y in zip(xs, ys):
        network.add_sensor(Sensor([_x, _y, z], sampling_rate))

    return network


def get_random_target_location(start_pt, end_pt):
    x = np.random.uniform(start_pt[0], end_pt[0])
    y = np.random.uniform(start_pt[1], end_pt[1])
    z = 0
    return np.array([x, y, z])


def plot_spatially(ax, network, x_true, x_ideal, x_est, lower_bounds, upper_bounds):
    for sensor in network:
        ax.plot(sensor.location[0], sensor.location[1], "k.")

    ax.plot(x_true[0], x_true[1], "go", label="target")
    ax.plot(x_ideal[0], x_ideal[1], "c+", label="ideal")
    if x_est is not None:
        for idx, x in enumerate(x_est):
            if idx == 0:
                label = "est"
            else:
                label = "_"
            ax.plot(x[0], x[1], "g+", label=label)
    if lower_bounds is None:
        return ax
    width = upper_bounds[0] - lower_bounds[0]
    height = upper_bounds[1] - lower_bounds[1]
    rect = patches.Rectangle(
        lower_bounds[:2],
        width,
        height,
        linewidth=1,
        edgecolor="none",
        facecolor=[0.7, 0.7, 0.7],
        label="floor",
    )
    ax.add_patch(rect)

    return ax


def plot_contour(ax, xx, yy, contour, label):
    zz = contour.reshape(xx.shape)
    contourf = ax.contourf(xx, yy, zz, levels=50, cmap="viridis")
    cbar = ax.figure.colorbar(contourf, ax=ax)
    cbar.set_label(label)
    ax.set_xlabel("X coordinate")
    ax.set_ylabel("Y coordinate")
    return ax


def evaluate_cost_function(localizer, lamb, events, lower_bounds, upper_bounds):
    x = np.linspace(lower_bounds[0], upper_bounds[0], 100)
    y = np.linspace(lower_bounds[1], upper_bounds[1], 100)
    xx, yy = np.meshgrid(x, y)
    xyz = np.vstack((xx.ravel(), yy.ravel(), np.zeros_like(yy.ravel()))).T
    residuals = np.zeros(len(xyz))
    costs = np.zeros(len(xyz))
    jacs_residual = np.zeros_like(residuals)
    jacs_cost = np.zeros_like(residuals)

    # for idx, location in enumerate(xyz):
    #     res = localizer.residuals_hyper(location, events.events)
    #     residuals[idx] = np.sum(np.square(res))
    #     jac_res = localizer.jacobian_hyper(location, events.events)
    #     jacs_residual[idx] = np.linalg.norm(jac_res @ jac_res.T)
    #     #
    #     cost = localizer.cost(location, events.events, lamb=lamb)
    #     jac_cost = localizer.jacobian_cost(location, events.events, lamb=lamb)
    #     costs[idx] = np.sum(np.square(cost))
    #     jacs_cost[idx] = np.linalg.norm(jac_cost @ jac_cost.T)
    return xx, yy, residuals, jacs_residual, costs, jacs_cost


def evaluate_network(
    network, wave_velocity, target_location, lamb, lower_bounds, upper_bounds, **kwargs
):
    localizer = TDOAHyperbolaeSolver(network, wave_velocity)
    ideal_events = localizer.inverse(target_location)
    x_ideal = localizer.predict(
        ideal_events,
        initial_guess=None,
        lamb=lamb,
        lower_bounds=lower_bounds,
        upper_bounds=upper_bounds,
    )
    xx_ideal, yy_ideal, res_ideal, jac_res_ideal, cost_ideal, jac_cost_ideal = (
        evaluate_cost_function(
            localizer, lamb, ideal_events, lower_bounds, upper_bounds
        )
    )
    result_df = pd.DataFrame(
        {
            "type": ["ideal"],
            "xx": [xx_ideal],
            "yy": [yy_ideal],
            "residuals": [res_ideal],
            "jac_residuals": [jac_res_ideal],
            "cost": [cost_ideal],
            "jac_cost": [jac_cost_ideal],
            "network": [network],
            "covariance": [0.0],
            "target_location": [target_location],
            "estimated_location": [x_ideal],
            "ideal_location": [x_ideal],
        }
    )

    noise_matrix = ss.multivariate_normal(mean=kwargs["mean"], cov=kwargs["cov"]).rvs(
        size=kwargs["num_repeat"]
    )

    noisy_events_all = []
    x_noisy_all = []

    for idx in range(kwargs["num_repeat"]):
        noisy_events = MatchedEvent()
        noise_vec = noise_matrix[idx, :]
        for event, noise in zip(ideal_events.events, noise_vec):
            noisy_event = Event(
                time=event.time + noise,
                energy=event.energy,
                confidence=event.confidence,
                detected_by=event.detected_by,
            )
            noisy_events.add_event(noisy_event)
        noisy_events_all.append(noisy_events)

    # Sequentially predict locations for noisy events
    for noisy_events in noisy_events_all:
        x_noisy = localizer.predict(
            noisy_events,
            initial_guess=None,
            lamb=lamb,
            lower_bounds=lower_bounds,
            upper_bounds=upper_bounds,
        )
        x_noisy_all.append(x_noisy)

    noisy_results = []

    # Sequentially evaluate cost function for noisy events
    for noisy_events in noisy_events_all:
        noisy_result = evaluate_cost_function(
            localizer,
            lamb,
            noisy_events,
            lower_bounds,
            upper_bounds,
        )
        noisy_results.append(noisy_result)

    # Store the results in a DataFrame
    for idx, (noise_vec, x_noisy, noisy_result) in enumerate(
        zip(noise_matrix, x_noisy_all, noisy_results)
    ):
        xx_ideal, yy_ideal, res_ideal, jac_res_ideal, cost_ideal, jac_cost_ideal = (
            noisy_result
        )
        result = pd.DataFrame(
            {
                "type": ["noisy"],
                "xx": [xx_ideal],
                "yy": [yy_ideal],
                "residuals": [res_ideal],
                "jac_residuals": [jac_res_ideal],
                "cost": [cost_ideal],
                "jac_cost": [jac_cost_ideal],
                "network": [network],
                "covariance": [np.linalg.det(np.cov(noise_matrix))],
                "target_location": [target_location],
                "estimated_location": [x_noisy],
                "ideal_location": [x_ideal],
            }
        )

        result_df = pd.concat([result_df, result], ignore_index=True)
    return result_df

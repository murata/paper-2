\section{Sigmoid Decomposition Theorem}

\begin{lem}[Sigmoid Function $\sigma(x)$]
    The sigmoid function \(\sigma: \mathbb{R} \rightarrow [0, 1] \) is a smooth function that is defined as \( \sigma(x) = \frac{1}{1 + \exp{(-x)}} \).
    The sigmoid function \(\sigma(x)\) is bounded, monotonic increasing, and continuous on \( \mathbb{R} \).
    It has horizontal asymptotes at \( y = 0 \) and \( y = 1 \).
\end{lem}

\begin{thm}[Sigmoid Decomposition Theorem]
    Let \( f: [x_0, x_1] \rightarrow \mathbb{R} \) be a monotonic non-decreasing continuous function such that for all $x \in [x_0, x_1], y_0 \leq f(x) \leq y_1$, where \( x_0, x_1, y_0, y_1 \in \mathbb{R} \) with \( x_0 \leq x_1 \) and \(y_0 \leq y_1\).
    Then, for all $x \in [x_0, x_1]$, the function \( f(x) \) can be approximated by the infinite weighted sum of shifted sigmoid functions as,
    \begin{equation}
            f(x) = \hat{f}(x) = \sum_{i=0}^{\infty} w_i \sigma(i x + b_i)~,
    \end{equation}1
    where $w_i \in [0, \infty)$ and $b_i \in (-\infty, \infty)$ are the weight and bias corresponding to $i^{th}$ sigmoid function, respectively.
\end{thm}

\begin{dft}
    The $n^{th}$ order approximation is defined as the sum of the first \( n \) terms of the series as given by \( \hat{f}_n(x) = \sum_{i=0}^n w_i \sigma(i x + b_i)\).
\end{dft}

\begin{dft}
    The error function \( e_n(x) \) for this approximation is given by \( e_n(x) = f(x) - \hat{f}_n(x)\).
\end{dft}

\hrule

\begin{lem}[Discrete Sigmoid Function]
    The discrete sigmoid function \(\sigma: \mathbb{Z} \rightarrow [0, 1] \) is defined as
    \[ \sigma[k] = \frac{1}{1 + \exp{(-k)}}~. \]
    The discrete sigmoid function \(\sigma[k]\) is bounded and monotonic increasing on \( \mathbb{Z} \).
    The function approaches 0 as \( k \) becomes large and negative, and approaches 1 as \( k \) becomes large and positive.
\end{lem}

\begin{thm}[Discrete Sigmoid Decomposition Theorem]
    Let \( f: \mathcal{K} \rightarrow \mathbb{R} \) be a monotonic non-decreasing sequence defined over a regularly spaced set \(\mathcal{K} = \{k_1, k_2, \ldots, k_N\}\), where each \( k_i \) follows an arithmetic progression with a common difference of \(\Delta_k\), i.e., \( k_{i+1} = k_i + \Delta_k \) for all \( i \) with \( 1 \leq i < N \).
    We have \( y_0 \leq f[k] \leq y_1 \) for all \( k \in \mathcal{K} \), and \( k_i, y_0, y_1 \in \mathbb{R} \) and \( \Delta_k \in \mathbb{R}_{>0} \).
    Then, for all \( k \in \mathcal{K} \), the sequence \( f[k] \) can be approximated by the infinite weighted sum of shifted discrete sigmoid functions as
    \[ f[k] = \hat{f}[k] = \sum_{i=0}^{\infty} w_i \sigma[i k + b_i]~, \]
    where \( w_i \in [0, \infty) \) and \( b_i \in \mathbb{Z} \) are the weight and bias corresponding to the \( i^{th} \) discrete sigmoid function, respectively.
\end{thm}

\begin{dft}
    The \( n^{th} \) order approximation is defined as the sum of the first \( n \) terms of the series as given by
    \[ \hat{f}_n[k] = \sum_{i=0}^n w_i \sigma[i k + b_i]~. \]
\end{dft}

\begin{dft}
    The error function \( e_n[k] \) for this approximation is given by
    \[ e_n[k] = f[k] - \hat{f}_n[k]~. \]
\end{dft}

\begin{prf}
    Consider a monotonic non-decreasing discrete function \( f: \mathcal{K} \rightarrow \mathbb{R} \) defined on a set \( \mathcal{K} \). We can express \( f[k] \) as a series of step functions where each step function \( S_i[k] \) corresponds to an increase in the function value at some point \( k_i \in \mathcal{K} \).

    Define \( S_i[k] \) such that:
    \[
        S_i[k] = \Delta f_i H(k - k_i)
    \]
    where \( \Delta f_i = f[k_i] - f[k_{i-1}] \) for \( i > 1 \) and \( \Delta f_1 = f[k_1] \) if \( f \) is not defined for \( k < k_1 \).
    The function \( f[k] \) can then be written as the sum of these step functions:
    \[ f[k] = \sum_{i=1}^{N} S_i[k]. \]
    Each step function \( S_i[k] \) can be approximated by a discrete sigmoid function:
    \[ \hat{S}_i[k] = \Delta f_i \cdot \sigma[k - k_i + \epsilon_i], \]
    where \( \epsilon_i \) is a small positive real number ensuring that the sigmoid function transitions from approximately 0 to 1 at \( k_i \).

    Thus, \( f[k] \) can be approximated by the sum of these sigmoid approximations:
    \[ \hat{f}[k] = \sum_{i=1}^{N} \hat{S}_i[k]. \]
    For any given \( \epsilon > 0 \), by choosing \( \epsilon_i \) sufficiently small and adjusting the weights \( \Delta f_i \), the cumulative sum of the sigmoid functions \( \hat{f}[k] \) can be made to lie within \( \epsilon \) of \( f[k] \) for all \( k \in \mathcal{K} \), thereby proving the theorem.
\end{prf}

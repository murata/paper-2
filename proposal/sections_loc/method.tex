The proposed technique employs a multi-sensing scheme, i.e., a sensor network observes the same natural phenomenon in a synchronized and centralized manner.
All the sensors are connected to \gls{fc} where their measurements are used to deduce the location of \gls{toi}.
We assume each event is unique; therefore, we follow the intuition that obtaining multiple observations should increase our ability to localize \gls{toi}.

In this work, I aim to find a mapping between two concepts: cumulative signal energy--shown in \Cref{dft:cse}--and \gls{cdf} of distance between the target and the event--shown in ~\Cref{dft:cdf}--.

\begin{dft}[Cumulative Signal Energy]
    In this approach, I propose representing the energy of the captured signals as a time-series defined as:
    \begin{equation}
        c_i[k] = \sum_{n=1}^{k} z_i[n]^2 = z_i[k]^2 + \sum_{n=1}^{k-1} c_i[n]~.
        \label{eq:energy}
    \end{equation}
    \label{dft:cse}
\end{dft}
\input{figs/cse_main.figure}

This representation not only shows what the signal energy is, it also represents how the signal energy accumulation is evolving with time.
In other words, this representation of the energy is \emph{an alternative} view of the dynamics of the observed phenomenon.

\begin{rmk}
    Some properties of cumulative energy signal $c_i[k]$ are given below.

    \begin{itemize}
        \item $c_i[1] = 0$ and $c_i[n_k] = e_i$.
        \item $c_i[a] \geq c_i[b]$,\, iff $a \geq b$.
        \item $c_i[k], \frac{\partial}{\partial\, k}c_i[k] \geq 0,\, \forall k \in \mathcal{K}$.
    \end{itemize}
    \label{rmk:ci}
\end{rmk}

By inspection, one might realize \Cref{rmk:ci} shows $c_i[k]$ is a monotonic non-decreasing of function of $k$.


\begin{dft}[\gls{cdf} of the distance between sensor $i$ and the event $f_{D_i}(d)$]
    The \gls{cdf}, denoted by \(f_{D_i}(d)\), represents the probability that the distance between sensor \(i\) and the event is less than or equal to \(d\). Mathematically, it is given by:
    \[
    f_{D_i}(d) = P(D_i \leq d)
    \]
    where \(D_i\) is the random variable denoting the distance between sensor \(i\) and the event.
    \label{dft:cdf}
\end{dft}
\input{figs/cdf_main.figure}

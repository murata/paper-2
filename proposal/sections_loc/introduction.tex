This document proposes a passive multi-sensor target localization technique.
We consider a target localization problem of a dynamic \gls{toi} that is located at \gls{x} in a localization space \gls{setS}.
% The localization space \gls{setS} is a part of a much higher dimensional \gls{als}.
% This event causes some perturbations in \gls{als}.
We aim to determine the location of \gls{toi} by tracking various perturbations in the localization space \gls{setS}.
These perturbations, that are also be considered as events, might manifest as a reflection of ambient radio frequencies, a disruption in the local acoustic field, or an alteration in the thermal profile of the environment.
For example, consider a submarine or submersible vehicle moving through the ocean.
The submarine's acoustic signature, when detected by passive \gls{sonar} arrays (networks) on ships around the submarine, represents an event at a specific location within the ocean.
This specific noise or acoustic signature of the submarine can be thought of as the event occurring at location \gls{x} in our localization space \gls{setS}.
Many aspects of human perception, including acoustic and optical perception, serve as examples of this passive localization method.
As an analogy, for instance, our eyes passively capture visual information without emitting light, much like the passive sensors described above.

\input{figs/overview.tikz}

The passive sensors positioned within the localization space \gls{setS} detect these subtle variations and utilize them to infer the position \gls{x} of the \gls{toi}.
These variations often exhibit a sharp, impact-like structure, akin to a blip on a \gls{sonar} screen.
This structure is characterized by a sudden onset that overcomes the ambient noise, followed by a rapid decay that diminishes quickly below the surrounding ambient noise.
Considering each event results in unique perturbations and a set of sensors observing them in the ambient space, our task is to distinguish \gls{toi} from the background and provide an estimated location vector \gls{xhat} corresponding to it.

\subsection{Problem Statement}
We consider a multi-sensor multi-target localization problem where different aspects of the problem are given in the definitions below.
The localization configuration is stated as in \Cref{dft:configuration}.
\begin{dft}[Configuration]
    Localization space \gls{setS} is a closed subset of a building.
    There exist \gls{no} number of occupants traversing in \gls{setS} in a non-evasive manner.
    The occupants generate \gls{ne} number of \emph{non-overlapping} events that are observed by \gls{ns} number of sensors.
    \label{dft:configuration}
\end{dft}


In order to derive the occupant location from the sensor measurements, we start modeling the sensor measurement as given in \Cref{dft:sensor_measurement}.
\begin{dft}[Sensor Measurement]
    Let \gls{zik} $\in \mathbb{R}$ be the time-domain measurement of sensor $i \in$ \gls{setI} where the index set $\mathcal{I} = \{1, \ldots, n_s\}$ denotes the indices of the sensors in the network of size \gls{ns}.
    Assuming a set of time-steps \gls{setK} $= \{1, \ldots, \text{\gls{nk}}\}$ corresponds to a single event, a measurement vector \gls{zi} can be defined as:
    \begin{equation}
        \vect{z}_i = z_i\left[\forall k \in \mathcal{K}\right] = (z_i[1], \ldots, z_i[n_k])^\top \in \mathbb{R}^{n_k},\, \forall i \in \mathcal{I}~.
    \end{equation}
    \label{dft:sensor_measurement}

    Given the imperfections of the sensor network, the localization task before us is indeed challenging.
    The measurements are not only quantized and taken at discrete timesteps, but they are also skewed by both sensor noise and bias.
    This results in the measurement vector \gls{zi} being a mix of the intended measurement and \gls{zeta}, a random variable characterizing these sensor imperfections.
    Assume \gls{chi} represents the dynamics of the sensor, taking into account the spatial constraints given by \gls{setS} and the duration of the natural phenomenon, \gls{setK}.
    With this understanding, the measurement vector can be expressed as:
    \begin{equation}
        \vect{z}_i = \vect{\chi}\left( \vect{x}, \vect{\zeta}_i; \mathcal{K}, \mathcal{S} \right)
    \end{equation}
\end{dft}

From a mathematical standpoint, we are faced with an inverse estimation problem.
The goal is to determine the target location based on observations from the ambient space, even though these observations have been distorted by the sensor's imperfections.

\subsection{Relevant Work}
In the previous work~\cite{ambarkutuk2023}, we employed the signal energy \gls{ei} to quantify the magnitude of the measurement vector \gls{zi} as shown below:
\begin{equation}
    e_i = \norm{\vect{z_i}}_2^2~.
\end{equation}

\subsubsection{Alajlouni Heuristic\todo{cite needed.}}
\todo{Add a brief summary.}
\subsubsection{Alajlouni New Fast\todo{cite needed.}}
\todo{Add a brief summary.}
\subsubsection{Alajlouni MLE\todo{cite needed.}}
\todo{Add a brief summary.}

\subsubsection{Our take}
We characterized the relationship between \gls{ei} and the distance \gls{di} between the footstep and the sensor with a nonlinear function--specifically, an exponential decay--.
This relationship was formally described as:
\begin{equation}
    d_i = g(e_i; \vect{\beta}_i) = \beta_{0,i} \exp{\left(\beta_{1,i} \ e_i\right)}~,
\end{equation}
where $\vect{\beta}_i = (\beta_{0, i}, \beta_{1, i})^\top$ is the calibration vector concerning sensor $i$.
The calibration matrix $\vect{B} = \left(\vect{\beta}_1^\top, \ldots, \vect{\beta}_{n_s}^\top\right) \in \mathbb{R}^{n_s \times 2}$ is obtained before the system is deployed from some calibration data.
We used the \gls{pdf} transformation theorem to project the uncertainty models of the measurements $f_{E_i}(e)$ to that of the distance estimations as shown below:

\begin{equation}
    f_{D_i}(d) = f_{E_i}(g^{-1}(d; \vect{\beta}_i)) \, \left\lvert \frac{\partial g^{-1}}{\partial d} \right\rvert~.
\end{equation}

We then employed sensor fusion with each sensor's belief about target location to minimize the localization error caused by the measurement errors.
However, the length of propagation path may or may not represent this shortest distance between the footstep and the sensor locations.
Also, nonlinear nature of wave propagation in solids and complexity of boundary conditions as well as alternative wave-fronts reaching the sensors, this propagation model rendered some of our sensors useless.
We rectified for this by employing our \gls{bse} algorithm.

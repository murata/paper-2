\section{Introduction\label{sec:intro}}
Decomposition techniques in signal processing are pivotal for dissecting and understanding the intricate components of signals.
These techniques have evolved significantly from the early works of Fourier in the 1820s~\cite{Fourier1822} to contemporary advancements, enabling a nuanced analysis of complex signals.
In this paper, we propose yet another decomposition method that has a potential to tackle different signal processing problems such as signal segmentation and denoising, and event detection, etc. with signals that are non-stationary and stochastic in nature.
Our approach is centered around the analysis of how the energy or power of a signal accumulates over time.
\gls{sd} operates on this cumulative power metric as a proxy to the actual signal.
The proposed methodology, named \gls{sd}, breaks down this proxy representation of the signal in hand into a finite number of sigmoid functions with varying weights and biases.

Monotonic non-decreasing functions are essential across various scientific fields due to their property of consistent growth or stability.
In probability theory, for instance, the \gls{cdf} is a classic example of such function.
Real-world applications, such as reliability engineering and economic utility theory, often rely on the principles of non-decreasing functions.
This concept underpins logical consistency in models and analyses, making these functions critical for both theoretical and practical applications.

This paper explores the realm of signal decomposition through the lens of monotonic non-decreasing signals, analyzed from a power-centric perspective.
This paper aims to explore the intricate world of signal decomposition methodologies, with a specific focus on monotonic non-decreasing signals from a power-centric perspective.
The study delves into the challenges and limitations posed by traditional methods, seeking to address specific gaps identified in the current literature.
In doing so, it presents a novel signal transform that can represent the the monotonic non-decreasing signals better than the contemporary work.

\subsection{Problem Statement}
The study of monotonic non-decreasing sequences in signal decomposition offers a unique viewpoint for understanding power accumulation in signals, especially in non-stationary and stochastic contexts.
This perspective is vital for interpreting the inherent nature and behavior of signals, which traditional decomposition methods might obscure.
\Cref{dft:monotonic} provides a formal definition of such sequences.

\begin{dft}[Monotonic Non-Decreasing Discrete Sequences]
    \label{dft:monotonic}
    Let \( x: \mathcal{N} \rightarrow \mathbb{R} \) be a monotonic non-decreasing sequence defined over a regularly spaced set of points shown as \(\mathcal{N} = \{0, 1, \ldots, N-1\}\).
    A sequence is said to be a monotonic non-decreasing sequence if for every pair of indices \( i, j \in \mathcal{N} \) with \( i \leq j \), it holds that \( x[i] \leq x[j] \).
    In other words, the value of the sequence at a later index is never less that of an earlier index.
    This condition ensures that the sequence does not decrease as the index increases.
    Additionally, the sequence is bounded if \( y_0 \leq x[n] \leq y_1 \), where \( y_0, y_1 \) are real numbers, meaning that the values of the sequence are confined within the range defined by \( y_0 \) and \( y_1 \).
\end{dft}

Decomposition techniques in signal processing vary widely in their characteristics and applicability. The effectiveness of a particular method depends on the signal's nature and the specific requirements of the analysis.
While some techniques are better suited for certain types of signal breakdowns, others might excel in computational efficiency or provide unique insights.
Our proposed signal decomposition methodology is designed to fulfill the need for an effective decomposition method for signals characterized by non-stationary behavior.
However, all signal decomposition methodology are expected to share the loose definitions shown in \Cref{dft:decomposition,dft:reconstruction}.

\begin{dft}[Decomposition of Signals]
    \label{dft:decomposition}
    Signal decomposition in signal processing is the process of breaking down a signal into its constituent components, simplifying its analysis, interpretation, or further processing. Representing the space of original signals as \( S \), a decomposition operation \( \mathcal{D} \) can be defined as a mapping:

    \[ \mathcal{D}: S \rightarrow \{C_0, \ldots, C_{n-1}\}~, \]

    where each \( C_i \) is a component of the original signal \( s \in S \). These components represent different aspects or characteristics of the signal, varying with the decomposition method.
\end{dft}

Following the definition of signal decomposition \(\mathcal{D}\), the concept of reconstruction or synthesis is considered. This process involves reassembling the decomposed components back into a signal form, which is essential in applications where the original signal's holistic representation is needed post-decomposition.

\begin{dft}[Reconstruction of Signals]
    \label{dft:reconstruction}
    Signal reconstruction, denoted as \(\mathcal{D}^{-1}\), is the process of reassembling the components of a decomposed signal back into its original form. This operation can be viewed as a mapping:

    \[ \mathcal{D}^{-1}: \{C_0, \ldots, C_{n-1}\} \rightarrow S \]

    where a set of components \( \{C_0, \ldots, C_{n-1}\} \) is transformed back into the original signal \( s \in S \). The integrity of a signal post-analysis is ensured through accurate reconstruction from its components.

    The feasibility and exact methodology of the reconstruction process \(\mathcal{D}^{-1}\) depend on the nature of the decomposition \(\mathcal{D}\) and the properties of the components and the original signal space.
\end{dft}

\subsection{Summary of Contributions}
Our primary contribution is the development of the \gls{sd}, a method tailored for decomposing monotonic non-decreasing signals. Unlike conventional decomposition methods, the \gls{sd} effectively discards noise, particularly white noise, inherent in signals, owing to the properties of its sigmoid components.
\st{Because the proposed transform is linear, superposition and other advantages of linear transforms are contained in the Discrete Sigmoid Transform.}
\todo{I need to revisit this.}

\subsection{Outline}
The paper is structured to provide a detailed exploration of the \gls{sd} and its applications in signal processing.
Following this introduction, \Cref{sec:literature} presents a Literature Review, placing our work within the context of existing research.
In \Cref{sec:theory}, we introduce the concept and theoretical framework of the \gls{sd}.
\Cref{sec:methodology} outlines our methodology, including experimental design and data analysis.
\Cref{sec:results} presents our findings, followed by a discussion in comparison with established theories.
The paper concludes in \Cref{sec:conclusion}, summarizing our research's broader impact and suggesting future research directions, including potential expansions and localization studies.
This section also sketches out avenues for future work, proposing potential expansions of our research and suggesting topics for further investigation.

\glsresetall{}
\section{The Sigmoid Decomposition\label{sec:theory}}
\gls{sd}, denoted as \( \mathcal{S} \), is a multi-scale signal analysis methodology which yields structurally-layered features for a given sensor measurement.
Let \( \vect{x} \) be the measurement vector containing the sensor readings for a duration of time.
In this work, we denote the measurement vector as a column vector \( \vect{x} = \left( x[0], \ldots, x[N-1] \right)^\top \in \mathbb{R}^N \).
Here, \( x[n] \in \mathbb{R} \) denotes a discrete, real, bounded, non-stationary, and, stochastic process representing measurements acquired from a sensor at time steps \( n \in \mathcal{N} = \{0, \ldots, N-1\} \).
The set of time instances \( \mathcal{N} \) encompasses a sequence of discrete, regularly sampled time instances, represented as integers in sequential order.

Let \( p[n] = \sum_{i}^{n} x[n]^2 \) be the cumulative power of the signal \( x[n] \) up to sample $n$ showing how signal power accumulates over time.
\gls{sd} originates from the observation that the cumulative power of a measurement vector $\vect{x}$ yields a monotonic non-decreasing function.
\gls{sd} uses this representation of signal \( x[n] \), as the proxy and decomposes it as weighted and shifted sum of discrete sigmoid function given as \( \sigma[n] = \frac{1}{1 + e^{-x}} \).
\Cref{eq:cumulative-power} below provides this relationship.

\begin{equation}
    p[n] = \sum_{i=0}^{n}{x[i]^2} = \sum_{i=0}^{N-1} w_i \sigma \left[ \frac{n-b_i}{\kappa} \right]
    \label{eq:cumulative-power}
\end{equation}
Notice, \( p[n] \) forms a monotonic non-decreasing function which makes it ideal sigmoid function to approximate it.
The discrete sigmoid function, serving as the kernel for our decomposition, is defined on \(\sigma: \mathbb{Z} \rightarrow [0, 1] \), and, is bounded and monotonically increasing on \( \mathbb{Z} \).
It approaches 0 as \( n \) becomes large and negative, and approaches 1 as \( n \) becomes large and positive.

\gls{sd} is able the measurement vector \( \vect{x} \) into \( K \) number of layers.
Each layer that is characterized by a weight vector \( \vect{w}_i \) and a bias vector \( \vect{b}_i \) is sufficient to reconstruct the signal $x[n]$ back.
At the \( i^{th} \) level of feature pyramid, the signal $x[n]$ is decomposed into $i+1$ sigmoids whose weights $\vect{w}_i \in \mathbb{R}^{i+1}_{+}$ and biases $\vect{b}_i \in \mathbb{R}^{i+1}_{+}$ where $i \in \mathcal{K} = \{0, \ldots, N - 1 \}$.
Because each layer provides different number of components, \gls{sd} yields two $K$-level feature pyramids (or sets) $\mathcal{W} \triangleq \left\{\vect{w}_0, \ldots, \vect{w}_{N-1} \right\}$,  $\mathcal{B} \triangleq \left\{\vect{b}_0, \ldots, \vect{b}_{N-1} \right\}$, and a masking vector $\vect{m} \in \mathbb{R}^N$.
The \gls{sd} is formally shown as in \Cref{eq:sd}.
\begin{equation}
    \mathcal{S}: \vect{x} \rightarrow \{ \mathcal{W}, \mathcal{B}, \vect{m} \}~.
    \label{eq:sd}
\end{equation}

The pyramids $\mathcal{W}$ and $\mathcal{B}$ are the weights and biases of the sigmoid functions, while the mask $\vect{m}$ is used to tackle the sign ambiguity when signal is reconstructed from its decomposition.
The masking vector is defined as the sign of the signal \( \vect{m} \triangleq \sign{\left( \vect{x} \right)} \).
A graphical representation of the feature pyramids are given in \Cref{fig:pyramids}.
The figure demonstrates the fact that as the pyramid gets deeper, the temporal resolution increases with increasing number of sigmoid functions.
On the other hand, as the pyramid gets deeper, signals semantics becomes vague.

\input{figs/pyramid.tikz}

\Cref{alg:sd} gets the input signal $\vect{x}$ as the argument and yields the complete weight $\mathcal{W}$ and bias $\mathcal{B}$ pyramids, as well as the masking vector $\vect{m}$.
The algorithm starts by calculating the cumulative power of the input vector $\vect{x}$.
Consequently, for each layer of the pyramid indexed with $k$, the calculated cumulative power is split into $k+1$ equal length segments.
If the length of the signal is not divisible by the layer index, the cumulative power of the signal is appended with its last member to remedy this.
Note that this appendange is equivalent to adding zeros at the end of the signal $x[n]$.
The weights and biases concerning each segment is then calculated as follows:
For each segment indexed with $i$, we set the corresponding element of weight vector with the total increase observed in the segment.
As for the bias, the time index that maximizes the derivative of the cumulative power $p[n]$.
By doing so, we place the sigmoid of this segment at the point where maximum change, likely corresponding to an event, is observed.

\begin{algorithm}[ht]
    \caption{Sigmoid Decomposition (SD)}
    \label{alg:sd}
    \begin{algorithmic}[1]
    \Require Signal $x[n]$, $n \in \mathcal{N} = \{0, \ldots, N-1\}$
    \Ensure Feature Pyramids $\mathcal{W}, \mathcal{B}$ and Masking Vector $\vect{m}$

    \Function{SD}{$x[n]$}
        \State Compute the cumulative power of $x[n]$: $p[n] \gets \sum_{i=0}^{N-1} \lvert x[i] \rvert^2$
        \State Initialize feature pyramids: $\mathcal{W} \gets \{\}, \mathcal{B} \gets \{\}$
        \For{$k \gets 0$ \textbf{to} $N-1$}
            \State Let $\Delta = \frac{N}{k+1}$
            \State Initialize weights and biases: $\vect{w}_k \gets \vect{0}_{k+1}, \vect{b}_k \gets \vect{0}_{k+1}$
            \For{$i \gets 0$ \textbf{to} $k$}
                \State $increase \gets p[\lfloor (i+1) \Delta \rfloor] - p[\lfloor i \Delta \rfloor]$
                \State $\vect{w}_k[i] \gets increase$
                \If{$increase > 0$}
                    \State $\vect{b}_k[i] \gets \argmax_{n \in \{\lfloor i \Delta \rfloor, \ldots, \lfloor (i+1) \Delta \rfloor \}}{\frac{\diff}{\diff n}p[n]}$
                \Else
                    \State $\vect{b}_k[i] \gets \lfloor \Delta (i + \frac{1}{2})\rfloor$
                \EndIf
            \EndFor
            \State $\mathcal{W} \gets \mathcal{W} \cup \{\vect{w}_k\}$
            \State $\mathcal{B} \gets \mathcal{B} \cup \{\vect{b}_k\}$
        \EndFor
        \State Compute masking vector: $\vect{m} \gets \sign{(x[n])}$
        \State \Return $(\mathcal{W}, \mathcal{B}, \vect{m})$
    \EndFunction
    \end{algorithmic}
\end{algorithm}

\input{figs/signals.figure}

\begin{rmk}
    As can be seen in the \Cref{alg:sd}, the \gls{sd} yields a redundant, i.e. over-complete, representation of the signal $x[n]$.
    If algorithm performance is a concern, as in real-time processing, the pyramids can be calculated for a range of layers, even a single layer.
\end{rmk}

\begin{rmk}[Computational Complexity of the \gls{sd}]
    As can be seen in \Cref{alg:sd}, the computational complexity of constructing $n^{th}$ layer of the pyramids is $O(n)$.
    If full pyramids are constructured, the overall performance of the algorithm is $O(N^2)$.
\end{rmk}

\subsection{Signal Reconstruction}
The process of reconstructing the original signal \( \vect{x} \) from the \gls{sd} involves utilizing the feature pyramids \( \mathcal{W} \) and \( \mathcal{B} \), along with the masking vector \( \vect{m} \). The inverse transformation, denoted as \( \mathcal{S}^{-1} \), aims to reverse the effects of the \gls{sd}, combining the components represented in the feature pyramids to approximate the original signal.

\begin{equation}
    \mathcal{S}^{-1}: \{ \mathcal{W}, \mathcal{B}, \vect{m} \} \rightarrow \vect{x}~.
    \label{eq:sd-inverse}
\end{equation}

The reconstruction process from a single layer $k$ can be expressed as follows:
\begin{equation}
    \hat{p}[n] = \sum_{i=0}^{k-1} \vect{w}_k[i] \cdot \sigma \left[ \frac{n - \vect{b}_k[i]}{\kappa} \right]
    \label{eq:reconstruction-pn}
\end{equation}

\begin{equation}
    \hat{x}[n] = \sqrt{ \hat{p}[n] - \hat{p}[n-1] } \cdot m[n]
    \label{eq:reconstruction}
\end{equation}

In Equation \ref{eq:reconstruction}, \( \vect{w}_k[i] \) and \( \vect{b}_k[i] \) represent the weight and bias of the \( i^{th} \) sigmoid component in the \( k^{th} \) layer of the feature pyramid, respectively. The function \( \sigma \) is the sigmoid function, and \( \kappa \) is a scaling factor which adjusts the steepness of the sigmoid. The masking vector \( \vect{m} \) is used to restore the original sign of the signal, as well as to handle the absolute value taken during the square root operation.

\begin{rmk}
    The reconstruction accuracy depends on the number of layers and components used in the SD. Higher layers and components lead to a more accurate reconstruction but at the cost of increased computational complexity.
\end{rmk}

\begin{rmk}
    The reconstruction process, as outlined in Equation \ref{eq:reconstruction}, assumes an ideal scenario. In practical applications, some information loss during decomposition might lead to minor discrepancies between the original and reconstructed signals.
\end{rmk}

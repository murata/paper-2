In this section, the manuscript delineates its principal contribution: a sophisticated energy-based vibro-localization methodology.
This method proficiently pinpoints the precise locations of occupants within an indoor setting, despite challenges posed by measurement uncertainties and the potential presence of Byzantine sensors.
The technique is based on vibro-measurements obtained from the floor due to the distinctive footfall patterns of the occupants.
As an individual traverses the environment, the force exerted by their heel-strikes, commonly referred to as \gls{grf}, induces structural vibration waves that propagate through the floor.
\input{figs/fig_interaction.tikz}

% \Cref{fig:overview} provides a graphical summary of the interaction between occupant (excitation), floor (wave propagation media), and the accelerometers (mode of perception).

\subparagraph{Definitions and Notation}
Consider the problem of estimating a footstep $\vect{x}_{true} \in \mathcal{R}^2$ using $m$ number of accelerometers placed under a dispersive floor as depicted in \Cref{fig:overview}.
Let $\mathcal{M} \triangleq \{1, \ldots, m\}$ be the index set of all the sensors.
Suppose sensor $i \in \mathcal{M}$ is located at $\vect{t}_i$ in a rectangular localization space $\mathcal{S}$ that is defined as $\mathcal{S} = \{ (x, y): \left(x_{min} \leq x \leq x_{max}\right) \wedge \left(y_{min} \leq y \leq y_{max}\right)\}$.
The vibro-measurement equation for sensor $i$ associated with the heel strike of the footstep is
\begin{equation}
    z_i[k] = z_{true, i}[k] + \upzeta_i[k]~,
    \label{eq:zi}
\end{equation}
where $z_{true, i}[k] \in \mathcal{R}$ and $z_i[k] \in \mathcal{R}$ consitute the unknown ``true'' and measured acceleration values obtained at time step $k$ by sensor $i \in \mathcal{M}$, respectively.
On the other hand, $\zeta_i[k] \in \mathcal{R}$ represents the measurement error that occurred at time step $k$ associated with sensor $i \in \mathcal{M}$.
We often use a shorthand notation of representing these quantities in vector form associated with the time samples between $k \in \{1, \ldots, n\}$.
Let us define these vectors for sake of clarity: $\vect{z}_i \triangleq \left(z_i[1], \ldots, z_i[n] \right)^\top \in \mathcal{R}^n$, $\vect{z}_{true, i} \triangleq \left(z_{true, i}[1], \ldots, z_{true, i}[n] \right)^\top \in \mathcal{R}^n$ and $\vect{\upzeta}_i \triangleq \left( \zeta_i[1], \ldots, \zeta_i[n] \right)^\top \in \mathcal{R}^n$.

Given these definitions, we derive a probabilistic localization framework that makes use of the signal energy of measurement vector $\vect{z}_i$ and a novel Byzantine Sensor Elimination algorithm to benefit from as many sensors as possible in estimating $\vect{x}_{true}$.
The proposed localization technique employs a parametric energy decay model to estimate the distance between the sensor and the occupant.
% To characterize the probabilistic properties of the overall localization framework, we start studying the signal given in \Cref{eq:zi} under certain assumptions.

The energy $e_i$ of the stochastic vibro-measurement vector $\vect{z}_i$ can be derived by employing Rayleigh's theorem as shown in \Cref{thm:rayleigh-deterministic}.
Without loss of generality, the signal energy of a random measurement vector $\vect{z}_i$ is
\begin{subequations}
    \begin{equation}
        e_i = \norm{\vect{z}_i}_2^2 = \vect{z}_i^\top \vect{z}_i~.
        \label{eq:energy}
    \end{equation}
    By plugging \Cref{eq:zi} in \Cref{eq:energy}, we get:
    \begin{equation}
        e_i = \left( \vect{z}_{true, i} + \vect{\upzeta}_i \right)^\top \left( \vect{z}_{true, i} + \vect{\upzeta}_i \right)~.
        \label{eq:rayleigh}
    \end{equation}
\end{subequations}

We assume the disturbance vector $\vect{\upzeta}_i$ is an independently and identically distributed normal random vector, i.e., $\zeta_i[k] \sim \N{\mu_\zeta}{\sigma_\zeta}$.
Therefore, we can show $\vect{z}_i \sim \NSTD{\vect{z}_{true,i } + \mu_\zeta}{\sigma_\zeta^2 \vect{I}}$.
With this, we present our proposition showing $\f{E_i}$ can be approximated with a normally distributed random variable when the number of samples $n$ is sufficiently large.

% In this study, we found that the signal energy $e_i \sim \f{E_i} = \sigma_\zeta^2 \chi^{2,~\prime}_n \left( \lambda \right)$ exactly follows a noncentral chi-squared distrubtion with $n$ degrees-of-freedom (number of samples that the energy is calculated with) and noncentrality parameter $\lambda = \frac{1}{\sigma_\zeta^2}\left(\vect{z}_{true,i } + \mu_\zeta \right)^\top \left(\vect{z}_{true,i } + \mu_\zeta\right) $, (c.f.
% \Cref{thm:noncentral-chi}).

\begin{Proposition}
    Energy of a stochastic vibro-measurement vector can be approximated with a normally distributed random variable with a mean $\mu_E$ and variance $\sigma^2_E$.
    \label{thm:opus-theorem}
    \begin{equation}
        e \simeq \N{\mu_e}{\sigma_e}
    \end{equation}
\end{Proposition}
% As the random variables in hand, i.e., $\vect{\upzeta}$ are normally distributed, the signal energy distribution of a stochastic signal is distributed with Noncentral Chi-squared distribution.
% It should be noted that Noncentral Chi-squared distribution is known to converge to a Normal distribution when number of summands are sufficiently large ($n>20$) and summands are independent.
% As the modern accelerometers can provide thousands of samples in a second, we assert this assumption holds and the energy distribution safely converges the Normal distribution.
The details of this assertion can be seen below.
\begin{proof}
    \label{pro:energy-normal}
    If we divide both sides of the \Cref{eq:rayleigh} with the variance $\sigma_\zeta^2$, we have:
    \begin{equation*}
        \frac{e_i}{\sigma_\zeta^2} = \frac{1}{\sigma_\zeta^2} \vect{z}_i^\top \vect{z}_i~.
    \end{equation*}

    Recall $\zeta_i[k] \sim \N{\mu_\zeta}{\sigma_\zeta}$; therefore, $\frac{\zeta_i[k]}{\sigma_\zeta} \sim \NSTD{\frac{\mu_\zeta}{\sigma_\zeta}}{1}$.
    Developing on this idea, the left hand side of the equation above is a random variable that is the squared sum of $n$ indepedently and identically distributed normal random variables that have unit variances.
    % , i.e., $\frac{2}{\sigma_\zeta^2}\vect{z}_{true, i}^\top \vect{\upzeta}_i \sim \mathcal{N} \left(\frac{2 \mu_\zeta}{\sigma_\zeta^2} \norm{\vect{z}_{true, i}}_1, \frac{4}{\sigma_\zeta^2} \vect{z}_{true, i}^\top \vect{z}_{true, i}\right)$, follows a normal distribution with a mean $\frac{2 \mu_\zeta}{\sigma_\zeta^2} \norm{\vect{z}_{true, i}}_1$ and variance $\frac{4}{\sigma_\zeta^2} \vect{z}_{true, i}^\top \vect{z}_{true, i}$.

    % Realize the last term in the equation above suggests that $\frac{1}{\sigma_\zeta^2}\vect{\upzeta}^\top\vect{\upzeta}$ is the squared sum of $n$ normally distributed random variables that have unit variances.
    Thus, we can show that $\frac{e_i}{\sigma_\zeta^2} \sim \chi^{\prime 2}_n\left(\lambda\right)$ follows a noncentral chi-squared distribution with n degrees-of-freedom and noncentrality parameter $\lambda = \frac{1}{\sigma_\zeta^2}\left(\vect{z}_{true,i } + \mu_\zeta \right)^\top \left(\vect{z}_{true,i } + \mu_\zeta\right)$ (cf.
    \Cref{thm:noncentral-chi}).
    % Gathering everything together, we have:
    % \begin{equation}
    %     \frac{e_i}{\sigma^2_\zeta} \sim \mathcal{N}\left(\frac{2 \mu_\zeta}{\sigma_\zeta^2} \norm{\vect{z}_{true, i}}_1, \frac{4}{\sigma_\zeta^2} \vect{z}_{true, i}^\top \vect{z}_{true, i}\right) + \chi^{\prime 2}_n \left( \dfrac{\mu_\zeta}{\sigma_\zeta} \right)^2~.
    %     \label{eq:chisq}
    % \end{equation}
    %     & \sim \chi^{\prime 2}_n \left( \frac{1}{\sigma^2_\zeta} \sum_{k=1}^n \left(z_t[k] + \mu_\zeta\right)^2 \right)
    By invoking \Cref{thm:normal-approximation}, it can be shown that the left-hand side of the equation can be actually approximated to a Normally distributed random variable when the number of time samples is sufficiently large ($n>20$).
    \begin{equation*}
        \frac{e_i}{\sigma_\zeta^2} \approx \NSTD{n + \frac{1}{\sigma_\zeta^2}\left(\vect{z}_{true,i } + \mu_\zeta \right)^\top \left(\vect{z}_{true,i } + \mu_\zeta\right)}{2 n + \frac{4}{\sigma_\zeta^2}\left(\vect{z}_{true,i } + \mu_\zeta \right)^\top \left(\vect{z}_{true,i} + \mu_\zeta\right)}
    \end{equation*}

    Finally, the scalibility property of Normal distribution can be employed to derive the distribution of the signal energy $e_i \simeq \N{\mu_{E_i}}{\sigma_{E_i}}$, where the mean and variance is characterized as shown below.

    \begin{subequations}
        \begin{align*}
            \mu_{E_i} &= \expt{e_i} = \sigma^2_\zeta n + \left(\vect{z}_{true,i } + \mu_\zeta \right)^\top \left(\vect{z}_{true,i } + \mu_\zeta\right)~,
            \intertext{and}
            \sigma_{E_i}^2 &= \Var{e_i} = 2\sigma^4_\zeta n+4 \sigma^2_\zeta \left(\vect{z}_{true,i } + \mu_\zeta \right)^\top \left(\vect{z}_{true,i } + \mu_\zeta\right)~.
        \end{align*}
    \end{subequations}
\end{proof}

As a consequence of \Cref{thm:opus-theorem}, we present the next corollary:

\begin{Corollary}
    If the sensor is calibrated such that sensor bias is neglibly small, i.e.
    $\mu_\zeta  \approxeq 0$ and the variance of the measurement error is known $\sigma_\zeta^2$, then the energy distribution can be parameterized with number of samples $n$ and the unknown true energy $e_{true,i} = \vect{z}_{true, i}^\top \vect{z}_{true, i}$ that the sensor was supposed to register:
    \begin{equation*}
            e_i \sim f_{E_i}(e_i; e_{true, i}) \approx \NSTD{n \sigma_\zeta^2 + e_{true, i}}{2 n \sigma_\zeta^4 + 4 e_{true, i} \sigma_\zeta^2} = \dfrac{1}{\sigma_{E_i} \sqrt{2\pi}}\exp{\left[-\frac{\left(e_i-\mu_{E_i}\right)^2}{2\sigma^2_{E_i}} \right]}~,
    \end{equation*}
    where the mean $\mu_E$ and variance $\sigma_E^2$ of the energy distribution are given by
    \begin{subequations}
        \begin{align*}
            \mu_{E_i} &= \expt{e_i} = \sigma_\zeta^2 n + e_{true, i}~,
            \intertext{and}
            \sigma_{E_i}^2 &= \Var{e_i} = 2 n \sigma_\zeta^4 + 4 e_{true, i} \sigma_\zeta^2~.
        \end{align*}
    \end{subequations}
    % $\mu_E = \sigma_\zeta^2 + e_t$, $\sigma_E^2 = 2 n \sigma_\zeta^4 + 4 e_t \sigma_\zeta^2$, respectively.
\end{Corollary}

\subsection{Parametric Energy Decay \& Localization Framework}
In this work, we exploit the notion that the signal energy decreases as the structural vibration wavefronts propagate along a path.
Based on this concept, we propose a localization function $\vect{h}_i: (e_i, \theta_i)^\top \mapsto \vect{x}_i$ that maps the signal energy $e_i$ of the vibro-measurement vector $\vect{z}_i$ and the directionality of the occupant $\theta_i$ to a location vector $\vect{x}_i \in \mathcal{S}$.
\Cref{eq:xi} demonstrates the hypothesized localization function $\vect{h}_i(e_i, \theta_i; \boldsymbol{\upbeta}_i)$.

\begin{equation}
    \vect{x}_i = \vect{h}_i\left(e_i, \theta_i; \boldsymbol{\upbeta}_i\right) = \vect{t}_i + g_i(e_i; \boldsymbol{\upbeta}_i)
    \begin{bmatrix}
        \cos{\theta_i} \\
        \sin{\theta_i}
    \end{bmatrix}~,
    \label{eq:xi}
\end{equation}
% = \vect{x}_{true} + \vect{\upchi}_i
% where the vector $\vect{\upchi}_i \in \mathbb{R}^2$ represents the estimation error when the occupant is located at $\vect{x}_{true}=\left(x_{true}, y_{true}\right)^\top \in \mathcal{S}$
where $\vect{\upbeta}_i$ represents a known calibration vector of a parametric energy decay model $g\left(e_i; \vect{\upbeta}_i \right):e_i \mapsto d_i$ that maps energy $e_i$ to the distance $d_i$.
A graphical representation of \Cref{eq:xi} is shown in \Cref{fig:equation-graphical}.
It should be noted that the parametric energy decay model $g(\cdot)$ is assumed to be a monotonically decreasing function for some positive energy measurement $e_i \in \mathbb{R}_+$; therefore, it is bijective and its inverse exists.

\input{figs/layout.tikz}

In this framework, the occupant location with respect to the $i^{th}$ sensor is parameterized with its representation in polar coordinate system centered at sensor location $\vect{t}_i$.
In other words, the proposed framework has a flexibility to distinguish the distance $d_i$ and directionality $\theta_i$ components separately.
As an accelerometer by itself provides only ranging information, i.e., the distance between the occupant and itself, we assume directionality component $\theta_i$ is completely unknown.
Therefore, we can simply model it as random variable which follows a Uniform distribution between the range $(0, 2\pi)$, that is denoted as $\theta_i \sim \mathcal{U}(0, 2\pi)$.
% The location estimation in global coordinate system can be then represented as:

The defining properties of location estimate $\vect{x}_i$ can be obtained by studying its \gls{pdf} $\f{\vect{X}_i}$.
The derivation of the \gls{pdf} $\f{\vect{X}_i}$ given the \gls{pdf} of signal energy $\f{E_i}$ is a straight-forward application of Density Transformation Theorem shown in \Cref{thm:density}.
The following section provides a simple signal model with which the signal energy $e_i$ can be computed from the measurement vector $\vect{z}_i$.

\subsection{Derivation of \gls{pdf} of Location Estimation}
Given the localization function $\vect{h}_i(e_i, \theta_i; \vect{\upbeta}_i)$ that maps $(e_i, \theta_i)^\top$ to $\vect{x}_i$, we can derive the \gls{pdf} of the location estimate $\vect{x}_i$ using the Density Transformation theorem, as presented in \Cref{thm:density}.

\begin{subequations}
    \label{eq:fxi}
    \begin{align}
        f_{\vect{X}_i}\left(\vect{x}_i; e_{true, i}, \vect{\upbeta}_i \right) &= f_{E_i, \theta_i}\left( g_{i}^{-1}\left(\norm{\vect{x}_i-\vect{t}_i} \right), \theta_i; e_{true,i}, \vect{\upbeta}_i \right) \left\lvert \det{\vect{J}^{-1}_{\vect{h}_i}(\vect{x}_i; \vect{\upbeta}_i)}\right\rvert~,
        \intertext{because $E_i$ and $\theta_i$ are independent from each other:}
        &= f_{E_i}(g_{i}^{-1}\left(\norm{\vect{x}_i-\vect{t}_i} \right); e_{true,i}, \vect{\upbeta}_i) \f{\theta_i} \left\lvert \det{\vect{J}^{-1}_{\vect{h}_i}(\vect{x}_i; \vect{\upbeta}_i)}\right\rvert~,
        \intertext{where $\f{\theta_i} = \frac{1}{2\pi}$ and $f_{E_i}(e; e_{true,i}, \vect{\upbeta}_i) = \frac{1}{\sigma_E \sqrt{2\pi}}\exp{\left[-\frac{\left(e-\mu_E\right)^2}{2\sigma^2_E} \right]}$.
        Therefore,}
        f_{\vect{X}_i} \left(\vect{x}_i; e_{true, i}, \vect{\upbeta}_i \right) &= \frac{1}{\sigma_E ({2\pi})^{\frac{3}{2}}}\exp{\left[-\frac{\left(g_i^{-1}\left(\norm{\vect{x}_i-\vect{t}_i} \right)-\mu_E\right)^2}{2\sigma^2_E} \right]} \left\lvert \det{\vect{J}^{-1}_{\vect{h}_i}(\vect{x}_i; \vect{\upbeta}_i)}\right\rvert~,
    \end{align}
\end{subequations}
where $\vect{J}^{-1}_{\vect{h}_i}(\vect{x}_i; \vect{\upbeta}_i)$ denotes the Jacobian matrix of inverse localization function $\vect{h_i}^{-1}(\cdot)$ evaluated at $\vect{x_i}$.

The definition of the Jacobian matrix $\vect{J}^{-1}_{\vect{h}_i}(\vect{x}_i)$ is given in \Cref{eq:jacobian}.
\begin{equation}
    \label{eq:jacobian}
    \vect{J}^{-1}_{\vect{h}_i}(\vect{x}) = \begin{bmatrix}
        \frac{\partial g_{i}^{-1}}{\partial x}\left(\norm{\vect{x}-\vect{t}_i}_2; \vect{\upbeta}_i \right) & \frac{\partial g_{i}^{-1}}{\partial y}\left(\norm{\vect{x}-\vect{t}_i}_2; \vect{\upbeta}_i \right) \\
        \frac{\partial \angle{\left(\vect{x}-\vect{t}_i\right)}}{\partial x} & \frac{\partial \angle{\left(\vect{x}-\vect{t}_i\right)}}{\partial y}
    \end{bmatrix}~,
\end{equation}
where
\begin{equation*}
    \det{\vect{J}^{-1}_{\vect{h}_i}(\vect{x})} = \frac{1}{\norm{\vect{x} - \vect{t}_i}}\frac{\partial g_{i}^{-1}}{\partial \vect{x}}\left( \norm{\vect{x} - \vect{t}_i}; \vect{\upbeta}_i\right)~.
\end{equation*}

By plugging \Cref{eq:jacobian} into \Cref{eq:fxi}, we obtain:

% \begin{equation*}
%     f_{\vect{X}_i}\left(\vect{x}_i; e_{true, i}, \vect{\beta}\right) = \frac{1}{\sigma_E ({2\pi})^{\frac{3}{2}}}\exp{\left[-\frac{\left(g^{-1}\left(\norm{\vect{x}_i-\vect{t}_i} \right)-\mu_E\right)^2}{2\sigma^2_E} \right]} \left\lvert \det{\begin{bmatrix}
%         \frac{\partial g^{-1}}{\partial x}\left(\norm{\vect{x}-\vect{t}_i}_2; \vect{\beta}\right) & \frac{\partial g^{-1}}{\partial y}\left(\norm{\vect{x}-\vect{t}_i}_2; \vect{\beta}\right) \\
%         \frac{\partial \angle{\left(\vect{x}-\vect{t}_i\right)}}{\partial x} & \frac{\partial \angle{\left(\vect{x}-\vect{t}_i\right)}}{\partial y}
%     \end{bmatrix}} \right\rvert
% \end{equation*}

\begin{equation}
    \Rightarrow f_{\vect{X}_i}\left(\vect{x}_i; e_{true, i}, \vect{\upbeta}_i \right) = \frac{\left\lvert \frac{\partial g_{i}^{-1}}{\partial \vect{x}_i}\left( \norm{\vect{x}_i - \vect{t}_i}; \vect{\upbeta}_i \right) \right\rvert}{ \norm{\vect{x}_i - \vect{t}_i} \sigma_E ({2\pi})^{\frac{3}{2}}}\exp{\left[-\frac{\left(g_{i}^{-1}\left(\norm{\vect{x}_i-\vect{t}_i} \right)-\mu_E\right)^2}{2\sigma^2_E} \right]}~.
    \label{eq:fxi_final}
\end{equation}

\Cref{eq:fxi_final} shows the \glspl{pdf} $f_{\vect{X}_i}(\cdot)$ assign a probability value for an occupant located at vector $\vect{x}_i$ given the inverse of the parametric decay function $g(\cdot)$ and sensor location $\vect{t}_i$.
Notice the term in the denominator, i.e., $\norm{\vect{x}_i - \vect{t}_i}$, in \Cref{eq:fxi_final} resulting in an inverse relationship between the probability values and the distance between the sensor and the impact location.
% As can be seen in the The \gls{pdf} $f_{\vect{X}_i}$ encodes

\subsection{Sensor Fusion}
Given the \glspl{pdf} $f_{\vect{X}_i}\left(\vect{x}_i; e{true, i}, \vect{\upbeta}_i\right)$ for all sensors indexed by $i \in \mathcal{M}$, we aim to find a joint-\gls{pdf}.
As all the sensors are independent of each other, we can represent the joint-\gls{pdf} as given below:

\begin{equation}
    f_{\vect{X}_1, \ldots, \vect{X}_m}\left(\vect{x}_1, \ldots, \vect{x}_m; e_{true, 1}, \ldots, e_{true, m}, \vect{\upbeta}_1, \ldots, \vect{\upbeta}_m \right) = \prod_{i=1}^{m} f_{\vect{X}_i}\left(\vect{x}_i; e_{true, i}, \vect{\upbeta}_i \right)~.
\end{equation}
Let $\kappa_i = \dfrac{e_{true,i}}{e_i}$ be an indepedent variable denoting the ratio between the unknown true energy $e_{true, i}$ and measured signal energy $e_i$.
Notice that $e_i = e_{true, i} + \epsilon_i$; thus, $\kappa_i \in [0, 1]$.
Given this definition, we can reparameterize the joint-\gls{pdf} as shown in \Cref{eq:sf} which forms the basis for the sensor fusion algorithm used in this paper.

\begin{equation}
    f_{\vect{X}_1, \ldots, \vect{X}_m}\left(\vect{x}_1, \ldots, \vect{x}_m; \kappa_1, \ldots, \kappa_m, \vect{\upbeta}_1, \ldots, \vect{\upbeta}_m \right) = \prod_{i=1}^{m} f_{\vect{X}_i}\left(\vect{x}_i; \kappa_i, \vect{\upbeta}_i \right)~.
    \label{eq:sf}
\end{equation}

\subsection{Byzantine Sensor Elimination}
Byzantine sensors, as illustrated in \Cref{fig:sensor-fusion}, are those that provide misleading or incorrect data, often deviating from the true value or introducing conflicting information into a sensor network.
In the figure, Sensor c is a prime example of a Byzantine sensor.
Its Probability Density Function (PDF), as shown in image (c), has a sharp peak indicating high precision, but it's offset from the true value, revealing its low accuracy.
When such a sensor's data is fused with data from other sensors, it can significantly distort the resulting joint likelihood, leading to erroneous conclusions or alternative hypotheses about the measured data.

The second row of the figure provides insights into the effects of fusing data from a Byzantine sensor with other sensors.
For instance, image (f) showcases the fusion result of an informative sensor (Sensor a) with the Byzantine Sensor c.
The resulting uniform distribution across the localization space underscores the lack of consensus between the two, emphasizing the detrimental impact of the Byzantine sensor on the fusion process.
Similarly, image (g) depicts the fusion of Sensor b with Sensor c, producing an offset peak that suggests an alternative hypothesis about the occupant's location.
To ensure the reliability of a sensor network, it's crucial to identify and eliminate such Byzantine influences.
By observing the fusion results and identifying distributions that deviate from expected patterns or the true value, one can iteratively pinpoint and remove Byzantine sensors, enhancing the overall accuracy and trustworthiness of the network.

\input{figs/sensor_fusion.tikz}

To prevent these unwanted cases, we present our algorithm identifying a subset of sensors among $\mathcal{M}$ that form a consensus to mitigate the effects of Byzantine sensors.
% This algorithm is demonstrated in \Cref{alg:cap}.
Essentially, the proposed Byzantine Sensor Elimination algorithm employs an iterative approach to cluster the sensors into two groups by using an information theoretic approach.

In the preliminary phase of the Byzantine Sensor Elimination algorithm, a comprehensive computation is executed to determine all conceivable pairwise joint-\glspl{pdf} and associated entropies associated with the sensors.
Following that, an initial consensus set is defined using the sensor pair $(i, j)$ that generates the most entropy after the fusion procedure.
Formally, this set is articulated as:
\begin{equation}
\mathcal{C} = \left\{i,j \mid \argmax_{i, j} \expt{-\log{f_{\vect{x}_i, \vect{x}_j}}\left( \vect{x} \right)} \right\}
\end{equation}
wherein both $i$ and $j$ are both members of the set $\mathcal{M}$ and are clearly distinct.

The joint-\gls{pdf} encapsulating the existing consensus, denoted as $f_{\mathcal{C}} \left(\vect{x}\right)$, is derived as follows:
\begin{equation}
f_{\mathcal{C}} \left(\vect{x}\right) \triangleq  \prod_{\forall i \in \mathcal{C}} f_{\vect{X}_i}\left(\vect{x}; \kappa_i, \vect{\upbeta}_i \right)
\end{equation}

Throughout each iterative phase, a novel sensor, denoted as $k$ (which is distinct from the pair $(i, j)$), is selected from the set $\mathcal{M}$.
This sensor undergoes an evaluation against the prevailing consensus set $\mathcal{C}$, achieved by amalgamating its \gls{pdf} with the consensus \gls{pdf} $f_{\mathcal{C}}(\vect{x})$.
A subsequent determination is predicated upon the entropy, or more precisely, the surprisal of the emergent hypothesis in juxtaposition with the current consensus.
Should the integration of sensor $k$ not attenuate the joint-\gls{pdf} to a uniform distribution (as exemplified in case (f) in \Cref{fig:sensor-fusion}) or not decrease the average entropy, it is incorporated into the consensus set.
Contrarily, sensor $k$ is classified as Byzantine.
Conclusively, the aforementioned procedure undergoes iterative repetition with varying vectors of $\vect{\upkappa} = \left(\kappa_1, \ldots, \kappa_m\right)^\top$.
This iteration progresses in the direction of the gradient of the $f_\mathcal{C}(\vect{x})$.
The process persists until a local maxima in the entropy landscape, corresponding to the optimal consensus, is identified and ascertained.


% \begin{algorithm}[htb!]
% \caption{Guided Gradient Descent-based Multi-Sensor Localization Algorithm}\label{alg:cap}
%     \begin{algorithmic}[1] % The number tells where the line numbering should start
%         \Procedure{Localization}{$\cdots$}
%             \State $m \gets \lvert \mathcal{M} \rvert$
%             \State $\mathcal{G} \gets \textproc{discretize}\left(\mathcal{S}\right)$
%             \State $g \gets \lvert \mathcal{G} \rvert$
%             \State $bestcost \gets -\infty$
%             \State $\vect{\upkappa}^* \gets \vect{0}_m$
%             \For{$\kappa \gets 0$ to $1$}
%                 \State $\vect{h} \gets \vect{0}_m$
%                 \For{$i \gets \mathcal{M}$}
%                     \State $\vect{F}_i \gets \vect{0}_{g}$
%                     \For{$g_i \gets \mathcal{G}$}
%                         \State $\vect{F}_i(j) \gets f_{\vect{X}_i}\left(g_i; \cdot \right)$
%                     \EndFor
%                     \State $\vect{h}(i) \gets \textproc{entropy}\left( \vect{F}_i \right)$
%                 \EndFor
%                 \State $\mathcal{C}, \mathcal{N} \gets \textproc{BSE}\left( \vect{h} \right)$
%                 \State $cost = \sum_{i=\mathcal{C}} \vect{h}(i)$
%                 \If{$bestcost \leq cost$}
%                     \State $bestcost \gets cost$
%                     \State $\vect{\upkappa} \gets \kappa \vect{I}_m$
%                 \EndIf
%             \EndFor
%             \State \textbf{return} $\vect{x}^*$ \Comment{Optimal Localization Result}
%         \EndProcedure
%         \Procedure{BSE}{$\cdots$}
%             \State $P, N \gets \emptyset$
%         \EndProcedure
%         \Procedure{Fusion}{$\mathcal{F}$} \Comment{Set of \glspl{pdf} evaluated at all grid centers.}
%         \State $m \gets \left \lvert \mathcal{F} \right \rvert $
%         \State $\vect{R} \gets \vect{I}$
%         \For{$i \gets 1$ to $m$}
%             \State $\vect{f}_i \gets \mathcal{F}\left(i\right)$
%             \State $\vect{R} \gets \vect{R} \vect{f}_i$
%         \EndFor
%         \State \textbf{return} $\vect{R}$\Comment{The result of the fusion procedure}
%         \EndProcedure
%     \label{euclidendwhile}
%     \end{algorithmic}
% \end{algorithm}

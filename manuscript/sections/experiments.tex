The proposed technique's effectiveness and performance were assessed through a series of controlled experiments, the details of which are outlined in this section.

\subsection{Experimental Setup}
To evaluate our vibro-localization approach, we executed controlled experiments in a corridor situated on the 4th level of Goodwin Hall, an operational building on Virginia Tech's campus.
In these experiments, two participants traversed a pre-defined 16-meter path.
\Cref{fig:layout-goodwin} represents the step locations constituting the traversed path --represented with green circles ({\color{green}$\circ$})-- as well as the sensor locations --represented with black squares ($\blacksquare$)-- overlayed on top.
This experimental setting has been employed in multiple research endeavors \cite{alajlouni2018,alajlouni2019,alajlouni2020,alajlouni2021} as a benchmark for contrasting various techniques.
We derived our reference point from these investigations and utilized identical experimental data as in \cite{alajlouni2020,alajlouni2021}.
The corridor's concrete floor housed the sensors, which were attached to uniform steel mounts welded to the flanges of the structural I-beams beneath.
Eleven accelerometers, detecting dynamic out-of-plane acceleration within the frequency range of (2, 10000) $Hz$ and with an average sensitivity of 1000 millivolts per $g$ (where $g = 9.8 m/s^2$), recorded the structural vibrations.
These devices captured data from 162 steps taken by each participant, amounting to a total of 324 steps.
The data collection was facilitated by EMX-4250 digital signal analyzer cards, connected to the accelerometers via coaxial cables and equipped with anti-aliasing filters and a high-precision 24-bit ADC.
The accelerometer data was sampled at a rate of 1024 $Hz$.
For a comprehensive insight into the experimental design, readers are directed to the foundational study \cite{alajlouni2019new}.

\input{figs/path.tikz}

\subparagraph{Data and Model Validity:} The preliminary study gathered vibration data during low-activity periods, ensuring minimal movement in the vicinity of the instrumented corridor.
The data revealed that the sensors' noise profiles were normally distributed with zero mean and consistent variance.
Thus, the signal model in \Cref{eq:zi} aptly represents the vibration measurements.
Given this observation, we confidently state that the energy-related random variables, represented as \( e_i \) for \( i=\{1,\ldots,m\} \), align with the experimental findings.

\subparagraph{Differences from the Baseline Study:}
Two primary distinctions exist between our study and the baseline: (\romannumeral1) the signal detection algorithm, and (\romannumeral2) the post-localization filtering used in the baseline.
In our recent research, we utilized a unique and proprietary stochastic detection algorithm.
This choice of algorithm led to a notable difference in the signal energy values when compared to the baseline study.
The baseline study, in its methodology, adopted a tight-window approach.
This approach was characterized by closely aligning the vibration response with the \gls{grf}.
In contrast, our method took a more flexible stance.
Instead of strictly aligning the signals, our algorithm was designed to be more lenient.
It permitted "silent" periods, which are intervals without significant signal activity, both before and after the vibration.
\Cref{fig:detection-results} demonstrates this difference between the signal detection algorithm employed by the baseline and the proposed techniques.
The figure examplifies this distinction with the impulse response curve of a underdamped second order system.
The black line represents the elements of the ``noisy'' vibro-measurement vector.
The dashed red and green lines represent the results of baseline and proposed detection algorithm, respectively.
This distinction in approach not only highlights the variability in signal processing techniques but also underscores the potential impact of these choices on the final results and interpretations.

\begin{figure}[htb!]
    \centering
    \includegraphics[scale=1]{figs/detection-results.eps}
    \caption{This figure demonstrates the differences between signal (step) detection algorithms employed by the baseline and proposed techniques. The black line (\textbf{---}) represents the noisy measurements of a second order system. The green dashed line ({\color{green} \textbf{--- -}}) represents the proposed ``relaxed'' detection results employed in this study. On the other hand, the red dashed line ({\color{red} \textbf{--- -}}) represents the signal detection algorithm employed by the baseline study.}
    \label{fig:detection-results}
\end{figure}

Another significant variation from the baseline study is the post-localization filtering, which utilized a Kalman filter.
The Kalman filter is a mathematical technique designed to estimate the state of a system by incorporating the dynamics of the system with a series of measurements obtained over time.
It excels in situations where the system is subject to uncertainties or noise.
The filter operates by first predicting the system's future state and then updating this prediction when new measurements become available.
This iterative method allows the Kalman filter to minimize the estimation error by balancing the predicted and measured values.
As a result of employing the Kalman filter in the baseline study, which likely accounted for these linear paths that the occupants followed, it achieved smaller localization errors compared to our reported results of the baseline.

\subsection{Implementation}
In the course of our data processing, we made a strategic decision to discretize the localization space, denoted as $\mathcal{S}$.
This was achieved by segmenting it into a total of 270,000 grid cells, specifically arranged in a $300 \times 900$ configuration.
One might argue that such discretization could introduce precision issues, and indeed, there is some validity to this concern.
However, the primary motivation behind this approach was to streamline and simplify our algorithm.
By focusing our evaluations of the probability density functions (\glspl{pdf}) solely at the center of each grid cell, we were able to bypass the complexities that might arise from evaluating them across the entire space.
This strategic simplification had a cascading effect on our calculations.
Notably, many of the equations integral to our proposed vibro-localization technique were transformed.
Instead of dealing with intricate surface integrals, we found that they could be expressed more succinctly as summations.

In order to evaluate the proposed technique, we employed a combinatoral study to analyze the effect of number of sensors used on the localization metrics.
Speficially, we employed all the possible combinations of $m=\{2, \ldots, 11\}$ with the given sensor configuration in the experimental data.
This yields $n_{cases} = \sum_{i=2}^{11} \binom{11}{i} = 2036$ number of cases to evaluate for each step and occupant.
In other words, each step is reevaluated 2036 times, yielding $329832 = 2036 \times 162$ data points for each occupant.
Therefore, in our analysis, we are able to provide different defining statistical characteristics of this compherensive studty.
This comprehensive study enables us to remedy various uncertainty sources such as the effects of sensor placement, difference in propagation paths while enriching the results indepedent of the individual sensor performance.

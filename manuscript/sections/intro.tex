Indoor occupant localization is a pivotal perception challenge that is instrumental in facilitating a myriad of applications, from smart home automation to emergency response.
These applications are predicated on meticulous location information of the occupants in a building.
Accurate occupant localization results yield finer-grained information about occupants, such as smart home monitoring and event classification~\cite{Woolard2017, Li2020, Clemente2020}, human gait assessment~\cite{Fagert2017, Kessler2019, Kessler2019a, Davis2022}, and, systemic occupant identification and tracking~\cite{Pan2015, Poston2017, Hu2021, Drira2022, Fagert2022}.
Furthermore, the symbiotic relationship between infrastructure and its occupants becomes more natural and uninterrupted especially when using passive techniques.
This implies that occupants do not need to carry beacons, transmitters, or any other apparatuses transmitting known features to the localization system.

\subsection{Background}
\gls{eol} is an example of such passive localization techniques which utilize the energy inherent in signals.
Whether the underlying signal is in the frequency or time domain, these techniques ascertain and monitor the positions of individuals within a designated area~\cite{Valero2021, Alam2021, MejiaCruz2021, davisdissertation}.
For this discussion, terms such as signal energy, power, intensity, and strength are utilized interchangeably, despite their nuanced differences.
\gls{eol} techniques have gained increasing popularity in academia due to their versatility in abstracting complex details of observed natural phenomena.
% These techniques provide a simplified framework that reduces the need for detailed signal analysis.
These techniques offer a simplified approach, reducing the need for exhaustive signal analysis.
The  signal energy serves a consistent and uniform metric for gauging the magnitude with a real number.
Specifically, higher signal amplitudes result in larger energy values registered by the sensors, and vice versa.
Furthermore, it does not require accurate time-synchronization among sensors which enables pervasive and distributed computing easier.
Therefore, it is considered one of the most intuitive and straightforward localization features in the localization literature.

Concurrent with the exploration of signal energy-based techniques, substantial scholarly endeavors have been directed towards examining the ramifications of the floor's dispersive attributes ---specifically, its role as a propagation medium or waveguide--- on localization outcomes.
The researchers have mitigated the dispersive effects inherent in the floor within the context of localization by isolating a narrow frequency band.
This band, derived via Continuous Wavelet Transformation, remains unaffected by dispersion~\cite{Kwon2008, Racic2009, Ciampa2010, Mirshekari2016}.
\citet{Kwon2008} present a non-intrusive methodolgy for discerning and recognition of human activities utilizing floor vibration sensors.
They advocate for a feature extraction technique based on wavelet packet decomposition coupled with statistical measures to capture the unique characteristics of different activities.
The empirical findings underscore the efficacy of the proposed modality in the precise delineation and categorization of diverse human activities, highlighting its prospective utility in intelligent environments and surveillance infrastructures.
\citet{Racic2009} propose a technique for the detection and categorical classification of human activities via floor vibrations.
They employ a combination of wavelet-based feature extraction and a support vector machine classifier to accurately identify different activities, demonstrating the potential of floor vibration sensors for activity monitoring and recognition in smart environments.
It is, however, noteworthy to mention that narrowband filtering invariably compromises the spatial resolution of the pertinent localization technique.
This challenge, especially in the context of radio-localization, has been discussed in detail by Ghany et al.~\cite{Ghany2020}.

Conversely, the Warped Frequency Transformation technique has been utilized to discern the dispersion curve, with the primary objective of directly mitigating perturbations attributed to dispersion~\cite{demarchi2011, woolard2018}.
There exist system-theoretic techniques that characterize the dynamic behavior of the floor via transfer function estimation~\cite{Mohammed2021, Davis2021, MejiaCruz2022}.
Additionally, the Green's function has been employed to account for wave reflections and its dispersion~\cite{Nowakowski2015}.
Despite their commendable precision in empirical analyses, these techniques exhibit limitations in their capacity to generalize the intricate material properties and boundary conditions inherent in floors encountered in practical applications.

On the other hand, \citet{bahroun2014} presented their work formalizing the group velocities, i.e., a major component of signal energy, as a function of propagation path distance.
These promising results paved the way for the model-based techniques which tend to explain the wave phenomena from the data.
\citet{alajlouni2018}, for instance, showed their hypothesis of an energy-decay model (energy logarithmically decays with propagation distance) as a localization model.
In their work, \citet{Pai2019} analyzed whether a relationship exists between the occupant's footfall patterns and the measured signal characteristics in an empirical case study.
In light of their work, the authors assert that there is no evidence of a monotonic relationship between amplitude or kurtosis of the measured signal and the propagation distance.
However, parametric energy-decay models proving there is still much room to improve~\cite{alajlouni2020, Alajlouni2022, Wu2023, Tarrio2012}.

Along with parametric decay models, \citet{poston2016}, in an alternative attempt, moved the localization frameworks to a probabilistic framework to model the probability of detection and false alarm.
\citet{Alajlouni2022} took a similar approach to localize the occupant by maximizing the sensor likelihood functions given the hypothesis of sensor's time domain measurements and the energy-decay model proposed in their earlier work.
\citet{Wu2023} propose G-Fall, a device-free fall detection system based on floor vibrations collected by geophone sensors.
Their system utilizes Hidden Markov Models (HMMs) and introduces an Energy-of-Arrival (EoA) positioning mechanism, achieving precise and user-independent fall detection with a significant reduction in false alarm rates.

\subsection{Limitations of the Existing Approaches}
In the literature, it is often seen that multiple sensor perception schemes are employed in such techniques to increase sensor coverage over the localization space and reduce the localization error.
However, there is an over-arching theme that can be easily seen in the \gls{eol} techniques: they yield relatively uncertain, i.e.
imprecise, and often less than desirable accuracy in the localization results than \gls{tdoa} and \gls{toa} based techniques~\cite{Sneha2020}.
In reviewing the existing literature on \gls{eol}, several notable gaps and areas of potential improvement emerge.
These points are categorically listed below.

\begin{itemize}
    \item \textbf{Limited Accountability for Measurement Errors}: The literature predominantly focused on the disturbances due to nonlinearities due to complex boundary conditions, non-homogenous material properties, and dispersive nature of the floor.
    Very little effort was invested in the characterization of localization errors due to sensing capabilities and characteristics.

    \item \textbf{Inadequate Handling of Uncertainties}: A comprehensive approach to quantify and handle uncertainties, especially those arising from sensor imperfections, seems to be lacking in many existing methods.
    This gap could lead to less robust and reliable localization outcomes.

    \item \textbf{Sensor Reliability Concerns}: Many studies might not have adequately addressed the challenge of sensors generating erroneous hypotheses.
    This oversight can adversely affect the overall consensus in localization tasks, leading to potential inaccuracies.
\end{itemize}

\subsection{Summary of the Contributions}
This paper presents an \gls{eol} technique that tackles the sensor imperfections and its effects on the localization results in the context of structural vibrations.
In its essence, the proposed technique employs a family of accelerometers placed on a floor, i.e., the propagation medium, measuring the structural vibrations, i.e., the signal of interest, due to the occupants' footfall patterns.
To tackle the aforementioned theme, this study employs a comprehensive uncertainty quantification to determine the bounds of uncertainty due to the sensor imperfections.
The following points summarize the proposed technique's contributions:
\begin{itemize}
    \item \textbf{Introduction of a Novel Vibro-localization Technique}: The paper presents a new energy-based vibro-localization method that determines the location of individuals in indoor settings by analyzing structural vibrations on a floor caused by their footsteps.

    \item \textbf{Information Theoretic Byzantine Sensor Elimination Algorithm}: The paper introduces a Byzantine Sensor Elimination algorithm.
    This algorithm identifies and eliminates sensors that generate erroneous hypotheses, preventing their influence on the overall consensus.

    \item \textbf{Comprehensive Uncertainty Quantification}: The study employs a comprehensive uncertainty quantification approach to determine the bounds of uncertainty due to sensor imperfections.

    \item \textbf{Quantification of Precision and Accuracy}: By implementing the "Byzantine Sensor Elimination" algorithm, the paper establishes a metric that quantifies the correlation between precision and accuracy in the proposed vibro-localization technique.

    \item \textbf{Empirical Validation}: Controlled experiments were conducted to validate the proposed technique.
    The results demonstrated significant improvements over the baseline approach in terms of both accuracy and precision.
\end{itemize}

\subsection{Organization of the Paper}
This paper is structured into six distinct sections to provide a comprehensive overview of the research topic.
\begin{itemize}
    \item The first section, referenced as \Cref{sec:intro}, serves as the introductory portion of the article.
    In this section, readers are introduced to the primary problem that the research addresses.
    Additionally, it offers a review of the existing literature on the subject, ensuring that readers have a foundational understanding of the context and the significance of the problem at hand.

    \item Following the introduction, \Cref{sec:technique} delves into the specifics of the proposed technique.
    This technique is unique as it is based on vibration measurements.
    Here, the methodology, the underlying principles, and the reasons for choosing vibration measurements are elaborated upon.

    \item In the subsequent section, \Cref{sec:experiments}, the focus is on the controlled experiments that were carried out.
    This section provides a detailed account of the experimental setup and procedure, laying the groundwork for the results that follow.

    \item \Cref{sec:results} presents the results obtained from the experiments. It offers insights into the technique's performance and reliability, discussing the outcomes in the context of the proposed method's effectiveness.

    \item Finally, the paper wraps up with \Cref{sec:conclusion}.
    This concluding section encapsulates the primary findings of the research.
    It presents the conclusions drawn from the study and sheds light on potential avenues for future work.
    This section serves to summarize the research's contributions and hint at the next steps or further explorations in this domain.
\end{itemize}

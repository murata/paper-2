# Use an official Python runtime as a parent image
FROM python:3.9-slim
# Install system dependencies
RUN apt-get update -y
RUN apt-get install -y --no-install-recommends libx11-dev \
                                               python3-tk \
                                               libegl1 \
                                               libgl1 \
                                               libgomp1 \
                                               libhdf5-dev
# Set the working directory in the container
WORKDIR /app

# Copy the current directory contents into the container at /app
COPY code/ .

# Install any needed packages specified in requirements.txt
RUN pip install --no-cache-dir -r requirements.txt

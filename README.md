# The Sigmoid Decomposition: Algorithms and Its Applications

This repository contains the code, manuscript, and proposal for the research paper titled "The Sigmoid Decomposition: Algorithms and Its Applications".

## Build Status

[![pipeline status](https://code.vt.edu/murata/paper-2/badges/main/pipeline.svg)](https://code.vt.edu/murata/paper-2/-/commits/main)
[![coverage report](https://code.vt.edu/murata/paper-2/badges/main/coverage.svg)](https://code.vt.edu/murata/paper-2/-/commits/main)
[![License](https://img.shields.io/gitlab/license/murata%2Fpaper-2?gitlab_url=https%3A%2F%2Fcode.vt.edu)](https://code.vt.edu/murata/paper-2/-/commits/main)

## Directory Structure

- `code/`: Contains the source code for the project.
  - `dataset/`: Data used in the research.
  - `localizers/`: Localization algorithms.
  - `sigmoid/`: Sigmoid decomposition implementation.
  - `utils/`: Utility functions and scripts.
  - `locata_processing.py`: Processing script for LOCATA data.
  - `requirements.txt`: List of Python dependencies.
  - `uncertainty_quantification.py`: Uncertainty quantification script.
- `Dockerfile`: Docker configuration file.
- `etc/`: Miscellaneous scripts and files.
- `manuscript/`: LaTeX files and resources for the manuscript.
- `proposal/`: LaTeX files and resources for the research proposal.
- `README.md`: This file.

## Getting Started

To get started with this project, clone the repository and install the required dependencies:

```bash
git clone https://code.vt.edu/murata/paper-2.git
cd paper-2/code
pip install -r requirements.txt
```
or you can use the docker images located at [here](https://code.vt.edu/murata/paper-2/container_registry/506).
